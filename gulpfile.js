'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var inject = require('gulp-inject');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var rigger = require('gulp-rigger');
var sourcemaps = require('gulp-sourcemaps');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var react = require('gulp-react');
var path = 'web/';

//WEB react tasks
gulp.task('react', function(){
    return gulp.src(['src/jsx/global/*.jsx', 'src/jsx/web/*.jsx'])
        .pipe(react())
        .pipe(gulp.dest('src/js/web-out'));
});
gulp.task('jsx', ['react'], function(){
    return gulp.src(['src/js/web-out/utils.js', 'src/js/web-out/cars.js', 'src/js/web-out/*.js', 'src/jsx/main.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('src/js/min'));
});
gulp.task('sass', function(){
    gulp.src('src/sass/style.sass')
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(minifyCss())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('src/css'))
});
gulp.task('watch', function(){
    gulp.start(['jsx', 'sass']);
    watch(['src/jsx/global/*.jsx', 'src/jsx/web/*.jsx', 'src/jsx/*.js'], function(){
        gulp.start('jsx');
    });
    watch(['src/sass/*.*'], function(){
        gulp.start('sass');
    });
});

gulp.task('default', ['jsx', 'sass']);
