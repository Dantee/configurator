var PrintPage = React.createClass({displayName: "PrintPage",
    render: function () {
        return React.createElement("section", {className: "print-page"}, 
            React.createElement("div", {className: "cat-title"}, "Accessories")
        );
    }
});

var PrintPage = React.createClass({displayName: "PrintPage",
    getDefaultProps: function () {
        return {
            'print': window.showPrint != null
        };
    },
    handlePrint: function (e) {
        e.preventDefault();
        window.print();
    },
    render: function () {
        return React.createElement("section", {className: "print-page"}, 
            React.createElement("div", {className: "cat-title"}, "Share"), 
            React.createElement("div", {className: "cat-list"}, 
                !this.props.print ? React.createElement("div", null) :
                    React.createElement("a", {className: "print-link", onClick: this.handlePrint}, 
                        React.createElement("img", {className: "print-image", src: "assets/printer.png"})
                    ), 
                React.createElement("a", {className: "print-link"}, 
                    React.createElement("img", {className: "email-image", href: "#email-form", src: "assets/email.png"})
                )
            )
        );
    }
});