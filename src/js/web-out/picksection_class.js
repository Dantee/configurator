var PickSection = React.createClass({displayName: "PickSection",
    openedMenu: false,
    currentTab: 'accessories',
    pagination: false,
    getInitialState: function () {
        return {
            firstName: '',
            lastName: '',
            mobile: '',
            email: '',
            hearMore: false
        }
    },
    setActive: function (tab) {
        return (function () {
            if (this.currentTab !== tab) {
                this.currentTab = tab;
            }
            this.forceUpdate();
        }).bind(this);
    },
    toggleMenu: function () {
        return (function () {
            this.props.openMenu();
        }).bind(this);
    },
    getSpec: function (type) {
        if (typeof this.props.summary !== 'undefined')
            switch (type) {
                case 'variant':
                    return this.props.summary.variant;
                    break;
                case 'accessories':
                    return this.props.summary.accessories;
                    break;
            }
    },
    submiting: false,
    formError: false,
    formValid: false,
    validateForm: function () {
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        if (
            this.state.firstName !== '' &&
            this.state.lastName !== '' &&
            this.state.mobile !== '' &&
            this.state.email !== ''
        ) {
            if (!validateEmail(this.state.email)) {
                this.formValid = false;
                this.formError = 'Please enter correct email address.';
            } else {
                this.formValid = true;
            }
        } else {
            this.formValid = false;
            this.formError = 'Please fill all the fields.';
        }
        this.forceUpdate();
    },
    showError: function () {
        return this.formError ? 'show' : 'hide';
    },
    loading: function () {
        return this.submiting ? 'loading' : '';
    },
    firstNameChange: function (e) {
        this.formError = false;
        this.setState({'firstName': e.target.value});
    },
    lastNameChange: function (e) {
        this.formError = false;
        this.setState({'lastName': e.target.value});
    },
    mobileChange: function (e) {
        this.formError = false;
        this.setState({'mobile': e.target.value});
    },
    emailChange: function (e) {
        this.formError = false;
        this.setState({'email': e.target.value});
    },
    hearMoreChange: function (e) {
        this.formError = false;
        this.setState({'hearMore': e.target.checked});
    },
    resetForm: function () {
        this.setState({
            firstName: '',
            lastName: '',
            mobile: '',
            email: '',
            hearMore: false
        })
    },
    submitForm: function (e) {
        e.preventDefault();
        this.formError = false;
        this.validateForm();
        var request;
        if (this.formValid) {
            this.submiting = true;
            this.forceUpdate();
            request = $.ajax({
                url: e.target.action,
                dataType: 'json',
                data: {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    mobile: this.state.mobile,
                    email: this.state.email,
                    hearMore: this.state.hearMore
                }
            });
            request.done((function () {
                this.submiting = false;
                this.resetForm();
            }).bind(this));

            request.fail((function () {
                this.submiting = false;
                this.formError = 'Something went wrong, please try again later!';
                this.forceUpdate();
            }).bind(this));
        }

    },
    render: function () {
        var _this = this;
        var className = function (tab) {
            if (tab == _this.currentTab) {
                return ' active';
            } else {
                return '';
            }
        };
        var openClass = function () {
            if (_this.props.openedMenu()) {
                return ' opened';
            } else {
                return '';
            }
        };
        var specifications = (this.props.specifications || []).map((function (spec) {
            var data = spec.Data.map((function (data) {
                return React.createElement("li", null, 
                    React.createElement("div", {className: "leftside"}, 
                        React.createElement("span", null, data.Title)
                    ), 
                    React.createElement("div", {className: "rightside"}, 
                        React.createElement("span", null, data.Description)
                    )
                )
            }).bind(this));

            var openedSpec = function (id) {
                if (_this.state !== null) {
                    return _this.state.spec == id;
                }
            };
            var toggleOpen = function (id) {
                return (function () {
                    if (_this.state !== null) {
                        if (_this.state.spec == id) {
                            return _this.setState({spec: null});
                        }
                    }
                    _this.setState({spec: id});
                });
            };

            return React.createElement("div", {className: "spec"}, 
                React.createElement("div", {className: "title " + openedSpec(spec.Title), onClick: toggleOpen(spec.Title)}, spec.Title), 
                React.createElement("div", {className: "data " + openedSpec(spec.Title)}, 
                    React.createElement("ul", null, 
                        data
                    )
                )
            )
        }).bind(this));

        return React.createElement("div", {className: "section-holder" + openClass()}, 
            React.createElement("a", {className: "menu-toggler", onClick: this.toggleMenu()}, 
                React.createElement("div", {className: "dashes"}, 
                    React.createElement("hr", null), 
                    React.createElement("hr", null), 
                    React.createElement("hr", null)
                ), 
            "Menu"
            ), 
            React.createElement("div", {className: "menu-content"}, 
                React.createElement(Back, null), 
                React.createElement("div", {className: "tabs"}, 
                    React.createElement("ul", null, 
                        React.createElement("li", {className: className('accessories'), onClick: this.setActive('accessories')}, 
                            React.createElement("i", {className: "icon accessories"}), 
                        "Accessories"
                        ), 
                        React.createElement("li", {className: className('specifications'), onClick: this.setActive('specifications')}, 
                            React.createElement("i", {className: "icon specifications"}), 
                        "Specifications"
                        ), 
                        React.createElement("li", {className: className('test-ride'), onClick: this.setActive('test-ride')}, 
                            React.createElement("i", {className: "icon test-ride"}), 
                        "Test ride"
                        )
                    )
                ), 
                React.createElement("div", {className: "tabs-wrapper"}, 
                    React.createElement("div", {className: 'tab-content' + className('accessories')}, 
                        React.createElement(AccessoriesPick, {accessories: this.props.accessories, 
                        onChange: this.props.accessoryChange, 
                        active: this.props.activeAccessories, 
                        pagination: this.pagination})
                    ), 
                    React.createElement("div", {className: 'tab-content specifications' + className('specifications')}, 
                        React.createElement("div", {className: "specs"}, 
                            specifications
                        )
                    ), 
                    React.createElement("div", {className: 'tab-content test-ride' + className('test-ride')}, 
                        React.createElement("div", {className: "heading"}, 
                            React.createElement("h2", null, "Test ride your"), 
                            React.createElement("h2", {className: "title"}, this.props.variant), 
                            React.createElement("hr", null)
                        ), 
                        React.createElement("form", {onSubmit: this.submitForm, action: "form/submit", noValidate: true}, 
                            React.createElement("label", null, 
                                React.createElement("span", null, "First Name"), 
                                React.createElement("input", {value: this.state.firstName, onChange: this.firstNameChange, type: "text"})
                            ), 
                            React.createElement("label", null, 
                                React.createElement("span", null, "Last Name"), 
                                React.createElement("input", {value: this.state.lastName, onChange: this.lastNameChange, type: "text"})
                            ), 
                            React.createElement("label", null, 
                                React.createElement("span", null, "Mobile"), 
                                React.createElement("input", {value: this.state.mobile, onChange: this.mobileChange, type: "tel"})
                            ), 
                            React.createElement("label", null, 
                                React.createElement("span", null, "Email"), 
                                React.createElement("input", {value: this.state.email, onChange: this.emailChange, type: "email"})
                            ), 
                            React.createElement("label", null, 
                                React.createElement("input", {value: this.state.hearMore, onChange: this.hearMoreChange, type: "checkbox"}), 
                                React.createElement("span", {className: "checkbox-text"}, "Sign me up to receive special promotions, latest products and to hear more from Honda Motorbikes")
                            ), 
                            React.createElement("div", {className: 'actions ' + this.loading()}, 
                                React.createElement("input", {type: "submit", value: "Make a booking"}), 
                                React.createElement("span", {className: this.showError()}, this.formError)
                            )
                        )
                    )

                )

            )
        );
    }
});