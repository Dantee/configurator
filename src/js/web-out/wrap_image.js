var WrapImage = React.createClass({displayName: "WrapImage",
    render: function () {
        var showClass = 'wrap-promo';
        if (window.wrap !== undefined) {
            showClass = showClass + ' active';
        }
        return React.createElement("div", {className: showClass}, 
            React.createElement("img", {src: "./assets/wraps-promo.png"})
        );
    }
});