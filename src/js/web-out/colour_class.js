var ColourPick = React.createClass({displayName: "ColourPick",
    colourClick: function (col) {
        return (function () {
            if (this.props.active != col.ID)
                this.props.onChange(col.ID);
        }).bind(this);
    },
    render: function () {
        var children = (this.props.colours || []).map((function (col) {
            var cName = this.props.active == col.ID ? " active" : "";
            return React.createElement("a", {className: "colour-choice" + cName, 
            onClick: this.colourClick(col), 
            key: col.ID}, 
                React.createElement("img", {src: col.Thumbnail}), 
                React.createElement("span", null, col.Title)
            )
        }).bind(this));
        return React.createElement("section", {className: "colour-pick"}, 
            React.createElement("div", {className: "cat-title"}, "Colour"), 
            React.createElement("div", {className: "cat-list"}, 
                children
            )
        );
    }
});