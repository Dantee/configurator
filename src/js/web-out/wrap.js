var SubmitWrap = React.createClass({displayName: "SubmitWrap",
    handleSubmit: function () {
        console.log(this.props);
        superagent.post("Thumbnail").send({
            colourID: this.props.activeColour,
            wheelID: this.props.activeWheel,
            accessories: this.props.activeAccessories || [],
            wrap: window.wrap,
            size: 'large'
        }).end((function (data) {
            window.location.href = '/wrap-gallery';
        }).bind(this));
    },
    render: function () {
        if (typeof window.wrap != "undefined" && (typeof window.hideSubmitButton == "undefined" || (typeof window.hideSubmitButton != "undefined" && window.hideSubmitButton == false))) {
            return React.createElement("div", {className: "submit-wrap", onClick: this.handleSubmit}, "Submit");
        } else {
            return false;
        }
    }
});
