var WheelsPick = React.createClass({displayName: "WheelsPick",
    wheelClick: function (whl) {
        return (function () {
            if (this.props.active != whl.ID)
                this.props.onChange(whl.ID);
        }).bind(this);
    },
    render: function () {
        var children = (this.props.wheels || []).map((function (whl) {
            var cName = this.props.active == whl.ID ? " active" : "";
            return React.createElement("a", {className: "wheel-choice" + cName, 
            onClick: this.wheelClick(whl), 
            key: whl.ID}, 
                React.createElement("img", {src: whl.Thumbnail}), 
                React.createElement("span", null, whl.Title)
            )
        }).bind(this));
        return React.createElement("section", {className: "wheel-pick"}, 
            React.createElement("div", {className: "cat-title"}, "Wheels"), 
            React.createElement("div", {className: "cat-list"}, 
                children
            )
        );
    }
});
