var DirtToggle = React.createClass({displayName: "DirtToggle",
    toggleDirt: function () {
        return (function () {
            var allWheels = this.props.wheels;
            var activeWheels = this.props.activeWheels;
            var selectedWheels;
            for (var i = 0; allWheels.length > i; i++) {
                if (allWheels[i].ID == activeWheels) {
                    selectedWheels = allWheels[i];
                }
            }
            if (typeof selectedWheels.Dirt !== 'undefined') {
                this.props.onChange(selectedWheels.Dirt.ID);
            }
        }).bind(this);
    },
    render: function () {
        var hideClass = '';
        var _this = this;
        function hideButton() {
            var activeAcc =  _this.props.activeAccessories,
                activeWhl =  _this.props.activeWheels,
                defaultAcc = _this.props.defaultAccessories,
                numOfAcc = activeAcc.length,
                checkedAcc = 0,
                dirtWheels = true;
            //Checking default accessories and dirt on wheels.
            for (var i = 0; numOfAcc > i; i++) {
                for (var x = 0; defaultAcc.length > x; x++) {
                    if (defaultAcc[x] == activeAcc[i]) {checkedAcc++;}
                }
                for (var z = 0; _this.props.wheels.length > z; z++) {
                    if (_this.props.wheels[z].ID == activeWhl &&
                        _this.props.wheels[z].Dirt.ID == activeAcc[i]) {
                        checkedAcc++;
                    }
                }
            }
            //Checking if current wheels has dirt object.
            for (var z = 0; _this.props.wheels.length > z; z++) {
                if (_this.props.wheels[z].ID == activeWhl &&
                    typeof _this.props.wheels[z].Dirt.ID == 'undefined') {
                    dirtWheels = false;
                }
            }
            //Return true if you can't set dirt on.
            return checkedAcc !== numOfAcc || !dirtWheels;
        }
        var ready = typeof this.props.wheels !== "undefined" && typeof this.props.wheels !== "undefined";
        if (ready) {
            if (hideButton()) {
                hideClass = ' hide';
            }
            if (_this.props.activeDirt !== null) {
                hideClass = hideClass + ' active';
            }
        }
        var button = React.createElement("a", {className: "dirt-button" + hideClass, onClick: this.toggleDirt()}, 
            "GET DIRTY"
        );
        return React.createElement("div", {className: "dirt-toggle"}, 
            button
        )
    }
});
