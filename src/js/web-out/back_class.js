var Back = React.createClass({displayName: "Back",
    go: function (e) {
        e.preventDefault();
        navigate('/cars/');
    },
    render: function(){
        if (typeof window.wrap == 'undefined') {
            return React.createElement("a", {className: "models", onClick: this.go}, 
                React.createElement("i", {className: "icon"}), 
            "Model list"
            );
        } else {
            return false;
        }
    }
});