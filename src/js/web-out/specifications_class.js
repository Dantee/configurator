var Specifications = React.createClass({displayName: "Specifications",
    hideSpecClass: 'hide',
    hidden: false,
    firstLoad: true,
    getSpec: function (type) {
        if (typeof this.props.specifications !== 'undefined')
            return this.props.specifications[type];
    },
    hide: '',
    toggle: function () {
        this.hidden = !this.hidden;
        this.hide = 'hide';
        this.forceUpdate();
    },
    specs: function () {
        if (this.hidden)
            return '';
        else
            return this.hideSpecClass;
    },
    render: function () {
        return React.createElement("div", {className: "specifications " + this.specs()}, 
            React.createElement("a", {onClick: this.toggle, className: "spec-toggle"}, "SPECIFICATIONS"), 
            React.createElement("div", {className: "faker"}), 
            React.createElement("table", null, 
                React.createElement("tr", null, 
                    React.createElement("td", null, "Variant:"), 
                    React.createElement("td", null, this.getSpec('variant'))
                ), 
                React.createElement("tr", null, 
                    React.createElement("td", null, "Accessories:"), 
                    React.createElement("td", null, this.getSpec('accessories'))
                )
            )
        )
    }
});