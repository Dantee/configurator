var VariantPick = React.createClass({displayName: "VariantPick",
    variantClick: function (vari) {
        return (function () {
            if (this.props.active != vari.ID)
                this.props.onChange(vari.ID);
        }).bind(this);
    },
    render: function () {
        var children = (this.props.variants || []).map((function (vari) {
            var cName = this.props.active == vari.ID ? " active" : "";
            return React.createElement("a", {className: "variant-choice" + cName, 
            onClick: this.variantClick(vari), 
            key: vari.ID}, 
                React.createElement("img", {src: vari.Thumbnail}), 
                React.createElement("span", null, vari.Title)
            )
        }).bind(this));
        return React.createElement("section", {className: "variant-pick"}, 
            children
        );
    }
});
