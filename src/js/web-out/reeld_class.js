var Reeld = React.createClass({displayName: "Reeld",
    getInitialState: function () {
        return {
            el: null,
            loader: 0,
            hidden: 0
        }
    },
    componentDidMount: function () {
        //var preloadedLayers = [this.props.wheels];
        //if (typeof this.props.wrap != "undefined") {
        //    preloadedLayers.push(this.props.wrap);
        //}
        var reeldel = $(this.refs.reeld.getDOMNode()).reeld({
            'autoLoad': false,
            'mf': this.props.manifest,
            'base_uri': 'reeld-data',
            'preloadedLayers': [],
            'colour': this.props.colour,
            'reel_opts': {
                'cursor': 'url("reeld/reeld.cur"), auto'
            }
        });
        //this.startLoader(true);

        //reeldel.reeld('atomicSwap', [null, this.props.wheels, (function () {
        //    this.stopLoader(true);
        //}).bind(this)]);

        //if (typeof this.props.wrap !== "undefined") {
        //    this.startLoader(true);
        //    reeldel.reeld('atomicSwap', [null, this.props.wrap, (function () {
        //        this.stopLoader(true);
        //    }).bind(this)]);
        //}

        for (var i = 0; i < this.props.accessories.length; i++) {
            this.startLoader(true);
            reeldel.reeld('atomicSwap', [null, this.props.accessories[i], (function () {
                this.stopLoader(true);
            }).bind(this)]);
        }

        reeldel.on('load.start', (function () {
            this.startLoader();
        }).bind(this));
        reeldel.on('load.end', (function () {
            this.stopLoader();
        }).bind(this));
    },
    componentWillReceiveProps: function (np) {
        var reeldel = $(this.refs.reeld.getDOMNode()),
            hide = false,
            rotate = null;
        if (np.colour != this.props.colour) {
            reeldel.reeld('colour', np.colour);
            hide = true;
        }
        //if (np.wheels != this.props.wheels) {
        //    this.startLoader(hide);
        //    var rot = this.props.manifest.layers[np.wheels]['rot'];
        //    if (rot !== null) rotate = rot;
        //reeldel.reeld('atomicSwap', [this.props.wheels, np.wheels, (function () {
        //    this.stopLoader(hide);
        //    if (rotate !== null) reeldel.reeld('goToFrame', [{
        //        frame_time: 50, target: rotate
        //    }]);
        //    rotate = null;
        //}).bind(this)]);
        //}
        var toggledAs = _.xor(np.accessories, this.props.accessories);


        for (var i = 0; i < toggledAs.length; i += 1) {
            if (_.contains(this.props.accessories, toggledAs[i])) {
                reeldel.reeld('toggleLayer', [toggledAs[i]]);
                //var related = this.props.manifest.layers[toggledAs[i]]['related'];
                //for (var j = 0; j < related.length; j++) {
                //    reeldel.reeld('disableLayer', [related[j]]);
                //}
                //var addAfterRemove = this.props.manifest.layers[toggledAs[i]]['addAfterRemove'];
                //for (var j = 0; j < addAfterRemove.length; j++) {
                //    reeldel.reeld('toggleLayer', [addAfterRemove[j]]);
                //}
            } else {
                this.startLoader(hide);

                //var conflicts = this.props.manifest.layers[toggledAs[i]]['conflicts'];
                //for (var j = 0; j < conflicts.length; j++) {
                //    reeldel.reeld('disableLayer', [conflicts[j]]);
                //}
                //
                //var related = this.props.manifest.layers[toggledAs[i]]['related'];
                //for (var j = 0; j < related.length; j++) {
                //    reeldel.reeld('toggleLayer', [related[j]]);
                //}
                //
                var rot = this.props.manifest.layers[toggledAs[i]]['rot'];
                if (rot !== null) rotate = rot;

                var _this = this, _i = i;

                if (rotate !== null) {
                    reeldel.reeld('goToFrame', [{
                        frame_time: 50, target: rotate
                    }, function () {
                        reeldel.reeld('atomicSwap', [null, toggledAs[_i], function () {
                            _this.stopLoader(hide);
                        }]);
                    }]);
                } else {
                    reeldel.reeld('atomicSwap', [null, toggledAs[_i], function () {
                        _this.stopLoader(hide);
                    }]);
                }
                rotate = null;



            }
        }
        this.forceUpdate();
    },
    startLoader: function (hide) {
        this.state.loader += 1;
        if (hide === true) this.state.hidden += 1;

        var i = this.refs.loading.getDOMNode();
        //$(i).css({
        //    'background': "rgba(255, 255, 255, .3) url('assets/loader.gif') no-repeat center center",
        //    'top': 0,
        //    'bottom': 0,
        //    'left': 0,
        //    'right': 0,
        //    'position': 'absolute',
        //    'z-index': '2005',
        //    '-webkit-transition': 'all 100ms',
        //    '-moz-transition': 'all 100ms',
        //    '-o-transition': 'all 100ms',
        //    'transition': 'all 100ms'
        //});
        $(i).show();
        if (this.state.hidden > 0)
            $(this.refs.reeld.getDOMNode()).css('visibility', 'hidden');
    },
    stopLoader: function (hide) {
        this.state.loader -= 1;
        if (hide === true) this.state.hidden -= 1;
        if (this.state.loader <= 0)
            $(this.refs.loading.getDOMNode()).hide();
        if (this.state.hidden <= 0)
            $(this.refs.reeld.getDOMNode()).css('visibility', '');
    },
    componentWillUnmount: function () {
        $(this.refs.reeld.getDOMNode()).reeld('destroy', []);
    },
    shouldComponentUpdate: function (nP, nS) {
        if (!_.isEqual(nP.manifest, this.props.manifest)) return true;
        return false;
    },
    prevFrame: function () {
        var reeldel = $(this.refs.reeld.getDOMNode());
        var timer = setInterval(function () {
            reeldel.reeld('frame', reeldel.reeld('frame') - 1);
        }, 100);
        $(document.body).mouseup(function () {
            clearTimeout(timer);
        });
    },
    nextFrame: function () {
        var reeldel = $(this.refs.reeld.getDOMNode());
        var timer = setInterval(function () {
            reeldel.reeld('frame', reeldel.reeld('frame') + 1);
        }, 100);
        $(document.body).mouseup(function () {
            clearTimeout(timer);
        });
    },
    render: function () {
        return React.createElement("div", {id: "reeld-wrapper", className: this.props.openedMenu() ? 'opened' : 'closed'}, 
            React.createElement("div", {id: "reeld", ref: "reeld"}), 
            React.createElement("a", {className: "next-frame left", onMouseDown: this.nextFrame}), 
            React.createElement("a", {className: "next-frame right", onMouseDown: this.prevFrame}), 
            React.createElement("div", {id: "load_indicator", ref: "loading"}, 
                React.createElement("div", {className: "wrap"}, 
                    React.createElement("div", {className: "cssload-thecube"}, 
                        React.createElement("div", {className: "cssload-cube cssload-c1"}), 
                        React.createElement("div", {className: "cssload-cube cssload-c2"}), 
                        React.createElement("div", {className: "cssload-cube cssload-c4"}), 
                        React.createElement("div", {className: "cssload-cube cssload-c3"})
                    )
                )
            )
        );
    }
});
