var Share = React.createClass({displayName: "Share",
    url: '',
    opened: false,
    startShare: function () {
        return (function () {
            (function(_this){
                setInterval(function(){
                    _this.url = window.parent.location.href;
                },1000);
            })(this);

            this.url = window.parent.location.href;
            this.opened = !this.opened;
            this.forceUpdate();
        }).bind(this);
    },
    openFacebookShare: function () {
        return (function () {
            this.url = window.parent.location.href;
            window.open('https://www.facebook.com/dialog/share?' +
                'app_id=1513892125530821' +
                '&display=popup' +
                '&href=' + encodeURIComponent(this.url),
                'facebook-share', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=370,width=1000');
        }).bind(this);
    },
    render: function () {
        return React.createElement("div", {className: "share-button"}, 
            React.createElement("a", {onClick: this.startShare()}, "Share"), 
            React.createElement("div", {className: this.opened ? "share-window opened" : "share-window"}, 
                React.createElement("div", {className: "input-block"}, 
                    React.createElement("input", {type: "text", disabled: "disabled", value: this.url})
                ), 
                React.createElement("div", {className: "social-block"}, 
                    React.createElement("a", {className: "fb", onClick: this.openFacebookShare()}), 
                    React.createElement("a", {className: "done", onClick: this.startShare()}, "Done")
                )
            )
        )
    }
});