var AccessoriesPick = React.createClass({displayName: "AccessoriesPick",
    mixins: [PaginateMixin],
    accessoryClick: function (acc) {
        return (function () {
            this.props.onChange(acc.ID);
        }).bind(this);
    },
    pageMove: function (amt) {
        return (function (e) {
            e.preventDefault();
            var pages = this.paginated(this.props.accessories);
            this.state.page += amt;
            if (this.state.page < 0) this.state.page = 0;
            if (!pages.hasOwnProperty(this.state.page)) this.state.page = pages.last;
            this.forceUpdate();
        }).bind(this);
    },
    render: function () {
        var children;
            children = (this.props.accessories || []).map((function (acc) {
                var cName = this.props.active == null ||
                this.props.active.indexOf(acc.ID) < 0 ? "" : " active";
                var image = {backgroundImage: 'url("'+ acc.Thumbnail +'")'};
                if (!acc.IsDefault) {
                    return React.createElement("li", {key: acc.ID, onClick: this.accessoryClick(acc), style: image, className: cName}, 
                        React.createElement("div", {className: "title"}, acc.Title)
                    );
                }
            }).bind(this));

        return React.createElement("section", {className: "accessory-pick"}, 
            React.createElement("ul", null, 
                children
            )
        );
    }
});
