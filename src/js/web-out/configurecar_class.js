var ConfigureCar = React.createClass({displayName: "ConfigureCar",
    propTypes: {
        'config': React.PropTypes.string.isRequired,
        'back': React.PropTypes.string.isRequired
    },
    configuration: {},
    setHash: function () {
        var configureHash = 'configure=v-' + this.configuration['v'] + ';c-' + this.configuration['c'] + ';w-' + this.configuration['w'] + ';a-' + this.configuration['a'];
        if (typeof window.wrap == 'undefined') {
            window.parent.location.hash = configureHash;
        } else {
            window.parent.location.hash = 'wrap=' + window.wrap + '&' + configureHash;
        }
    },
    getInitialState: function () {
        return {};
    },
    componentWillMount: function () {
        this.variantChange(this.props.config);
    },
    variantChange: function (config) {
        var c = config.split(',');
        var variant, colour, accessories, wheels;
        for (var i = 0; c.length > i; i++) {
            if (c[i].substr(0, 1) == 'v') {
                variant = c[i].replace('v-', '');
            }
            //if (c[i].substr(0,1) == 'w') {
            //    wheels = c[i].replace('w-', '');
            //}
            if (c[i].substr(0, 1) == 'c') {
                colour = c[i].replace('c-', '');
            }
            if (c[i].substr(0, 1) == 'a') {
                accessories = c[i].replace('a-', '');
            }
        }
        var _this = this;


        superagent.get('api/VariantData/' + variant)
            .end((function (data) {
                var vardata = JSON.parse(data.text);

                this.state['dirt'] = null;
                this.state['vardata'] = vardata;
                this.state['manifest'] = generateMF(vardata);


                //SPECS SUMMARY
                this.state['specifications'] = {};
                this.state['specifications']['variant'] = vardata['variant'].Title;
                this.state['specifications']['accessories'] = 'None';
                this.state['activeVariant'] = variant;
                this.state['colour'] = vardata['colour'];

                //SPECS TABLE
                this.state['specifications-table'] = vardata['specifications'];

                if (vardata['colour'].indexOf(this.state['activeColour']) < 0) {
                    //this.state['activeColour'] = vardata['colour'][0].ID;
                }
                //this.state['specifications']['colour'] = vardata['colour'][0].Title;

                if (typeof colour !== 'undefined') {
                    this.state['activeColour'] = colour;
                    this.state['specifications']['colour'] = _this.state.manifest.bases[colour].title;
                }

                this.state['bodies'] = vardata['bodies'];
                this.state['accessories'] = vardata['accessories'];
                this.state['defaultAccessories'] = [];
                for (var i = 0; this.state['accessories'].length > i; i++) {
                    if (this.state['accessories'][i].IsDefault) {
                        this.state['defaultAccessories'].push(this.state['accessories'][i].ID);
                    }
                }
                this.state['activeAccessories'] = _.pluck(
                    _.filter(vardata['accessories'], function (acc) {
                        return acc.IsDefault && solveConstraints(vardata, acc.ID, vardata['colour'][0].ID);
                    }),
                    'ID'
                );
                if (typeof accessories !== 'undefined' && accessories !== '') {
                    this.state['activeAccessories'] = accessories.split('+');

                    var accTitles = [];
                    for (var i = 0; i < this.state['activeAccessories'].length; i++) {
                        var id = this.state['activeAccessories'][i];
                        if (this.state['defaultAccessories'].length !== 0) {
                            var defaultAcc = false;
                            for (var x = 0; x < this.state['defaultAccessories'].length; x++) {
                                if (this.state['defaultAccessories'][x] == id) {
                                    defaultAcc = true;
                                }
                            }
                            if (!defaultAcc) {
                                accTitles.push(_this.state.manifest.layers[id].title);
                            }
                        } else {
                            accTitles.push(_this.state.manifest.layers[id].title);
                        }
                        this.state['specifications']['accessories'] = accTitles.join(', ');
                        if (accTitles.length == 0) {
                            this.state['specifications']['accessories'] = 'None';
                        }
                    }
                    if (this.state['activeAccessories'].length == 0) {
                        this.state['specifications']['accessories'] = 'None';
                    }
                }


                if (typeof window.wrap !== "undefined") {
                    this.state['wrap'] = window.wrap;
                    var path = 'http://www.isuzuutes.co.nz:8080/export/' + window.wrap + '/';
                    //var path = 'http://wraps/export/' + window.wrap + '/';
                    superagent.get(path + 'manifest').end((function (data) {
                        var wrapData = JSON.parse(data.text);
                        this.state['manifest'].layers[wrap] = {
                            'z': wrapData.z,
                            'is': wrapData.is,
                            'lp': 'sprite.png',
                            'base_uri': path
                        };
                        this.forceUpdate();
                    }).bind(this));
                } else {
                    this.forceUpdate();
                }
                this.configuration = {
                    'v': this.state['activeVariant'],
                    'c': this.state['activeColour'],
                    'w': this.state['activeWheels'],
                    'a': this.state['activeAccessories'].join('+')
                };
                //this.setHash(this.configuration);
            }).bind(this));
    },
    clearAccessories: function () {
        if (this.state['defaultAccessories'].length == 0) {
            this.state['activeAccessories'] = [];
        } else {
            this.state['activeAccessories'] = [];
            for (var i = 0; this.state['defaultAccessories'].length > i; i++) {
                this.state['activeAccessories'].push(this.state['defaultAccessories'][i]);
            }
        }
        this.state['specifications']['accessories'] = 'None';

        this.configuration['a'] = '';
        //this.setHash();
    },
    colourChange: function (colourID) {
        this.state['specifications']['colour'] = this.state.manifest.bases[colourID].title;
        this.state['activeColour'] = colourID;
        this.state['activeWheels'] = solveConstraints(this.state.vardata,
            this.state['activeWheels'], colourID);
        this.state['activeAccessories'] = _.filter(
            _.map(this.state['activeAccessories'], function (v) {
                return solveConstraints(this.state.vardata, v,
                    this.state['activeColour']);
            }, this));
        this.forceUpdate();
        //this.scheduleThumbnail();
        this.configuration['c'] = this.state['activeColour'];
        //this.setHash();

    },
    accessoryChange: function (accID) {
        if (this.state['dirt'] !== null) {
            this.state['dirt'] = null;
            this.clearAccessories();
        }
        if (this.state['activeAccessories'] == null) {
            this.state['activeAccessories'] = [accID];
        } else {
            this.state['activeAccessories'] = _.xor(this.state['activeAccessories'], [accID]);
        }
        var conflicts = this.state.manifest.layers[accID]['conflicts'];
        var related = this.state.manifest.layers[accID]['related'];
        var addAfterRemove = this.state.manifest.layers[accID]['addAfterRemove'];

        this.state['activeAccessories'] = _.remove(this.state['activeAccessories'], function (acc) {
            return !_.contains(conflicts, acc);
        });
        if (_.contains(this.state['activeAccessories'], accID)) {
            this.state['activeAccessories'] = _.union(this.state['activeAccessories'], related);
        } else {
            this.state['activeAccessories'] = _.union(this.state['activeAccessories'], addAfterRemove);
            this.state['activeAccessories'] = _.remove(this.state['activeAccessories'], function (acc) {
                return !_.contains(related, acc);
            });
        }

        //Setting up specifications accessories titles
        var accTitles = [];
        for (var i = 0; i < this.state['activeAccessories'].length; i++) {
            var id = this.state['activeAccessories'][i];
            if (this.state['defaultAccessories'].length !== 0) {
                var defaultAcc = false;
                for (var x = 0; x < this.state['defaultAccessories'].length; x++) {
                    if (this.state['defaultAccessories'][x] == id) {
                        defaultAcc = true;
                    }
                }
                if (!defaultAcc) {
                    accTitles.push(this.state.manifest.layers[id].title);
                }
            } else {
                accTitles.push(this.state.manifest.layers[id].title);
            }
            this.state['specifications']['accessories'] = accTitles.join(', ');
            if (accTitles.length == 0) {
                this.state['specifications']['accessories'] = 'None';
            }
        }
        if (this.state['activeAccessories'].length == 0) {
            this.state['specifications']['accessories'] = 'None';
        }

        this.forceUpdate();
        this.configuration['a'] = this.state['activeAccessories'].join('+');
        //this.setHash();

        //this.scheduleThumbnail();
    },
    scheduleThumbnail: function () {
        superagent.post("Thumbnail").send({
            colourID: this.state.activeColour,
            wheelID: this.state.activeWheels,
            accessories: this.state.activeAccessories || [],
            wrap: this.state.wrap,
            size: 'large'
        }).end((function (data) {
            this.state['image_url'] = data.url;
            this.forceUpdate();
        }).bind(this));
    },
    getSpec: function (type) {
        if (typeof this.state.specifications !== 'undefined')
            return this.state.specifications[type];
    },
    openedMenu: true,
    getMenuStatus: function () {
        return this.openedMenu;
    },
    openMenu: function () {
        this.openedMenu = !this.openedMenu;
        this.forceUpdate();
    },
    render: function () {
        var accessories = _.filter(this.state.accessories, function (v) {
            if (v.ColourID.length == 0) return true;
            if (v.IsDefault) return false;
            return _.contains(v.ColourID, this.state['activeColour']);
        }, this);
        return React.createElement("div", {id: "car-configure"}, 
            React.createElement("div", {className: "picker-container"}, 
                React.createElement("div", {className: "header " + (this.openedMenu ? 'opened' : 'closed')}, 
                    React.createElement("h1", null, 
                    this.getSpec('variant')
                    ), 
                    React.createElement("hr", null)
                ), 
                React.createElement(PickSection, {
                openMenu: this.openMenu, 
                openedMenu: this.getMenuStatus, 
                colour: this.state.colour, 
                colourChange: this.colourChange, 
                activeColour: this.state.activeColour, 
                accessories: accessories, 
                accessoryChange: this.accessoryChange, 
                activeAccessories: this.state.activeAccessories, 
                summary: this.state.specifications, 
                specifications: this.state['specifications-table'], 
                variant: this.getSpec('variant')}
                ), 
                this.state.manifest != null ?
                    React.createElement(Reeld, {
                    openMenu: this.openMenu, 
                    openedMenu: this.getMenuStatus, 
                    manifest: this.state.manifest, 
                    colour: this.state.activeColour, 
                    accessories: this.state.activeAccessories, 
                    wrap: this.state.wrap, 
                    key: this.state.activeVariant}) : null, 
                React.createElement("div", {className: "footer " + (this.openedMenu ? 'opened' : 'closed')}, 
                    React.createElement("div", {className: "share"}, 
                        React.createElement("span", {className: "share-icon"}), 
                        React.createElement("span", {className: "text"}, "Share"), 
                        React.createElement("div", {className: "icons"}, 
                            React.createElement("a", {className: "email"}), 
                            React.createElement("a", {className: "facebook"}), 
                            React.createElement("a", {className: "twitter"}), 
                            React.createElement("a", {className: "pinterest"})
                        )
                    )
                )

            )
        );

    }

});