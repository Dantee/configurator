/** @jsx React.DOM */
"use strict";

var PaginateMixin = {
    propTypes: {
        perPage: function (ps, pN) {
            if (pN in ps && ps[pN] < 3) {
                throw new Error('You cannot have less than 3 items per page');
            }
        }
    },
    getInitialState: function () {
        return {
            page: 0
        };
    },
    getDefaultProps: function () {
        return {
            // WARNING: including the arrows, should not be less than 3!
            perPage: 10
        };
    },
    paginated: function (list) {
        if (list.length == 0) return {0: [], last: 0};
        var flPages = this.props.perPage - 1,
            mPages = this.props.perPage - 2,
            pageCount = Math.ceil(((list.length - 2 * flPages) / mPages) + 1),
            obj = _.groupBy(list, (function (e, k, l) {
                //e = object itself, k = item number, l = list array
                //Collecting first page
                if (k < flPages) return 0;
                //Put last objects to the second page
                if (pageCount == 1) return 1;
                //Filling the last page
                if (pageCount > 1 && k > (l.length - ((l.length - flPages) - ((pageCount - 1) * mPages)))) {
                    return pageCount;
                }
                //Adding middle pages
                k = k - flPages;
                return ~~(k / mPages) + 1;
            }).bind(this));
        obj.last = pageCount;
        return obj;
    }
};
