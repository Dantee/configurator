/** @jsx React.DOM */
"use strict";

var generateMF = function (vd) {
    var bs = vd.bodies, wh = vd.wheels, ac = vd.accessories, cs = vd.colour,
        bases = {}, layers = {};

    for (var i = 0; i < bs.length; i += 1) {
        var title, id = bs[i]['ColourID'];
        for (var x = 0; x < cs.length; x++) {
            if (cs[x]['ID'] == id)
                title = cs[x]['Title'];
        }
        bases[bs[i]['ColourID']] = {
            bp: '',
            h: bs[i]['Height'],
            w: bs[i]['Width'],
            is: bs[i]['Images'],
            'title': title
        };
    }
    //
    //for (var i = 0; i < wh.length; i += 1) {
    //    layers[wh[i]['ID']] = {
    //        'z': wh[i]['zIndex'],
    //        'lp': wh[i]['Image'],
    //        'is': wh[i]['Map'],
    //        'rot': wh[i]['Frame'],
    //        'title': wh[i]['Title']
    //    };
    //}

    for (var i = 0; i < ac.length; i += 1) {
        layers[ac[i]['ID']] = {
            //'z': ac[i]['zIndex'],
            'zArray': ac[i]['zArray'],
            'lp': ac[i]['Image'],
            'is': ac[i]['Map'],
            'rot': ac[i]['Frame'],
            'conflicts': ac[i]['ConflictingAccessories'],
            'related': ac[i]['RelatedAccessories'],
            'addAfterRemove': ac[i]['AddAfterRemove'],
            'title': ac[i]['Title']
        };
    }

    ////Getting dirt layers
    //for (var i = 0; i < wh.length; i += 1) {
    //    layers[wh[i]['Dirt']['ID']] = {
    //        'z': wh[i]['Dirt']['zIndex'],
    //        'lp': wh[i]['Dirt']['Image'],
    //        'is': wh[i]['Dirt']['Map'],
    //        'rot': wh[i]['Dirt']['Frame']
    //    }
    //}
    return {'bases': bases, 'layers': layers};
};

var solveConstraints = function (vd, id, colour) {
    var name, constraintvoided = false, accessory = false;

    // Solve accessory contstraint, see whether ID is accessory ID.

    // Get current acc name and whether colour constraints are voided.
    $.each(vd.accessories, function (k, v) {
        if (v.ID == id) {
            accessory = true;
            name = v.Title;
            if (v.ColourID == null || v.ColourID.length == 0) return;
            constraintvoided = $.inArray(colour, v.ColourID) < 0;
        }
    });

    if (accessory) {
        // if constraint is voided, try solving it with another layer
        // with same name.
        if (constraintvoided) {
            id = null;
            $.each(vd.accessories, function (k, v) {
                var satisfiesconstraints = v.ColourID == null
                    || v.ColourID.length == 0
                    || $.inArray(colour, v.ColourID) > -1;
                if (v.Title == name && satisfiesconstraints) {
                    id = v.ID;
                    return false;
                }
            });
            return id;
        } else {
            return id;
        }
    } else { // We’re dealing with wheels
        $.each(vd.wheels, function (k, v) {
            if (v.ID == id) {
                if (v.ColourID == null || v.ColourID.length == 0
                    || $.inArray(colour, v.ColourID) > -1) return false;
                else {
                    id = null;

                    $.each(vd.wheels, function (k_, v_) {
                        // Find wheels with same title
                        if (v_.Title != v.Title) return;

                        if (v_.ColourID == null || v_.ColourID.length == 0
                            || $.inArray(colour, v_.ColourID) > -1) {
                            id = v_.ID;
                            return false;
                        }
                    });

                    // We could not find wheels with same name and solved
                    // constraints. Go for any wheels that work.
                    if (id == null) {
                        $.each(vd.wheels, function (k_, v_) {
                            if (v_.ColourID == null || v_.ColourID.length == 0
                                || $.inArray(colour, v_.ColourID) > -1) {
                                id = v_.ID;
                                return false;
                            }
                        });
                    }
                }
                return false;
            }
        });
        return id;
    }
};