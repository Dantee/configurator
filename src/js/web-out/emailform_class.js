var EmailForm = React.createClass({displayName: "EmailForm",
    get_colour: function () {
        for (var i = 0; i < this.props.colours.length; i++) {
            if (this.props.colours[i].ID == this.props.activeColour) {
                return this.props.colours[i].Title;
            }
        }
    },
    get_wheel: function () {
        for (var i = 0; i < this.props.wheels.length; i++) {
            if (this.props.wheels[i].ID == this.props.activeWheel) {
                return this.props.wheels[i].Title;
            }
        }
    },
    get_variant: function () {
        for (var i = 0; i < this.props.variants.length; i++) {
            if (this.props.variants[i].ID == this.props.activeVariant) {
                return this.props.variants[i].Title;
            }
        }
    },
    get_accessories: function () {
        var text = "";
        if (typeof this.props.activeAccessories != "undefined") {
            for (var j = 0; j < this.props.activeAccessories.length; j++) {
                for (var i = 0; i < this.props.accessories.length; i++) {
                    if (this.props.activeAccessories[j] == this.props.accessories[i].ID) {
                        if (text == "") {
                            text = this.props.accessories[i].Title;
                        } else {
                            text = text + ", " + this.props.accessories[i].Title;
                        }

                    }
                }
            }
        }
        return text;
    },
    handleSubmit: function (e) {
        e.preventDefault();

        var name = this.refs.name.getDOMNode().value.trim();
        var surname = this.refs.surname.getDOMNode().value.trim();

        var email = this.refs.email.getDOMNode().value.trim();
        var colour = this.get_colour();
        var wheels = this.get_wheel();
        var accessories = this.get_accessories();
        var variant = this.get_variant();
        var image_url = this.props.image_url;

        $.ajax({
            url: "SendUserEmail",
            type: "POST",
            async: false,
            data: {
                configuration: {
                    colour: colour,
                    wheels: wheels,
                    accessories: accessories,
                    variant: variant,
                    image_url: image_url
                },
                name: name,
                surname: surname,
                email: email
            },
            success: function (data) {
                $("#email-form span, #email-form input").hide();
                $("<div id='success-message'>").html("Your message has been sent successfuly.").appendTo("#email-form");
            }
        });
        return false;
    },
    render: function () {
        return React.createElement("form", {id: "email-form", onSubmit: this.handleSubmit, style: {'display': 'none'}}, 
            React.createElement("div", {className: "row"}, "First Name", 
                React.createElement("input", {type: "text", name: "name", ref: "name"})
            ), 
            React.createElement("div", {className: "row"}, "Surname", 
                React.createElement("input", {type: "text", name: "surname", ref: "surname"})
            ), 
            React.createElement("div", {className: "row"}, "Your Email", 
                React.createElement("input", {type: "email", name: "email", ref: "email"})
            ), 
            React.createElement("div", {className: "row"}, 
                React.createElement("input", {type: "submit", value: "Send"})
            )
        );
    }
});