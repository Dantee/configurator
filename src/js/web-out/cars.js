/** @jsx React.DOM */
"use strict";

var CategoryList = React.createClass({displayName: "CategoryList",
    render: function () {
        return React.createElement("div", {id: "car-pick"}, 
            React.createElement("div", {className: "background"}), 
            React.createElement("div", {className: "content"}, 
                React.createElement("div", {className: "title"}, 
                    React.createElement("h1", null, "Select your pioneer"), 
                    React.createElement("hr", null)
                ), 
                React.createElement("div", {className: "car-wrapper"}, 
                    React.createElement(CarCategories, null)
                )
            )
        );
    }
});

var CarCategories = React.createClass({displayName: "CarCategories",
    getInitialState: function () {
        return {"Categories": []};
    },
    componentWillMount: function () {
        superagent.get('api/AllVariants').end((function (data) {
            this.setState(JSON.parse(data.text));
        }).bind(this));
    },
    render: function () {
        var categories = _.values(this.state.Categories).map(function (cat) {
            return React.createElement("div", {key: cat.ID, className: "category"}, 
                React.createElement(CarList, {id: cat.ID, variants: cat.Variants})
            )
        });
        return React.createElement("div", {className: "categories"}, categories);
    }
});

var CarList = React.createClass({displayName: "CarList",
    render: function () {
        var cars = this.props.variants.map(function (car) {
            return React.createElement(Car, {id: car.ID, thumbnail: car.Thumbnail, 
            title: car.Title, key: car.ID, modelID: car.ModelID, specifications: car.Specifications})
        });
        return React.createElement("div", {className: "car-list"}, cars);
    }
});

var Car = React.createClass({displayName: "Car",
    go: function (e) {
        var config = 'v-' + this.props.id;
        e.preventDefault();
        navigate('/configure/' + config);
    },
    render: function () {
        return React.createElement("a", {key: this.props.id, onClick: this.go, className: "car-container"}, 
            React.createElement("img", {src: this.props.thumbnail}), 
            React.createElement("span", null, this.props.title)
        );
    }
});