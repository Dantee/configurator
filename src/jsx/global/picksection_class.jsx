var PickSection = React.createClass({
    openedMenu: false,
    currentTab: 'accessories',
    pagination: false,
    getInitialState: function () {
        return {
            firstName: '',
            lastName: '',
            mobile: '',
            email: '',
            hearMore: false
        }
    },
    setActive: function (tab) {
        return (function () {
            if (this.currentTab !== tab) {
                this.currentTab = tab;
            }
            this.forceUpdate();
        }).bind(this);
    },
    toggleMenu: function () {
        return (function () {
            this.props.openMenu();
        }).bind(this);
    },
    getSpec: function (type) {
        if (typeof this.props.summary !== 'undefined')
            switch (type) {
                case 'variant':
                    return this.props.summary.variant;
                    break;
                case 'accessories':
                    return this.props.summary.accessories;
                    break;
            }
    },
    submiting: false,
    formError: false,
    formValid: false,
    validateForm: function () {
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        if (
            this.state.firstName !== '' &&
            this.state.lastName !== '' &&
            this.state.mobile !== '' &&
            this.state.email !== ''
        ) {
            if (!validateEmail(this.state.email)) {
                this.formValid = false;
                this.formError = 'Please enter correct email address.';
            } else {
                this.formValid = true;
            }
        } else {
            this.formValid = false;
            this.formError = 'Please fill all the fields.';
        }
        this.forceUpdate();
    },
    showError: function () {
        return this.formError ? 'show' : 'hide';
    },
    loading: function () {
        return this.submiting ? 'loading' : '';
    },
    firstNameChange: function (e) {
        this.formError = false;
        this.setState({'firstName': e.target.value});
    },
    lastNameChange: function (e) {
        this.formError = false;
        this.setState({'lastName': e.target.value});
    },
    mobileChange: function (e) {
        this.formError = false;
        this.setState({'mobile': e.target.value});
    },
    emailChange: function (e) {
        this.formError = false;
        this.setState({'email': e.target.value});
    },
    hearMoreChange: function (e) {
        this.formError = false;
        this.setState({'hearMore': e.target.checked});
    },
    resetForm: function () {
        this.setState({
            firstName: '',
            lastName: '',
            mobile: '',
            email: '',
            hearMore: false
        })
    },
    submitForm: function (e) {
        e.preventDefault();
        this.formError = false;
        this.validateForm();
        var request;
        if (this.formValid) {
            this.submiting = true;
            this.forceUpdate();
            request = $.ajax({
                url: e.target.action,
                dataType: 'json',
                data: {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    mobile: this.state.mobile,
                    email: this.state.email,
                    hearMore: this.state.hearMore
                }
            });
            request.done((function () {
                this.submiting = false;
                this.resetForm();
            }).bind(this));

            request.fail((function () {
                this.submiting = false;
                this.formError = 'Something went wrong, please try again later!';
                this.forceUpdate();
            }).bind(this));
        }

    },
    render: function () {
        var _this = this;
        var className = function (tab) {
            if (tab == _this.currentTab) {
                return ' active';
            } else {
                return '';
            }
        };
        var openClass = function () {
            if (_this.props.openedMenu()) {
                return ' opened';
            } else {
                return '';
            }
        };
        var specifications = (this.props.specifications || []).map((function (spec) {
            var data = spec.Data.map((function (data) {
                return <li>
                    <div className="leftside">
                        <span>{data.Title}</span>
                    </div>
                    <div className="rightside">
                        <span>{data.Description}</span>
                    </div>
                </li>
            }).bind(this));

            var openedSpec = function (id) {
                if (_this.state !== null) {
                    return _this.state.spec == id;
                }
            };
            var toggleOpen = function (id) {
                return (function () {
                    if (_this.state !== null) {
                        if (_this.state.spec == id) {
                            return _this.setState({spec: null});
                        }
                    }
                    _this.setState({spec: id});
                });
            };

            return <div className="spec">
                <div className={"title " + openedSpec(spec.Title)} onClick={toggleOpen(spec.Title)}>{spec.Title}</div>
                <div className={"data " + openedSpec(spec.Title)}>
                    <ul>
                        {data}
                    </ul>
                </div>
            </div>
        }).bind(this));

        return <div className={"section-holder" + openClass()}>
            <a className="menu-toggler" onClick={this.toggleMenu()}>
                <div className="dashes">
                    <hr/>
                    <hr/>
                    <hr/>
                </div>
            Menu
            </a>
            <div className="menu-content">
                <Back/>
                <div className="tabs">
                    <ul>
                        <li className={className('accessories')} onClick={this.setActive('accessories')}>
                            <i className="icon accessories"></i>
                        Accessories
                        </li>
                        <li className={className('specifications')} onClick={this.setActive('specifications')}>
                            <i className="icon specifications"></i>
                        Specifications
                        </li>
                        <li className={className('test-ride')} onClick={this.setActive('test-ride')}>
                            <i className="icon test-ride"></i>
                        Test ride
                        </li>
                    </ul>
                </div>
                <div className='tabs-wrapper'>
                    <div className={'tab-content' + className('accessories')}>
                        <AccessoriesPick accessories={this.props.accessories}
                        onChange={this.props.accessoryChange}
                        active={this.props.activeAccessories}
                        pagination={this.pagination}/>
                    </div>
                    <div className={'tab-content specifications' + className('specifications')}>
                        <div className="specs">
                            {specifications}
                        </div>
                    </div>
                    <div className={'tab-content test-ride' + className('test-ride')}>
                        <div className="heading">
                            <h2>Test ride your</h2>
                            <h2 className="title">{this.props.variant}</h2>
                            <hr/>
                        </div>
                        <form onSubmit={this.submitForm} action="form/submit" noValidate >
                            <label>
                                <span>First Name</span>
                                <input value={this.state.firstName} onChange={this.firstNameChange} type="text"/>
                            </label>
                            <label>
                                <span>Last Name</span>
                                <input value={this.state.lastName} onChange={this.lastNameChange} type="text"/>
                            </label>
                            <label>
                                <span>Mobile</span>
                                <input value={this.state.mobile} onChange={this.mobileChange} type="tel"/>
                            </label>
                            <label>
                                <span>Email</span>
                                <input value={this.state.email} onChange={this.emailChange} type="email"/>
                            </label>
                            <label>
                                <input value={this.state.hearMore} onChange={this.hearMoreChange} type="checkbox"/>
                                <span className="checkbox-text">Sign me up to receive special promotions, latest products and to hear more from Honda Motorbikes</span>
                            </label>
                            <div className={'actions ' + this.loading()}>
                                <input type="submit" value="Make a booking"/>
                                <span className={this.showError()}>{this.formError}</span>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>;
    }
});