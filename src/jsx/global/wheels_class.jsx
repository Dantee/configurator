var WheelsPick = React.createClass({
    wheelClick: function (whl) {
        return (function () {
            if (this.props.active != whl.ID)
                this.props.onChange(whl.ID);
        }).bind(this);
    },
    render: function () {
        var children = (this.props.wheels || []).map((function (whl) {
            var cName = this.props.active == whl.ID ? " active" : "";
            return <a className={"wheel-choice" + cName}
            onClick={this.wheelClick(whl)}
            key={whl.ID}>
                <img src={whl.Thumbnail} />
                <span>{whl.Title}</span>
            </a>
        }).bind(this));
        return <section className="wheel-pick">
            <div className="cat-title">Wheels</div>
            <div className="cat-list">
                {children}
            </div>
        </section>;
    }
});
