/** @jsx React.DOM */
"use strict";

var CategoryList = React.createClass({
    render: function () {
        return <div id="car-pick">
            <div className="background"></div>
            <div className="content">
                <div className="title">
                    <h1>Select your pioneer</h1>
                    <hr/>
                </div>
                <div className="car-wrapper">
                    <CarCategories />
                </div>
            </div>
        </div>;
    }
});

var CarCategories = React.createClass({
    getInitialState: function () {
        return {"Categories": []};
    },
    componentWillMount: function () {
        superagent.get('api/AllVariants').end((function (data) {
            this.setState(JSON.parse(data.text));
        }).bind(this));
    },
    render: function () {
        var categories = _.values(this.state.Categories).map(function (cat) {
            return <div key={cat.ID} className="category">
                <CarList id={cat.ID} variants={cat.Variants} />
            </div>
        });
        return <div className="categories">{categories}</div>;
    }
});

var CarList = React.createClass({
    render: function () {
        var cars = this.props.variants.map(function (car) {
            return <Car id={car.ID} thumbnail={car.Thumbnail}
            title={car.Title} key={car.ID} modelID={car.ModelID} specifications={car.Specifications} />
        });
        return <div className="car-list">{cars}</div>;
    }
});

var Car = React.createClass({
    go: function (e) {
        var config = 'v-' + this.props.id;
        e.preventDefault();
        navigate('/configure/' + config);
    },
    render: function () {
        return <a key={this.props.id} onClick={this.go} className="car-container">
            <img src={this.props.thumbnail} />
            <span>{this.props.title}</span>
        </a>;
    }
});