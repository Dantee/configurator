var Reeld = React.createClass({
    getInitialState: function () {
        return {
            el: null,
            loader: 0,
            hidden: 0
        }
    },
    componentDidMount: function () {
        var reeldel = $(this.refs.reeld.getDOMNode()).reeld({
            'autoLoad': false,
            'mf': this.props.manifest,
            'base_uri': 'reeld-data',
            'preloadedLayers': [],
            'colour': this.props.colour,
            'reel_opts': {
                'cursor': 'url("reeld/reeld.cur"), auto'
            }
        });

        for (var i = 0; i < this.props.accessories.length; i++) {
            this.startLoader(true);
            reeldel.reeld('atomicSwap', [null, this.props.accessories[i], (function () {
                this.stopLoader(true);
            }).bind(this)]);
        }

        reeldel.on('load.start', (function () {
            this.startLoader();
        }).bind(this));
        reeldel.on('load.end', (function () {
            this.stopLoader();
        }).bind(this));
    },
    componentWillReceiveProps: function (np) {
        var reeldel = $(this.refs.reeld.getDOMNode()),
            hide = false,
            rotate = null;
        if (np.colour != this.props.colour) {
            reeldel.reeld('colour', np.colour);
            hide = true;
        }

        var toggledAs = _.xor(np.accessories, this.props.accessories);


        for (var i = 0; i < toggledAs.length; i += 1) {
            if (_.contains(this.props.accessories, toggledAs[i])) {
                reeldel.reeld('toggleLayer', [toggledAs[i]]);
            } else {
                this.startLoader(hide);
                var rot = this.props.manifest.layers[toggledAs[i]]['rot'];
                if (rot !== null) rotate = rot;

                var _this = this, _i = i;

                if (rotate !== null) {
                    reeldel.reeld('goToFrame', [{
                        frame_time: 50, target: rotate
                    }, function () {
                        reeldel.reeld('atomicSwap', [null, toggledAs[_i], function () {
                            _this.stopLoader(hide);
                        }]);
                    }]);
                } else {
                    reeldel.reeld('atomicSwap', [null, toggledAs[_i], function () {
                        _this.stopLoader(hide);
                    }]);
                }
                rotate = null;



            }
        }
        this.forceUpdate();
    },
    startLoader: function (hide) {
        this.state.loader += 1;
        if (hide === true) this.state.hidden += 1;

        var i = this.refs.loading.getDOMNode();
        $(i).show();
        if (this.state.hidden > 0)
            $(this.refs.reeld.getDOMNode()).css('visibility', 'hidden');
    },
    stopLoader: function (hide) {
        this.state.loader -= 1;
        if (hide === true) this.state.hidden -= 1;
        if (this.state.loader <= 0)
            $(this.refs.loading.getDOMNode()).hide();
        if (this.state.hidden <= 0)
            $(this.refs.reeld.getDOMNode()).css('visibility', '');
    },
    componentWillUnmount: function () {
        $(this.refs.reeld.getDOMNode()).reeld('destroy', []);
    },
    shouldComponentUpdate: function (nP, nS) {
        if (!_.isEqual(nP.manifest, this.props.manifest)) return true;
        return false;
    },
    prevFrame: function () {
        var reeldel = $(this.refs.reeld.getDOMNode());
        var timer = setInterval(function () {
            reeldel.reeld('frame', reeldel.reeld('frame') - 1);
        }, 100);
        $(document.body).mouseup(function () {
            clearTimeout(timer);
        });
    },
    nextFrame: function () {
        var reeldel = $(this.refs.reeld.getDOMNode());
        var timer = setInterval(function () {
            reeldel.reeld('frame', reeldel.reeld('frame') + 1);
        }, 100);
        $(document.body).mouseup(function () {
            clearTimeout(timer);
        });
    },
    render: function () {
        return <div id="reeld-wrapper" className={this.props.openedMenu() ? 'opened' : 'closed'}>
            <div id="reeld" ref="reeld" />
            <a className="next-frame left" onMouseDown={this.nextFrame} />
            <a className="next-frame right" onMouseDown={this.prevFrame} />
            <div id="load_indicator" ref="loading" >
                <div className="wrap">
                    <div className="cssload-thecube">
                        <div className="cssload-cube cssload-c1"></div>
                        <div className="cssload-cube cssload-c2"></div>
                        <div className="cssload-cube cssload-c4"></div>
                        <div className="cssload-cube cssload-c3"></div>
                    </div>
                </div>
            </div>
        </div>;
    }
});
