var Share = React.createClass({
    url: '',
    opened: false,
    startShare: function () {
        return (function () {
            (function(_this){
                setInterval(function(){
                    _this.url = window.parent.location.href;
                },1000);
            })(this);

            this.url = window.parent.location.href;
            this.opened = !this.opened;
            this.forceUpdate();
        }).bind(this);
    },
    openFacebookShare: function () {
        return (function () {
            this.url = window.parent.location.href;
            window.open('https://www.facebook.com/dialog/share?' +
                'app_id=1513892125530821' +
                '&display=popup' +
                '&href=' + encodeURIComponent(this.url),
                'facebook-share', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=370,width=1000');
        }).bind(this);
    },
    render: function () {
        return <div className="share-button">
            <a onClick={this.startShare()}>Share</a>
            <div className={this.opened ? "share-window opened" : "share-window"}>
                <div className="input-block">
                    <input type="text" disabled="disabled" value={this.url}/>
                </div>
                <div className="social-block">
                    <a className="fb" onClick={this.openFacebookShare()}></a>
                    <a className="done" onClick={this.startShare()}>Done</a>
                </div>
            </div>
        </div>
    }
});