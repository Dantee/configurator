var PrintPage = React.createClass({
    render: function () {
        return <section className="print-page">
            <div className="cat-title">Accessories</div>
        </section>;
    }
});

var PrintPage = React.createClass({
    getDefaultProps: function () {
        return {
            'print': window.showPrint != null
        };
    },
    handlePrint: function (e) {
        e.preventDefault();
        window.print();
    },
    render: function () {
        return <section className="print-page">
            <div className="cat-title">Share</div>
            <div className="cat-list">
                {!this.props.print ? <div /> :
                    <a className="print-link" onClick={this.handlePrint}>
                        <img className="print-image" src="assets/printer.png" />
                    </a>}
                <a className="print-link">
                    <img className="email-image" href="#email-form" src="assets/email.png" />
                </a>
            </div>
        </section>;
    }
});