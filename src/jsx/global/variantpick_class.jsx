var VariantPick = React.createClass({
    variantClick: function (vari) {
        return (function () {
            if (this.props.active != vari.ID)
                this.props.onChange(vari.ID);
        }).bind(this);
    },
    render: function () {
        var children = (this.props.variants || []).map((function (vari) {
            var cName = this.props.active == vari.ID ? " active" : "";
            return <a className={"variant-choice" + cName}
            onClick={this.variantClick(vari)}
            key={vari.ID}>
                <img src={vari.Thumbnail} />
                <span>{vari.Title}</span>
            </a>
        }).bind(this));
        return <section className="variant-pick">
            {children}
        </section>;
    }
});
