var ColourPick = React.createClass({
    colourClick: function (col) {
        return (function () {
            if (this.props.active != col.ID)
                this.props.onChange(col.ID);
        }).bind(this);
    },
    render: function () {
        var children = (this.props.colours || []).map((function (col) {
            var cName = this.props.active == col.ID ? " active" : "";
            return <a className={"colour-choice" + cName}
            onClick={this.colourClick(col)}
            key={col.ID}>
                <img src={col.Thumbnail} />
                <span>{col.Title}</span>
            </a>
        }).bind(this));
        return <section className="colour-pick">
            <div className="cat-title">Colour</div>
            <div className="cat-list">
                {children}
            </div>
        </section>;
    }
});