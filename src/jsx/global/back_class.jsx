var Back = React.createClass({
    go: function (e) {
        e.preventDefault();
        navigate('/cars/');
    },
    render: function(){
        if (typeof window.wrap == 'undefined') {
            return <a className="models" onClick={this.go}>
                <i className="icon"></i>
            Model list
            </a>;
        } else {
            return false;
        }
    }
});