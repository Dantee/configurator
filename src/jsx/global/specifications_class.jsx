var Specifications = React.createClass({
    hideSpecClass: 'hide',
    hidden: false,
    firstLoad: true,
    getSpec: function (type) {
        if (typeof this.props.specifications !== 'undefined')
            return this.props.specifications[type];
    },
    hide: '',
    toggle: function () {
        this.hidden = !this.hidden;
        this.hide = 'hide';
        this.forceUpdate();
    },
    specs: function () {
        if (this.hidden)
            return '';
        else
            return this.hideSpecClass;
    },
    render: function () {
        return <div className={"specifications " + this.specs()}>
            <a onClick={this.toggle} className="spec-toggle">SPECIFICATIONS</a>
            <div className="faker"></div>
            <table>
                <tr>
                    <td>Variant:</td>
                    <td>{this.getSpec('variant')}</td>
                </tr>
                <tr>
                    <td>Accessories:</td>
                    <td>{this.getSpec('accessories')}</td>
                </tr>
            </table>
        </div>
    }
});