var Summary = React.createClass({

    get_variant: function () {
        for (var i = 0; i < this.props.variants.length; i++) {
            if (this.props.variants[i].ID == this.props.activeVariant) {
                return this.props.variants[i].Title;
            }
        }
    },
    get_accessories: function () {
        var text = "";
        if (typeof this.props.activeAccessories != "undefined") {
            for (var j = 0; j < this.props.activeAccessories.length; j++) {
                for (var i = 0; i < this.props.accessories.length; i++) {
                    if (this.props.activeAccessories[j] == this.props.accessories[i].ID) {
                        if (text == "") {
                            text = this.props.accessories[i].Title;
                        } else {
                            text = text + ", " + this.props.accessories[i].Title;
                        }

                    }
                }
            }
        }
        return text;
    },
    render: function () {
        return <section className="summary">
            <div className="cat-title">Summary</div>
            <div className="cat-list">
                <div className="summary-info">
                    <div>Variant: {this.get_variant()}</div>
                    <div>Accessories: {this.get_accessories()}</div>
                </div>
            </div>
        </section>;
    }
});