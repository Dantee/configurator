"use strict";

(function () {

    var cars = function () {
        React.renderComponent(CategoryList(), document.body);
    };
    var configureCar = function (config) {
        var config = config.replace('configure=', '');
        React.renderComponent(ConfigureCar({
            'config': config,
            'back': window.parent.location.hash === '' ? '/cars' : 'history.back'
        }), document.body);
    };

    window.addEventListener('load', function () {
        // TODO: Piping perhaps?
        crossroads.addRoute('/cars', cars);

        // TODO: Construct the full URI?
        //crossroads.addRoute('/configure/{carId}', configureCar);
        //crossroads.addRoute('/configure/{carId}/{varId}', configureCar);
        crossroads.addRoute('/configure/{config}', configureCar);

        if (window.parent.location.hash !== '') {
            var hash = window.parent.location.hash.replace(/^#/, '');

            var hashes = hash.split('&');
            var config = {};
            for (var i = 0; hashes.length > i; i++) {
                var key = hashes[i].substr(0, hashes[i].indexOf('='));
                config[key] = (hashes[i].substr(hashes[i].indexOf('=')+1, hashes[i].length)).split(';');
            }

            if (typeof config['wrap'] !== 'undefined') {
                var hash = config['wrap'][0];
                window.wrap = hash;
            }

            if (typeof config['configure'] !== 'undefined') {
                crossroads.parse('/configure/' + config['configure']);
            }

        } else {
            history.replaceState({path: '/cars'}, "TV", "/configurator/");
            crossroads.parse('/cars');
        }

        crossroads.bypassed.add(function (uri) {
            console.log(uri, 'does not exist, but request was made to render it');
        });

        window.navigate = function (path, scroll) {
            if (scroll == null || scroll !== false) {
                window.scrollTo(0, 0);
            }
            if (path == 'history.back') {
                history.back();
            } else {
                history.pushState({path: path}, "TV", '/configurator/');
                crossroads.parse(path);
            }
        };
        window.onpopstate = function (event) {
            if (event.state !== null && typeof event.state.path != "undefined") {
                crossroads.parse(event.state.path);
            }
        };
    });

    function createDiv(id) {
        var div = document.createElement('div');
        div.setAttribute('id', id);
        return div;
    }

    function getId(id) {
        return document.getElementById(id);
    }

    function removeId(id) {
        if (getId(id) !== null) {
            getId(id).parentNode.removeChild((getId(id)));
        }
    }

    var mobile = createDiv('mobile-rotate');
    var cell = document.createElement('div');
    var image = document.createElement('span');
    cell.appendChild(image);
    mobile.appendChild(cell);

    function checkVertical() {
        var turnMobile = (window.innerWidth < window.innerHeight && window.innerWidth < 900);
        if (turnMobile) {
            if (getId('mobile-rotate') == null) {
                document.body.appendChild(mobile);
            }
        } else {
            removeId('mobile-rotate');
        }
    }
    window.onload = function () {
        checkVertical();
    };
    window.onresize = function (e) {
        checkVertical();
    };

})();