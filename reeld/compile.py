import argparse
import logging
import os
import os.path
import sys
import tempfile
import math
import itertools
import shutil
import json
import subprocess
from functools import partial
from PIL import Image, ImageFile
import concurrent.futures
from multiprocessing import cpu_count

def compile_layer_set(path, args):
    data = {'is': []}
    layer_id = os.path.split(path)[1]
    try:
        data['z'] = int(layer_id.split('-')[0])
        layer_name = '-'.join(layer_id.split('-')[1:])
    except ValueError:
        logger.warn('Layer ID %s does not contain Z-index, defaulting to 10',
                    layer_id)
        data['z'] = 10
        layer_name = layer_id
    layer_path = os.path.join('layers', layer_name, 'sprite.png')

    try: os.makedirs(os.path.join(args.tmp, 'layers', layer_name))
    except: pass

    frames = list(sorted(os.path.join(path, f) for f in os.listdir(path)))
    logger.debug('Found %d frames at %s', len(frames), path)

    imgs = list(map(Image.open, frames))
    for i in imgs:
        if args.width is None:
            pass
        elif i.size[0] > args.width:
            i.thumbnail((args.width, float('inf')), Image.ANTIALIAS)
        else:
            logger.warn('Original frame size is less than maximum width' +
                        ' specified, not scaling')

    bboxes = [i.getbbox() for i in imgs]
    # Decide whether we stacking everything horizontally or vertically
    wsum = sum(bbox[2] - bbox[0] for bbox in bboxes if bbox)
    wmax = max(bbox[2] - bbox[0] for bbox in bboxes if bbox)
    hsum = sum(bbox[3] - bbox[1] for bbox in bboxes if bbox)
    hmax = max(bbox[3] - bbox[1] for bbox in bboxes if bbox)
    # Firefox can't support images wider or taller than 2^15-1 pixels.
    # TODO: fill a bug for that.
    # NOTE: We don't handle a case where image gets taller than 2^15-1 as
    # images usually are either very tall or very wide, but not both.
    h_stacking = wsum * hmax <= hsum * wmax and wsum < 2**15 - 1

    logger.debug('%s stacking chosen for %s',
                 'Horizontal' if h_stacking else 'Vertical', layer_id)

    res = Image.new("RGBA", (wsum, hmax,) if h_stacking else (wmax, hsum,))
    offset = 0
    target_path = os.path.join(args.tmp, layer_path)
    for frame, bbox in zip(imgs, bboxes):
        if not bbox:
            data['is'].append(None)
            continue
        data['is'].append({
            'w': bbox[2] - bbox[0],
            'h': bbox[3] - bbox[1],
            'iw': frame.size[0],
            'ih': frame.size[1],
            'cxo': bbox[0],
            'cyo': bbox[1]
        })
        data['is'][-1]['xo' if h_stacking else 'yo'] = offset
        data['is'][-1]['yo' if h_stacking else 'xo'] = 0
        frame = frame.crop(bbox)
        res.paste(frame, (offset, 0,) if h_stacking else (0, offset,))
        frame.save(target_path + str(len(data['is']) - 1), "PNG", optimize=True)
        offset += frame.size[0] if h_stacking else frame.size[1]
    res.save(target_path, "PNG", optimize=True)
    if not args.no_lossy_png:
        result_size = os.path.getsize(target_path)
        if result_size > 2**20: # 1MB
            logger.warn('Trying lossy PNG approach to reduce size for %s',
                         layer_id)
            # Try saving with palette instead
            # PIL produces visually unacceptable results, therefore we
            # shell out to pngquant.
            subprocess.call(['pngquant', '256', target_path, '--ext=.png',
                             '--speed=1', '-f'])
            logger.debug('Lossy approach reduced file to %2.1f%% of original',
                         os.path.getsize(target_path) / result_size * 100)
    logger.info('Finished processing %s layer', layer_id)
    return (layer_name, data)


def compile_base_set(directory, args):
    data = {'is': []}
    base_id = os.path.split(directory)[1]
    base_path = os.path.join('bases', base_id)
    # TODO: race?
    try:
        os.makedirs(os.path.join(args.tmp, base_path))
    except:
        pass

    images = list(sorted((d, list(sorted(fs))) for d, ds, fs in
                                   os.walk(directory, followlinks=True) if fs))
    logger.info('Compiling %s base with layers in %s order', base_id,
                ', '.join(o[0] for o in images))
    layers = list(zip(*[[os.path.join(i[0], p) for p in i[1]] for i in images]))
    logger.info('Found %d frames for %s', len(layers), base_id)
    if len(layers) != max([len(s[1]) for s in images]):
        logger.warn('Inconsistent frame lengths, expect issues')

    finalw, finalh = 0, 0
    for key, layer in enumerate(layers):
        imgs = list(map(Image.open, layer))
        if len(set(i.size for i in imgs)) > 1:
            logger.warn('Not all layers for %s are of identical size, '+
                        'expect issues', base_id)
        minw = min(i.size[0] for i in imgs)
        minh = min(i.size[1] for i in imgs)
        res = Image.new("RGBA", (minw, minh,))
        for i in imgs:
            if i.size != res.size:
                logger.warn('Layer sizes for %s mismatch, resizing', base_id)
                i.resize(res.size)
            res = Image.alpha_composite(res, i.convert(res.mode))
        if args.width is None:
            pass
        elif res.size[0] > args.width:
            res.thumbnail((args.width, float('inf')), Image.ANTIALIAS)
        else:
            logger.warn('Original frame size is less than maximum width' +
                        ' specified, not scaling')
        img_name = '{0}.jpg'.format(key)
        old_mb = ImageFile.MAXBLOCK
        ImageFile.MAXBLOCK = res.size[0] * res.size[1]
        target_path = os.path.join(args.tmp, base_path, img_name)
        res.save(target_path, "JPEG", quality=args.quality, optimize=True,
                 progressive=True)
        ImageFile.MAXBLOCK = old_mb
        # Set some metadata
        if (finalw, finalh,) != res.size and finalw and finalh:
            logger.warn('Base %s sizes between frames differ, expect issues',
                        base_id)
        if (finalw, finalh,) < res.size:
            finalw, finalh = res.size
        data['is'].append(img_name)
    data['w'], data['h'] = finalw, finalh
    logger.info('Finished processing %s base', base_id)
    return (base_id, data,)

if __name__ == "__main__":
    curr_dir = os.path.dirname(os.path.abspath(__name__))
    desc = "Compile the images for reeld library"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-t', '--target', default=curr_dir,
                        help='Directory which contains source directories')
    parser.add_argument('-o', '--output',
                        default=os.path.join(curr_dir, 'reeld_out'),
                        help='Path to output the result to')
    parser.add_argument('--separate-manifests', action='store_true',
                        help='Separate manifests for each element')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Set logging level to debug')
    parser.add_argument('--please-do-rewrite', action='store_true',
                        help='Will proceed even if output directory is not empty')
    parser.add_argument('--no-tmp-directory', action='store_true',
                        help='Compile directly into output directory')
    parser.add_argument('-w', '--width', type=int,
                        help='Scale images to some width')
    parser.add_argument('-q', '--quality', default=85,
                        type=int,
                        help='Quality (15-100) of generated jpeg images')
    parser.add_argument('-p', '--processes', type=int, choices=range(1, 9),
                        default=min(cpu_count(), 8),
                        help='Use a provided number of processes');
    parser.add_argument('-l', '--no-lossy-png', action="store_true",
                        help='Do not lossy compress big PNG images')
    parser.add_argument('ids', metavar='ID', type=str, nargs='*',
                        help='Specify specific layers or bases to compile')

    args = parser.parse_args()
    formatter = logging.Formatter(fmt="{levelname[0]}: {message}", style="{")
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG if args.verbose else logging.WARN)
    logger.addHandler(handler)

    # Early path checks
    if not os.path.exists(args.target):
        logger.error('%s does not exist!', args.target)
        sys.exit(1)
    if not os.path.isdir(args.target):
        logger.error('%s is not a directory!', args.target)
        sys.exit(1)
    bases = [os.path.join(args.target, 'bases'), False]
    layers = [os.path.join(args.target, 'layers'), False]
    for _dir in (bases, layers,):
        if not os.path.exists(_dir[0]):
            logger.warn('%s does not exist', _dir[0])
            _dir[1] = True
        elif not os.path.isdir(_dir[0]):
            logger.warn('%s is not a _dirory', _dir[0])
            _dir[1] = True
    if bases[1] and layers[1]:
        logger.error('Nothing to do')
        sys.exit(1)

    # Early checks for output
    if not os.path.exists(args.output):
        logger.debug('Creating output directory %s', args.output)
        try:
            os.makedirs(args.output)
        except:
            logger.exception('Could not create output directory %s', args.output)
            sys.exit(1)
    elif not os.path.isdir(args.output):
        logger.error('%s is not a directory', args.output)
        sys.exit(1)
    elif not os.access(args.output, os.W_OK | os.X_OK):
        logger.error('%s is not writeable', args.output)
        sys.exit(1)
    if not args.please_do_rewrite:
        if os.listdir(args.output):
            logger.error('%s is not empty', args.output)
            sys.exit(1)
    if not args.no_tmp_directory:
        args.tmp = tempfile.mkdtemp()
    else:
        args.tmp = args.output

    if len(args.ids) != 0 and not args.please_do_rewrite:
        logger.error('Selective compilation can only be run with ' +
                     '--please_do_rewrite')
        sys.exit(1)
    if len(args.ids) != 0 and args.no_tmp_directory:
        logger.error('Selective compilation can only be run without ' +
                     '--no-tmp-directory')
        sys.exit(1)

    # Misc. checks
    if not 15 <= args.quality <= 100:
        logger.error('Quality %d is out of range', args.quality)
        sys.exit(1)


    manifest = {'bases': {}, 'layers': {}}
    try:
        os.setpgrp()
    except:
        logger.warn('Will be unable to terminate all subprocesses on failure')

    executor = concurrent.futures.ProcessPoolExecutor(args.processes)

    process_all = len(args.ids) == 0
    bases_ids = {}
    layers_ids = {}
    if not process_all:
        for i in args.ids:
            split = i.split('/')
            if split[0] == 'bases' and len(split) == 2:
                bases_ids[split[1]] = False
            elif split[0] == 'layers' and len(split) == 2:
                layers_ids[split[1]] = False
            else:
                logger.error('IDs should be provided in format bases/id or layers/id.')
                sys.exit(1)


    got_bases = got_layers = False

    # Collect bases
    if not bases[1]:
        logger.info('Collecting bases')
        bases_dir = bases[0]
        collected_bases = set(os.listdir(bases_dir))
        bases = {os.path.join(bases_dir, l) for l in collected_bases}
        special = set(filter(lambda x: os.path.split(x)[1][0] == '_', bases))
        bases -= special
        logger.debug('Found these special directories: %s', ', '.join(special))
        logger.debug('Found these bases: %s', ', '.join(bases))

        if process_all: got_bases = True
        elif len(bases_ids) != 0:
            for b in collected_bases:
                if b in bases_ids:
                    bases_ids[b] = True
            if not all(bases_ids.values()):
                logger.error('Can’t find some of the provided base IDs')
                sys.exit(1)
            bases = {os.path.join(bases_dir, l) for l in bases_ids.keys()}
            got_bases = True


    # Collect layers
    if not layers[1]:
        logger.info('Collecting layers')
        layers_dir = layers[0]
        collected_layers = set(os.listdir(layers_dir))
        layers = {os.path.join(layers_dir, l) for l in collected_layers}
        logger.debug('Found these layers: %s', ', '.join(layers))
        # Create output directory
        try:
            os.makedirs(os.path.join(args.tmp, 'layers'))
        except:
            pass

        if process_all: got_layers = True
        elif len(layers_ids) != 0:
            for l in collected_layers:
                if l in layers_ids:
                    layers_ids[l] = True
            if not all(layers_ids.values()):
                logger.error('Can’t find some of the provided layer IDs')
                sys.exit(1)
            layers = {os.path.join(layers_dir, l) for l in layers_ids.keys()}
            got_layers = True


    # Start layer generation jobs
    fsb, fsl = {}, {}
    if got_bases and len(bases) != 0:
        logger.info('Generating base backgrounds')
        fsb = {executor.submit(compile_base_set, b, args): b for b in bases}
    if got_layers and len(layers) != 0:
        logger.debug('Generating layers')
        fsl = {executor.submit(compile_layer_set, l, args): l for l in layers}
    logger.debug('Waiting for jobs to complete')
    fs = fsl.copy()
    fs.update(fsb)


    try:
        # Wait until all jobs finish or one fails
        t = concurrent.futures.FIRST_EXCEPTION
        res = concurrent.futures.wait(fs, return_when=t)
    except:
        for f in fs:
            f.cancel()
        for i in executor._processes.keys():
            try:
                os.kill(i, 9)
            except:
                logger.exception('Failed at killing a child')
        shutil.rmtree(args.tmp)
        os._exit(1)

    # Check if anything has failed. Terminate
    for r in res.done:
        if r.exception():
            try:
                raise r.exception()
            except:
                logger.exception('A job has failed, cancelling')
                # Such force exit. Very don't finish processing.
                # Much DIEEEEEEE~
                for f in fs: f.cancel()
                for i in executor._processes.keys():
                    try:
                        os.kill(i, 9)
                    except:
                        logger.exception('Failed at killing a child')
                shutil.rmtree(args.tmp)
                os._exit(1)

    # Generate manifest
    if args.separate_manifests:
        logger.info('Writing manifest files')
        for bp in fsb:
            base_id, result = bp.result()
            with open(os.path.join(args.tmp, 'bases',
                                   base_id, 'manifest'), 'w') as f:
                json.dump(result, f)
        for lp in fsl:
            layer_id, result = lp.result()
            with open(os.path.join(args.tmp, 'layers',
                                   layer_id, 'manifest'), 'w') as f:
                json.dump(result, f)
    else:
        for bp in fsb:
            base_id, result = bp.result()
            manifest['bases'][base_id] = result
        for lp in fsl:
            layer_id, result = lp.result()
            manifest['layers'][layer_id] = result

        # Write the manifest file
        with open(os.path.join(args.tmp, 'manifest.json'), 'w') as f:
            logger.info('Writing the manifest file')
            json.dump(manifest, f)

    if args.tmp != args.output:
        logger.info('Moving contents from temp directory to actual output')
        if args.please_do_rewrite:
            shutil.rmtree(args.output)
            os.makedirs(args.output)
        for res in os.listdir(args.tmp):
            shutil.move(os.path.join(args.tmp, res), args.output)
        os.rmdir(args.tmp)
