Reeld
*****

A 360-like scene. Uses `reel360`_ for many things and augments
it with additional functionality.

.. _reel360: http://reel360.org/reel

Compiler
========

This project contains a rudimentary compiler to generate images in expected
format and manifest for them. Compiler is a regular CLI application written
in Python which in addition to simply preparing images optimises them to be
consumable on the web. The compiler has many flags which influence its
behaviour. You can find out about them by invoking compiler with a ``--help``
switch.

First of all, make sure you have a directory containing ``bases/`` and
``layers/``, directories containing base car images and accessory layers
respectively. The compiler has well specified structure it expects to find
inside those directories.

The output will appear in ``$PWD/reeld_out/`` by default, which should be
made visible by public.

Bases
-----

First type of images compiler will process are base images of cars for which
frames reside inside ``bases/`` directory. Example of a structure under
``bases/`` where ``<frames>`` denotes a list of frame images ::

    bases/
    | first-base-id
    | | 00-layer-one
    | | | <frames>
    | | 20-layer-two
    | | | <frames>
    | | ...
    | second-base-id
    | | 10-layer-one
    | | | <frames>
    | | 99-layer-two
    | | | <frames>
    | | ...
    | ...

Right inside ``bases/`` directory there are directories ``<base-id>/``
containing a set of layer directories containing frames.

Layer directories are sorted by name and then to generate the first frame for
base ``<base-id>`` compiler will pick first frame from first layer and
draw first frame from all other layers in order onto it. The resulting image
is then saved.

This allows you to generate a base image from several layers allowing to
have only a single set of background with only the body frames varying between
bases.

.. note::

    If <base-id> begins with an underscore (``_``) it is ignored and won't
    be considered as a directory containing potential base, therefore you
    can put common base layers such as background under ``bases`` prefixed and
    then symlink it to all bases which needs the layer to be used. This serves
    as a kind of configuration.

.. note::

    As layer directories are sorted by name before use, prefixing all of them
    with two-number sequence is recommended to have control over which layer
    is used first and in what order layers are layered.

Layers
------

Layers are the images you can put on top of the car in runtime. Due to way
layers are saved only small pieces of car should be made into layer instead of
made a different set of base images. For example a good candidate for being
a layer is a towbar, while a different set of tires already pushes the limit
and anything bigger than that should be considered for a different set
of base images.

An example of structure under ``layers/`` where ``<frames>`` denotes a set
of frame images: ::

    layers/
    | 01-layer-id
    | | <frames>
    | 10-another-layer-id
    | | <frames>
    | ...

Every directory under ``layers/`` have a name in format
``<z-index>-<layer-id>``. You may omit ``<z-index>`` in which case it will
be defaulted to ``10``. The z-index has no meaning during compilation, but
is used during runtime to decide in what order to draw layers onto the base
image while ``<layer-id>`` shall be used to toggle layer visibility during
runtime. ``<layer-id>`` must be unique.

.. note::
    In case generated layer is over 2MiB in size, compiler will make an attempt
    to do a lossy conversion to PNG8 and replace alpha transparency data with
    either fully transparent or fully opaque. If it is unacceptable you may
    pass a switch to compiler in order to disable the lossy conversion.

Manifest
========

The manifest file contains data serialised as JSON. Example of manifest file
without data about images (``is``) follows (comments about each key is provided
inline after ``//``) ::

    {
      "bases": { // Constant key expected to be present in all manifests.
        "gray": { // ID of the base.
          "is": [],           // Image filenames for each frame
          "h": 446,           // Height of frame
          "w": 990            // Width of frame
        },
        "blue": {
          "is": [],
          "h": 446,
          "w": 990
        }
      },
      "layers": { // Constant key expected to be present in all manifests.
        "01-antenna": { // ID of the layer
          "z": 1,                       // z-index of the layer
          "is": [],                     // Data about images for each frame
        },
        "10-decal": {
          "z": 10,
          "is": [],
        }
      }
    }

Now you probably noticed the ``is`` for ``bases`` and ``is`` for ``layers`` are
completely different. ``bases.is`` simply contain a list of filenames of images,
while ``layers.is`` is much more complex and contains much more data. An
average element of ``layers.is`` looks like this::

    { "h": 54    // Height of viewport for current frame
    , "xo": 0    // Horizontal offset of data in sprite image
    , "cxo": 125 // Viewport's position from the left inside canvas
    , "iw": 990  // Original canvas width before cutting
    , "ih": 446  // Original canvas height before cutting
    , "cyo": 206 // Viewport's position from the top inside canvas
    , "w": 111   // Width of viewport for current frame
    , "yo": 0    // Vertical offset of data in sprite image
    }

API
===

There are two sides to the API: events and ``.reeld()`` method.

Events
------

Reeld emits numerous events to help user to monitor reeld's state. All events
use jQuery's event infrastructure and obeys its rules.

layer.disable(e, id), layer.enable(e, id)
+++++++++++++++++++++++++++++++++++++++++

``layer.disable`` and ``layer.enable`` are emitted after the layer is
disabled or enabled respectively. You will get disabled/enabled layer's id
as an argument to callback.

Usually this event is emitted after a call to ``toggleLayer``.

change.frame, change._width, change.colour, change.mf, change.<option>
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The ``change.*`` events are emited after properties inside the
context of reeld are changed.

Some of these are more interesting than others. For example ``change._width``
tells user that the reeld plugin's container size has changed, while
``change.mf`` simply tells that manifest file has loaded (useful during the
initialisation, for example).

load.start, load.progress(e, {'loaded': 0, 'total': 36}), load.end
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Load events for base image sets. ``load.start`` and ``load.end`` are
guaranteed to fire no more than once in a pair after ``change.colour``.
If ``load.start`` is not fired, you're guaranteed to not get any other
``load.*`` events and in case ``load.start`` is fired, you're guaranteed to
get a corresponding ``load.end`` emission.

``load.progress`` will be fired between ``load.start`` and ``load.end`` events
with ``{'loaded': integer, 'total': integer}`` argument to inform client of
load progress. Library strives to make progress monotonic.

Properties
----------

There're several properties you may change or read using ``.reeld()`` method.
The syntax is as follows: ::

    $('#reeld-element').reeld('propertyName') // To read
    $('#reeld-element').reeld('propertyName', newValue) // To change

There's only a few properties you can write to while you can read many more.
The writeability will be pointed out along with the documentation of property.

Some of the properties can be set during construction, but cannot be changed
at runtime. If the variable is not writeable, construction is your only chance
to change its value. ::

    $('#reeld-element').reeld({
        'propertyName': value,
        'anotherProp': anotherVal,
        ...
    });

As name construction implies this only applies to the first invocation of
``.reeld()`` for any given element.

colour
++++++

**Writeable**

The ID of base image currently being shown. Writing to this value will change
the base image.

frame
+++++

**Writeable**

Current frame. It is bound to the frame number of underlying reel360 plugin,
but unlike reel360, uses 0-based numbering instead of 1-based one. Setting
this value will simply change the currently visible frame.

base_uri
++++++++

URI used in construction of pathes of manifested images. It should point to
publicly accessible location where output from compiler is located. AJAX
requests to the specified location must be possible. If the ``base_uri`` is not
in the same domain as origin, CORS should be enabled.

manifest
++++++++

Filename of manifest file generated by compiler.

autoLoad
++++++++

Whether the reeld should automatically load manifest from the server with AJAX.
If you can send manifest file over with HTML and provide it to ``mf`` property
during construction, you should set it to false.

mf
++

The manifest object.

reel_opts
+++++++++

Options you can pass over to the underlying reel plugin.

load_indicator
++++++++++++++

Element used as load indicator. It is appropriately placed plain empty div
you should fill (with contents) and style yourself.

It is visible by default and hidden after the plugin is fully initialized. The
client code is then responsible for showing and hiding it.

Methods
-------

There also are some methods you might be interested in. The can be called
using the ``.reeld()`` method for element.

The general syntax is as follows: ::
    $('#reeld-element').reeld('methodName', [argument1, argument2, ....]);

toggleLayer(id)
+++++++++++++++

Enable the layer if it is not enabled and disable otherwise.

atomicSwap(what, with_what, callback)
+++++++++++++++

Swap layer ``with`` with ``with_what`` in such a manner that the time between
dissapearance of ``with`` and rendering of ``with_what`` is minimal. After the
swap ``callback`` is called. Please note, that unlike with toggleLayer, the
operation is asynchronous and actual swap might happen further in the future.

.. note::

    If ``with_what`` is falsy, ``atomicSwap`` simply enables ``with_what``.
    You might be interested in using this function that way if you want to
    get a callback for layer draw.

isLayerVisible(id)
++++++++++++++++++

Returns whether layer is visible or would be visible if it was enabled for
current frame.

goToFrame({'frame_time': 0, 'target': 0, 'easing': 'swing'})
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Spin to a ``target`` frame, optionally with ``easing`` animation giving
``frame_time`` for each frame on average.

If ``frame_time`` is ``0``, then animation is disabled and effect is
instantaneous (equivalent to setting ``frame`` property to ``target``).

The ``change.frame`` event will be emitted for all frames visible during
the transition.
