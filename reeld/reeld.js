"use strict;"
$.fn.reeld = function (option, value) {
    // Methods that can be called by the client.
    // Methods can be called by passing function name as first argument
    // and arguments as a list for second argument.
    var methods = {
        'wrapLayer': function (object) {
            ctx.mf.layers['wrap'] = object;
            ctx.layers.push('wrap');
            this.trigger('layers.enable', 'wrap');
        },
        'toggleLayer': function (id) {
            var index = $.inArray(id, ctx.layers);
            if (index >= 0) {
                ctx.layers.splice(index, 1);
                this.trigger('layers.disable', id);
            } else {
                if (id in ctx.mf.layers) {
                    ctx.layers.push(id);
                    calculateZIndexes();
                    this.trigger('layers.enable', id);
                }
            }
        },
        'disableLayer': function (id) {
            var index = $.inArray(id, ctx.layers);
            if (index >= 0) {
                ctx.layers.splice(index, 1);
                this.trigger('layers.disable', id);
            }
        },
        'atomicSwap': function (wth, swapee, cb) {
            // Swap layer `wth` with `swapee` in such a manner that
            // the time between dissapearance of wth and rendering of swapee
            // is minimal. After the swap cb is called. Please note, that
            // unlike with toggleLayer, the operation is asynchronous and
            // might happen further in the future.
            //
            // If wth is falsy, swapee is simply enabled.
            var layer = getLayer(swapee);
            var doSwap = $.proxy(function () {
                var wthindex = $.inArray(wth, ctx.layers),
                    swapeedex = $.inArray(swapee, ctx.layers);
                if (wth) {
                    if (wthindex >= 0) {
                        ctx.layers.splice(wthindex, 1);
                        this.trigger('layers.disable', wth);
                    }
                }
                if (swapeedex < 0 && swapee in ctx.mf.layers) {
                    ctx.layers.push(swapee);
                    ctx.layers = ctx.layers.sort(function (a, b) {
                        return ctx.mf['layers'][a].zArray[ctx.frame] - ctx.mf['layers'][b].zArray[ctx.frame];
                    });
                    this.trigger('layers.enable', swapee);
                }
                if (cb) cb();
            }, this);
            if (isIE) {
                if (layer.complete) setTimeout(doSwap, ieDrawTimeout);
                else $(layer).one('load', function () {
                    setTimeout(doSwap, ieDrawTimeout)
                });
            } else {
                if (layer.complete) doSwap();
                else $(layer).one('load', doSwap);
            }
        },
        'isLayerVisible': function (id) {
            return ctx.mf['layers'][id].is[ctx.frame] !== null;
        },
        // Spin to the frame, optionally with animation
        'goToFrame': function (opts, cb) {
            opts = $.extend({
                'frame_time': 0,
                'target': 0,
                'easing': 'swing'
            }, opts);
            var reel = $(ctx.img);
            if (opts.frame_time === 0) {
                reel.reel('frame', opts.target);
            } else {
                var curr = ctx.frame + 1, tot = reel.reel('frames')
                to_right = opts.target - curr,
                    to_left = curr - opts.target;
                if (to_right < 0) {
                    to_right += tot;
                }
                if (to_left < 0) {
                    to_left += tot;
                }
                var steps = to_left < to_right ? to_left : to_right,
                    direction = to_left < to_right ? -1 : 1;
                if (ctx.__curr_animation) ctx.__curr_animation.stop();
                ctx.__curr_animation = $({'f': 0}).animate({'f': steps}, {
                    'duration': opts.frame_time * steps,
                    'easing': opts.easing,
                    'step': function () {
                        reel.reel('frame', curr + Math.round(this.f * direction));
                    },
                    'complete': function () {
                        reel.reel('frame', curr + Math.round(this.f * direction));
                        if (cb) cb();
                    }
                });
            }
        },
        'destroy': function () {
            $(ctx.img).unreel();
        },
        'spin': function (d) {
            this.trigger('change.frame', ctx.frame + d);
        }
    };

    // Properties used by reeld
    // You can change them by passing property name as first argument and
    // value as a second.
    var properties = {
        'colour': '',
        'frame': 0
    };

    // Internal functions
    var smallestGif = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
    var doInit = $.proxy(function () {
        // Add load indicator
        ctx.load_indicator.setAttribute('id', 'reeld-load-indicator');
        this.append(ctx.load_indicator);

        this.one('change.mf', $.proxy(function (e, d) {
            // Make sure there is a colour set
            if (!ctx.colour) this.reeld('colour', Object.keys(d.bases)[0]);
            // Handle ids that do not exist in manifest. These might get in
            // during initialisation.
            for (var i = 0; i < ctx.layers.length; i += 1)
                if (!d['layers'].hasOwnProperty(ctx.layers[i]))
                    this.reeld('toggleLayer', [ctx.layers[i]]);
            // And start preloading the layers
            for (var i = 0; i < ctx.preloadedLayers.length; i += 1) {
                if (d['layers'].hasOwnProperty(ctx.preloadedLayers[i]))
                    getLayer(ctx.preloadedLayers[i]);
            }

            // Build DOM
            var bmf = ctx.mf['bases'][ctx.colour];
            ctx.img = document.createElement('img');
            ctx.img.onload = $.proxy(function () {
                $(ctx.load_indicator).hide();
                try {
                    ctx.img.onload = undefined;
                    // IE8.
                } catch (e) {
                }
                this.append(ctx.img);
                // Initialise reel plugin. We'll be piggybacking on
                // it for interactions
                var reel_ = $(ctx.img).reel($.extend({
                    frame: ctx.frame + 1,
                    cursor: 'grab',
                    preloader: 0,
                    brake: 5,
                    images: Array.apply(null, new Array(ctx._bases.length))
                        .map(function () {
                            return smallestGif;
                        })
                }, ctx.reel_opts)).on('frameChange', $.proxy(function (e, _, f) {
                    if (ctx.frame != f - 1) {
                        ctx.frame = f - 1;
                        this.trigger('change.frame', f - 1);
                    }
                }, this));
                // Keep container width cached
                ctx._width = $(this).width();
                $(window).on('resize', $.proxy(function () {
                    // Keep layers container width cached so we wouldn't force
                    // a style recalculation on every frame change.
                    clearTimeout($(window).data('restimeout'));
                    $(window).data('restimeout', setTimeout($.proxy(function () {
                        var width = this.width();
                        if (width != ctx._width) {
                            ctx._width = width;
                            this.trigger('change._width');
                        }
                    }, this), 25));
                }, this));
                this.on('change.colour', onColourChange);
                this.on('change.frame', onFrameChange);
                this.on('change.frame', function (e, f) {
                    reel_.reel('frame', f + 1);
                });
                this.on('change._width', onResize);
                this.on('layers.enable layers.disable', function () {
                    dirtyFrames();
                });
                // Start drawing. onColourChange handles this aspect
                this.trigger('change.colour');
            }, this);
            ctx.img.src = smallestGif;
            ctx.img.width = bmf.w;
            ctx.img.height = bmf.h;
        }, this));

        if (ctx.autoLoad) {
            $.getJSON(ctx.base_uri + ctx.manifest, $.proxy(function (d) {
                ctx['mf'] = d;
                this.trigger('change.mf', d);
            }, this));
        } else {
            if (ctx['mf'] != {}) this.trigger('change.mf', ctx['mf']);
        }
    }, this);

    var drawFrame = function (bmf, frame, num) {
        var dctx = frame.getContext('2d'),
            ondraw = function () {
                drawScaled(dctx, img, 0, 0, frame.width, frame.height);
                drawAccessories(dctx, frame, num);
            },
            img = ctx._bases[num];
        if (typeof FlashCanvas != "undefined") {
            dctx.drawImage(img, 0, 0, frame.width, frame.height);
            drawAccessories(dctx, frame, num);
        } else {
            if (isIE) {
                if (img.complete) setTimeout(ondraw, ieDrawTimeout);
                else $(img).one('load', function () {
                    setTimeout(ondraw, ieDrawTimeout)
                });
            } else {
                if (img.complete) ondraw();
                else $(img).one('load', ondraw);
            }
        }
    };

    var drawAccessories = $.proxy(function (dctx, frame, num) {
        // NOTE: Do NOT use drawScaled for this.
        for (var i = 0; i < ctx.layers.length; i += 1) {
            var layer = getLayer(ctx.layers[i]), draw = (function (i, ld) {
                return function () {
                    if (ld == null) return;
                    var s = ctx._width / ld.iw;
                    dctx.drawImage(i, ld.xo, ld.yo, ld.w, ld.h,
                        ld.cxo * s, ld.cyo * s, ld.w * s, ld.h * s);
                };
            })(layer, ctx.mf['layers'][ctx.layers[i]].is[num]);
            if (typeof FlashCanvas != "undefined") {
                dctx.loadImage(layer.src + num, (function (s, ld) {
                    var sc = ctx._width / ld.iw;
                    dctx.drawImage({'src': s}, ld.xo, ld.yo, ld.w, ld.h,
                        ld.cxo * sc, ld.cyo * sc, ld.w * sc, ld.h * sc);
                })(layer.src + num, ctx.mf['layers'][ctx.layers[i]].is[num]));
            } else {
                if (isIE) {
                    if (layer.complete) setTimeout(draw, ieDrawTimeout);
                    else $(layer).one('load', function () {
                        setTimeout(draw, ieDrawTimeout)
                    });
                } else {
                    if (layer.complete) draw();
                    else $(layer).one('load', draw);
                }
            }
        }
    }, this);

    var getFrame = function (num) {
        var bmf = ctx.mf['bases'][ctx.colour];

        if (isIE) for (var i = 0; i < bmf.is.length; i += 1) {
            // Draw all the other frames too to get rid of flickering in
            // older versions of IE
            if (ctx._frames[i] == null && i != num) {
                var f = ctx._frames[i] = document.createElement('canvas');
                f.width = ctx._width;
                f.height = Math.round(bmf.h * f.width / bmf.w);
                drawFrame(bmf, f, i);
            }
        }

        if (ctx._frames[num] == null) {
            var f = ctx._frames[num] = document.createElement('canvas');
            if (typeof FlashCanvas != "undefined") {
                FlashCanvas.initElement(f);
            }
            f.width = ctx._width;
            f.height = Math.round(bmf.h * f.width / bmf.w);
            drawFrame(bmf, f, num);
            return f;
        } else return ctx._frames[num];
    };

    var dirtyFrames = $.proxy(function () {
        ctx._frames = [];
        this.trigger('change.frame', ctx.frame);
    }, this);

    var getLayer = function (id) {
        if (ctx._layers.hasOwnProperty(id)) return ctx._layers[id];
        else {
            var ni = ctx._layers[id] = document.createElement('img');
            if (typeof ctx.mf['layers'][id]['base_uri'] !== "undefined") {
                ni.src = ctx.mf['layers'][id]['base_uri'] + ctx.mf['layers'][id]['lp'];
            } else {
                ni.src = ctx.base_uri + '/' + ctx.mf['layers'][id]['lp'];
            }
            return ni;
        }
    };

    var calculateZIndexes = function(){
        ctx.layers = ctx.layers.sort(function (a, b) {
            return ctx.mf['layers'][a].zArray[ctx.frame] - ctx.mf['layers'][b].zArray[ctx.frame];
        });
    };

    var onFrameChange = $.proxy(function () {
        calculateZIndexes();
        var nframe = getFrame(ctx.frame);
        if (ctx.__current_frame)
            this[0].replaceChild(nframe, ctx.__current_frame);
        else this.prepend(nframe);
        ctx.__current_frame = nframe;
        console.log('%cFRAME: ' + (ctx.frame + 1), 'background: #222; color: #bada55; font-size: 20px; padding: 5px');
        for (var i = 0; ctx.layers.length > i; i++) {
            console.log('%c'+ctx.mf.layers[ctx.layers[i]].title + ' zIndex is:' + '%c' + ctx.mf.layers[ctx.layers[i]].zArray[ctx.frame], 'background: red; color: white; padding: 2px; font-weight: bold', 'color: red; font-size: 20px; font-weight: bold; padding: 5px');
        }

    }, this);



    var baseImages = function (bmfo) {
        var is = [], bmf = bmfo || ctx.mf['bases'][ctx.colour];
        for (var i = bmf.is.length - 1; i >= 0; i--) {
            is.push(ctx.base_uri + bmf.bp + '/' + bmf.is[i]);
        }
        return is.reverse();
    };

    var onColourChange = $.proxy(function () {
        var bmf = ctx.mf['bases'][ctx.colour],
            timeout = setTimeout($.proxy(function () {
                loadfired = true;
                this.trigger('load.start');
            }, this), bmf.has_been_loaded ? 100 : 0),
            loadfired = false,
            loaded = 0,
            total;

        var wkey = baseImages(bmf).map(function (uri, key) {
            return {'el': uri, 'key': key};
        });

        ctx._bases = byDistance(wkey, ctx.frame).map($.proxy(function (el) {
            var i = document.createElement('img');
            $(i).one('load', $.proxy(function () {
                loaded += 1;
                if (loaded == total && !loadfired) return clearTimeout(timeout);
                else if (loaded == total) {
                    bmf.has_been_loaded = true;
                    this.trigger('load.progress', {
                        'loaded': loaded,
                        'total': total
                    });
                    return this.trigger('load.end');
                } else if (loadfired)
                    return this.trigger('load.progress', {
                        'loaded': loaded,
                        'total': total
                    });
            }, this));
            i.src = el['el'];
            return {'el': i, 'key': el['key']}
        }, this)).sort(function (a, b) {
            return a.key - b.key
        })
            .map(function (a) {
                return a.el
            });

        total = ctx._bases.length;
        dirtyFrames();
    }, this);

    var byDistance = function (array, index) {
        // Sort array with first element at index, then elements around the
        // index with increasing distance (wraps as if array was a ring).
        // E.g. in case of array = [1, 2, 3, 4, 5, 6] and index = 4
        // the result would be something like [5, 4, 6, 3, 1, 2]
        var result = [], d = 1, len = array.length;
        index %= len;
        result.push(array[index]);
        while (true) {
            l = (index + d) % len, r = (index + len - d) % len, d += 1;
            result.push(array[l]);
            if (l == r) break;
            result.push(array[r]);
            if (result.length == len) break;
        }
        return result;
    };

    // Better quality scaling
    var drawScaled = function (target, img, sx, sy, sw, sh, dx, dy, dw, dh) {
        if (sx == null || sy == null || sw == null || sh == null) {
            throw new Error('What the hell are you trying to do?!');
        }
        if (dx == null && dy == null && dw == null && dh == null) {
            dx = sx;
            dy = sy;
            dw = sw;
            dh = sh;
            sx = 0;
            sy = 0;
            sw = img.naturalWidth;
            sh = img.naturalHeight;
        }
        if (dw / sw > 0.8) {
            target.drawImage(img, 0, 0, sw, sh, dx, dy, dw, dh);
            return;
        }
        var temp = document.createElement('canvas'),
            tctx = temp.getContext('2d');
        temp.width = sw;
        temp.height = sh;
        tctx.drawImage(img, sx, sy, sw, sh, 0, 0, sw, sh);
        var stepcount = Math.min(Math.floor(sw / dw), 4) + 1,
            wstep = (sw - dw) / stepcount,
            hstep = wstep * sh / sw;
        for (var i = 1; i < stepcount; i += 1) {
            var swn = sw - wstep,
                shn = sh - hstep;
            tctx.drawImage(temp, 0, 0, sw, sh, 0, 0, swn, shn);
            sw = swn;
            sh = shn;
        }
        target.drawImage(temp, 0, 0, Math.floor(sw), Math.floor(sh),
            dx, dy, dw, dh);
    };

    var onResize = $.proxy(function () {
        dirtyFrames();
    }, this);

    var isIEother = /*@cc_on!@*/0;
    var ieDrawTimeout = 100;
    // Screw you, IE!
    var isIE11 = !(window.ActiveXObject) && "ActiveXObject" in window;
    var isIE = isIEother || isIE11;

    // Initialize context
    // On first initialization you can pass an object of settings
    // to override default values. Some of them are overrideable only during
    // initialization.
    if (this.data('reeld')) {
        var ctx = this.data('reeld');
    } else {
        var ctx = $.extend({
            // Basic options
            'base_uri': '/reeld_out/',
            // Path to manifest file
            'manifest': 'manifest.json',
            // Autoload manifest file?
            // If you set `mf` during initialization yourself you should
            // set this variable to false.
            'autoLoad': true,
            // Initially enabled layers
            'layers': [],
            // Layers that need to be preloaded before the body load
            'preloadedLayers': [],
            // Manifest
            'mf': {},
            // Options for underlying reel
            'reel_opts': {},
            // PRIVATE-ISH
            // Unscaled base images
            '_bases': [],
            // Layer images
            '_layers': {},
            // Frame canvases
            '_frames': [],
            // Load indicator element. Client is responsible for filling
            // it and showing/hiding himself.
            'load_indicator': document.createElement('div')
        }, properties, $.isPlainObject(option) ? option : {});
        this.data('reeld', ctx);
        doInit();
    }

    if (typeof option == "string") {
        // Either function call, property change or property read.
        if (option in methods) {
            var ret = methods[option].apply(this, value);
            if (ret != null) return ret;
        } else if (typeof value !== "undefined") {
            if (option in properties) {
                if (ctx[option] !== value) {
                    ctx[option] = value;
                    this.trigger('change.' + option, value);
                }
            } else throw new Error("Reeld: Option " + option + " does not exist");
        } else return ctx[option];
    }
    return this;
}