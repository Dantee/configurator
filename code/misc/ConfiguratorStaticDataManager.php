<?php

class ConfiguratorStaticDataManager {

    /*
     * General name for manifest file (with or without json extension)
     */
    CONST MANIFEST_FILENAME = 'manifest';

    /*
     * In case we are not supplied with image name in manifest.json, add sprite filename manually
     */
    CONST SPRITENAME = "sprite.png";

    public static function generateCategories() {

        $categories_list = [];
        $categories = DataObject::get('CarConfiguratorCategory')->sort('ConfiguratorOrder', 'ASC');
        foreach ($categories as $category) {
            $categoryData = [];
            foreach ($category->CarConfiguratorModels() as $model) {
                if ($model->ID) {
                    $categories_list[$category->ID] = array('Title' => $category->Title, 'ID' => $category->ID);


                    $thumb = $model->ThumbnailPhoto()->Link();
                    //because of many_many we have to do a loop
                    foreach ($model->CarModelCategory() as $page) {
                        if ($page->ID == $category->ID) {
                            $variants = [];
                            foreach ($model->CarVariants() as $variant) {
                                $variants[] = ['VariantID' => $variant->VariantID];
                            }
                            $categoryData[$model->ID] = [
                                'ID'             => $model->ModelID,
                                'Title'          => $model->Title,
                                'Thumbnail'      => $thumb,
                                'Variants'       => $variants
//                                'Specifications' => $model->getVehicleSpecificationsArray()
                            ];
                        }
                    }
                }
            }
            if ($categoryData) {
                $category_nonaasoc = array_values($categoryData);
            } else {
                $category_nonaasoc = array();
            }
            $json = json_encode(['cars' => $category_nonaasoc], JSON_UNESCAPED_SLASHES);
            self::saveToFile('Cars/' . $category->ID, $json);
        }
        if ($categories_list) {
            $category_nonaasoc = array_values($categories_list);
        } else {
            $category_nonaasoc = array();
        }
        $json = json_encode(['Categories' => $category_nonaasoc], JSON_UNESCAPED_SLASHES);
        self::saveToFile('Categories', $json);


    }

    public static function generateAllVariants() {

        $data = array();
        $models = DataObject::get('CarConfiguratorModel')->sort('Title');
        foreach ($models as $model) {

            if (!isset($data['Categories'][$model->CarModelCategory()->ID])) {
                $data['Categories'][$model->CarModelCategory()->ID] = [
                    'ID' => $model->CarModelCategory()->ID,
                    'Title' => $model->CarModelCategory()->Title,
                    'Tag' => str_replace(' ', '-', strtolower($model->CarModelCategory()->Title)),
                    'Variants' => []
                ];
            }

//            $defaultWheels = self::getDefaultWheels($model);
            $variant_data = [];
            foreach ($model->CarVariants() as $variant) {

                if ($variant->ThumbnailPhotoID == 0)
                    $thumb = $model->ThumbnailPhoto()->Link();
                else
                    $thumb = $variant->ThumbnailPhoto()->Link();

                $variant_data[] = array(
                    'ID' => $variant->VariantID,
                    'Title' => $variant->Title,
                    'Content' => $variant->Description,
                    'Thumbnail' => $thumb
                );

                $data['Categories'][$model->CarModelCategory()->ID]['Variants'][] = [
                    'RealID' => $variant->ID,
                    'ID' => $variant->VariantID,
                    'Title' => $variant->Title,
                    'Content' => $variant->Description,
                    'Thumbnail' => $thumb,
                    'ModelID' => $model->ModelID,
//                    'Specifications' => $model->getVehicleSpecificationsArray()
                ];

                self::saveVariantData($variant/*, $defaultWheels*/);
            }

            $variant_json =  json_encode(["variants" => $variant_data], JSON_UNESCAPED_SLASHES);
            self::saveToFile('Variants/' . $model->ModelID, $variant_json);
        }
        $json = json_encode($data, JSON_UNESCAPED_SLASHES);
        self::saveToFile('AllVariants', $json);
    }

//    public static function getDefaultWheels($model) {
//
//        $defaultWheels = [];
//
//        foreach ($model->DefaultWheels() as $model_wheel) {
//            if ($model_wheel->VariantID == 0) {
//                if ($layer = self::getLayers($model->ModelID, $model_wheel->WheelID)) {
//                    $coloursArr = array_filter(explode(',', $model_wheel->ColoursArray));
//
//                    if ($dirtLayer = self::getLayers($model->ModelID, $model_wheel->WheelID . '-dirt')) {
//                        $dirt = array(
//                            'ID' => $model->ModelID . '-' . $model_wheel->WheelID . '-dirt',
//                            'zIndex' => /*$dirtLayer['z']*/ 9999,
//                            'Image' => $dirtLayer['dir'] . DIRECTORY_SEPARATOR . $dirtLayer['lp'],
//                            'Map' => $dirtLayer['is'],
//                            'Frame' => 1,
//                        );
//                    } else {
//                        $dirt = [];
//                    }
//
//                    $defaultWheels[] = array(
//                        'ID' => $model->ModelID . '-' . $model_wheel->WheelID,
//                        'Title' => $model_wheel->Title,
//                        'ColourID' => $coloursArr,
//                        'Thumbnail' => $model_wheel->ThumbnailPhoto()->Link(),
//                        'zIndex' => $layer['z'],
//                        'Image' => $layer['dir'] . DIRECTORY_SEPARATOR . $layer['lp'],
//                        'Map' => $layer['is'],
//                        'Frame' => 1,
//                        'Dirt' => $dirt
//                    );
//                }
//            }
//        }
//
//        return $defaultWheels;
//    }

    public static function saveVariantData($variant/*, $defaultWheels*/) {
        $colours = [];
        $bodies = [];
//        $wheels = [];

        $model = $variant->CarModelVariant();

        /*
         * Get bodies that belong to this variant
         */
        foreach ($variant->Colours() as $colour) {
            if ($base = self::getBases($variant->VariantID, $colour->ColourID)) {
                //append directory to images
                array_walk($base['is'], function(&$item1, &$key, $directory) {
                    $item1 = "$directory" . DIRECTORY_SEPARATOR . "$item1";
                }, $base['dir']);

                /*
                 * Get colours that belong to this variant
                 */
                $colours[] = array(
                    'ID' => $variant->VariantID . '-' . $colour->ColourID,
                    'Title' => $colour->Title,
                    'Thumbnail' => $colour->ThumbnailPhoto()->Link(),
                );

                $bodies[] = array(
                    'ID' => $variant->VariantID . '-' . $colour->ColourID,
                    'ColourID' => $variant->VariantID . '-' . $colour->ColourID,
                    'Height' => $base['h'],
                    'Width' => $base['w'],
                    'Images' => $base['is'],
                );
            }
        }

        /*
         * Get wheels that belong to a variant
         */
//        $wheelIndex = 0;
//        foreach ($variant->Wheels() as $wheel) {
//            if ($layer = self::getLayers($variant->VariantID, $wheel->WheelID)) {
//                $coloursArr = array_filter(explode(',', $wheel->ColoursArray));
//
//                if ($dirtLayer = self::getLayers($variant->VariantID, $wheel->WheelID . '-dirt')) {
//                    $dirt = array(
//                        'ID' => $variant->VariantID . '-' . $wheel->WheelID . '-dirt',
//                        'zIndex' => /*$dirtLayer['z']*/ 9999,
//                        'Image' => $dirtLayer['dir'] . DIRECTORY_SEPARATOR . $dirtLayer['lp'],
//                        'Map' => $dirtLayer['is'],
//                        'Frame' => 1,
//                    );
//                } else {
//                    $dirt = [];
//                }
//
//                $wheels[$wheelIndex] = array(
//                    'ID' => $variant->VariantID . '-' . $wheel->WheelID,
//                    'Title' => $wheel->Title,
//                    'ColourID' => $coloursArr,
//                    'Thumbnail' => $wheel->ThumbnailPhoto()->Link(),
//                    'zIndex' => $layer['z'],
//                    'Image' => $layer['dir'] . DIRECTORY_SEPARATOR . $layer['lp'],
//                    'Map' => $layer['is'],
//                    'Frame' => 1,
//                    'Dirt' => []
//                );
//
//                if($wheelIndex === 0) {
//                    $wheels[$wheelIndex]['Dirt'] = $dirt;
//                }
//
//                $wheelIndex++;
//            }
//        }


        $variantJson =  json_encode([
            'colour' => $colours, 'bodies' => $bodies,
            'accessories' => self::getAccessoriesArray($model->Accessories()),
//            'wheels' => array_merge($defaultWheels, $wheels),
            'variant' => ['ID' => $variant->VariantID, 'Title' => $variant->Title],
            'specifications' => $model->getVehicleSpecificationsArray()
        ], JSON_UNESCAPED_SLASHES);



        self::saveToFile('VariantData/' . $variant->VariantID, $variantJson);
    }

    private static function getAccessoriesArray($accessories) {
        $data = [];
        foreach ($accessories as $model_accessory) {

            $modelID = $model_accessory->Car()->ModelID;
            if ($layer = self::getLayers($modelID, $model_accessory->AccessoryID)) {

                $conflictingAccessories = [];
                $relatedAccessories = [];
                $AddAfterRemove = [];
                foreach($model_accessory->ConflictingAccessories() as $accessory){
                    $conflictingAccessories[] = $modelID . '-' . $accessory->AccessoryID;
                }
                foreach($model_accessory->RelatedAccessories() as $accessory){
                    $relatedAccessories[] = $modelID . '-' . $accessory->AccessoryID;
                }
                foreach($model_accessory->AddAfterRemove() as $accessory){
                    $AddAfterRemove[] = $modelID . '-' . $accessory->AccessoryID;
                }
                $coloursArr = array_filter(explode(',', $model_accessory->ColoursArray));
                $acc = [
                    'ID' => $modelID . '-' . $model_accessory->AccessoryID,
                    'Title' => $model_accessory->Title,
                    'ColourID' => $coloursArr,
                    'Thumbnail' => $model_accessory->ThumbnailPhoto()->Link(),
                    'zArray' => $model_accessory->getZIndexesArray(),
                    'Image' => $layer['dir'] . DIRECTORY_SEPARATOR . $layer['lp'],
                    'Map' => $layer['is'],
                    'Frame' => ($model_accessory->RotateCar == 0 ? NULL : ((int) $model_accessory->RotateCar)),
                    'ConflictingAccessories' => $conflictingAccessories,
                    'RelatedAccessories' => $relatedAccessories,
                    'AddAfterRemove' => $AddAfterRemove,
                    'IsDefault' => !!$model_accessory->IsDefault
                ];
                if ($model_accessory->ClassName == 'CarConfiguratorVariantPart') {
                    $acc['PartGroupID'] = $model_accessory->PartGroupID;
                }
                $data[] = $acc;
            }
        }
        return $data;
    }

    private static function generateSocial($variant, $model) {
        $_tmpl = array("{model}", "{variant}");
        $_replace = array($model->Title, $variant->Title);

        return [
            'socialFacebook' => array(
                'data-title' => str_replace($_tmpl, $_replace, SiteConfig::current_site_config()->CarConfiguratorFacebookTitle),
                'data-caption' => str_replace($_tmpl, $_replace, SiteConfig::current_site_config()->CarConfiguratorFacebookCaption),
                'data-description' => str_replace($_tmpl, $_replace, SiteConfig::current_site_config()->CarConfiguratorFacebookDescription),
                'data-url' => Director::absoluteBaseURL().$variant->CarModelProductPage()->Link()
            ),
            'socialTwitter' => array(
                'data-url' => Director::absoluteBaseURL().$variant->CarModelProductPage()->Link()
            )
        ];
    }

    private static function getBases($parentID, $colourID) {
        $bases_directory = 'bases' . DIRECTORY_SEPARATOR . $parentID . '-' . $colourID;
        if ($bases = self::getLocalManifest($bases_directory)) {
            $bases['dir'] = $bases_directory;
            return $bases;
        } else
            return;
    }

    private static function getBaseById($ID) {
        $bases_directory = 'bases/' . $ID;
        if ($bases = self::getLocalManifest($bases_directory)) {
            $bases['dir'] = $bases_directory;
            return $bases;
        } else
            return;
    }

    private static function getLayers($parentID, $itemID) {
        $layers_dir = 'layers/' . $parentID . '-' . $itemID;
        if ($layers = self::getLocalManifest($layers_dir)) {
            $layers['dir'] = $layers_dir;
            //override sprite name by default;
            if (!array_key_exists('lp', $layers)) {
                $layers['lp'] = self::SPRITENAME;
            }
            return $layers;
        } else
            return;
    }

    private static function getLayerByID($ID) {
        $layers_dir = 'layers/' . $ID;
        if ($layers = self::getLocalManifest($layers_dir)) {
            $layers['dir'] = $layers_dir;
            //override sprite name by default;
            if (!array_key_exists('lp', $layers)) {
                $layers['lp'] = self::SPRITENAME;
            }
            return $layers['is'];
        } else
            return;
    }

    private static function getLocalManifest($directory) {
        $filename = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . self::MANIFEST_FILENAME;
        if (file_exists($filename))
            return json_decode(file_get_contents($filename), TRUE);
        else
            return NULL;
    }

    private static function saveToFile($filename, $data) {
        $path = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . STATIC_DATA_DIR . DIRECTORY_SEPARATOR;

        $arr = explode('/', $filename);
        if (count($arr) > 1) {
            $folder = $path . $arr[0];
            if (!is_dir($folder)) {
                mkdir($folder);
            }
        }

        file_put_contents($path . $filename, $data);
    }
}