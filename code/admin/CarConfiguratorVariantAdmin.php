<?php

class CarConfiguratorVariantAdmin extends ModelAdmin
{
    private static $managed_models = [
        'CarConfiguratorModel' => ['title' => 'Vehicle Configurator Models'],
        'UserConfigurationModel' => ['title' => 'User Configurations Model'],
        'CarConfiguratorCategory' => ['title' => 'Vehicle Configurator Categories'],
        'BeaconModel' => ['title' => 'Beacon Model'],
    ];

    private static $url_segment = 'configurator';

    private static $menu_title = 'Vehicle Configurator';

    private static $menu_icon = 'mysite/images/cars-admin-16x16.png';

}



