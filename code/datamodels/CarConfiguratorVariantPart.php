<?php

class CarConfiguratorVariantPart extends CarConfiguratorVariantAccessory {

    static $db = [
        'IsDefault' => 'boolean'
    ];

    static $has_one = [
        'PartGroup' => 'PartGroup'
    ];

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeFieldsFromTab('Root.Main', [
            'ConflictingAccessories',
            'RelatedAccessories',
            'AddAfterRemove',
        ]);
        return $fields;
    }

    public function onAfterWrite() {

        if ($this->IsDefault) {
            $otherParts = $this->PartGroup()->Parts();
            foreach ($otherParts as $part) {
                if ($part->ID != $this->ID) {
                    $part->ConflictingAccessories()->add($this);
                    $part->AddAfterRemove()->add($this);
                    $part->write();
                }
            }
        }

        parent::onAfterWrite();
    }

    public function validate() {

        $result = new ValidationResult();

//        $otherParts = $this->PartGroup()->Parts();
//
//        foreach ($otherParts as $part) {
//
//            if ($part->ID != $this->ID) {
//
//                if ($part->IsDefault) {
//                    $result->error($part->Title . ' (ID: ' . $part->ID . ') is already default');
//                    break;
//                }
//            }
//        }

        if($this->Title == '' || strlen($this->Title) < 3) {
            $result->error('Need at least three characters for the title');
        }
        if($this->SourceFilesID == 0) {
            $result->error('Need to add the source files');
        }
        return $result;
    }
}