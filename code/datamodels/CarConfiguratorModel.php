<?php

class CarConfiguratorModel extends DataObject {
    private static $db = [
        'Title' => 'Varchar(125)',
        'ModelID' => 'Varchar(125)'
    ];

    private static $has_many = [
        'Accessories'   => 'CarConfiguratorVariantAccessory',
//        'PartGroups'    => 'PartGroup',
        'CarVariants'   => 'CarConfiguratorVariant',
//        'DefaultWheels' => 'CarConfiguratorVariantWheel'
    ];

    private static $has_one = [
        'VehicleModel'     => 'Vehicle',
        'CarModelCategory' => 'CarConfiguratorCategory',
        'ThumbnailPhoto'   => 'Image'
    ];

    private static $extensions = [
        //'SocialFeature'
    ];

    private static $belongs_many_many = [
    ];

    private static $summary_fields = [
        'Title' => 'Title',
        'Category' => 'Category'
    ];

    //for ModelAdmin naming
    private static $singular_name = 'Vehicle Configurator Model';

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeFieldFromTab('Root.Main', 'ModelID');
        if(!empty($this->ModelID)) {
            $link = new ReadonlyField('link', 'Configurator opener link', '#configure-honda/' . $this->ModelID);
            $fields->addFieldToTab('Root.Main', $link, 'ThumbnailPhoto');
        }
        $fields->addFieldsToTab('Root.Main', new HeaderField('Model', 'Vehicle Model'), 'Title');
        $thumb = $fields->dataFieldByName('ThumbnailPhoto');
        $thumb->setFolderName('configurator');
        $gridFieldConfig = GridFieldConfig_RecordEditor::create();
        $gridfield = new GridField(
            "Accessories",
            "Accessories",
            DataObject::get('CarConfiguratorVariantAccessory')
                ->filter([
                    'CarID' => $this->ID,
                    'ClassName' => 'CarConfiguratorVariantAccessory'
                ]),
            $gridFieldConfig
        );
        $fields->addFieldToTab('Root.Accessories', $gridfield);
        $gridFieldConfig = GridFieldConfig_RecordEditor::create();
        $gridfield = new GridField("CarVariants", "Vehicle Variants", $this->CarVariants(), $gridFieldConfig);
        $fields->addFieldToTab('Root.CarVariants', $gridfield);
        $gridFieldConfig = GridFieldConfig_RecordEditor::create();
//        $gridfield = new GridField("DefaultWheels", "Default Wheels", $this->DefaultWheels(), $gridFieldConfig);
//        $fields->addFieldToTab('Root.DefaultWheels', $gridfield);

        $gridFieldConfig = GridFieldConfig_RecordEditor::create();
        $gridFieldConfig->removeComponentsByType('GridFieldDeleteAction');
        $gridFieldConfig->addComponents(
            new GridFieldDeleteAction(false)
        );
//        $gridfield = new GridField("PartGroups", "Part Groups", $this->PartGroups(), $gridFieldConfig);
//        $fields->addFieldToTab('Root.PartGroups', $gridfield);

        return $fields;
    }

    public function getVehicleSpecificationsArray() {
        $specificationsArray = array();

        if ($this->VehicleModel()->exists()) {
            foreach ( $this->VehicleModel()->SpecificationTabs() as $tab ) {
                $tempArray = array("Title" => $tab->Title, "Data" => array());
                foreach ($tab->Specifications() as $specification) {
                    array_push($tempArray['Data'], array(
                        "Title"       => $specification->Title,
                        "Description" => $specification->Description
                    ));
                }
                array_push($specificationsArray, $tempArray);
            }
            return $specificationsArray;
        }

        return array();
    }

    private function getAccessoriesForGrid() {
        //$accessorie = $this->Accessories()
    }

    public function validate() {
        $result = parent::validate();
        $existingTitleModel= DataObject::get('CarConfiguratorModel', "ID != $this->ID")->filter(array('ModelID' => $this->formatModelID()))->First();
        if($existingTitleModel) {
            $result->error('A vehicle configurator model with the same name already exists');
        }
        if($this->Title == '' || strlen($this->Title) < 3) {
            $result->error('Need at least three characters for the title');
        }
        if($this->ThumbnailPhotoID == 0) {
            $result->error('Need to add a thumbnail picture');
        }
        return $result;
    }


    //it returns the parent car name for the summary fields
    public function getCategory() {
        return $this->CarModelCategory()->Title;
    }

    public function onBeforeWrite() {
        parent::onBeforeWrite();
        if(!$this->ModelID)
        $this->createModelID();
    }

    public function onAfterWrite() {
        parent::onAfterWrite();
        ConfiguratorStaticDataManager::generateCategories();
        ConfiguratorStaticDataManager::generateAllVariants();
    }

    protected function onBeforeDelete() {
        //delete all the children and the car configurator data
        foreach($this->CarVariants() as $variant) {
            $variant->delete();
        }
//        foreach($this->DefaultWheels() as $wheel) {
//            $wheel->delete();
//        }
        foreach($this->Accessories() as $accessory) {
            $accessory->delete();
        }
        parent::onBeforeDelete();
    }

    function createModelID() {
        //if the variant has been set dont call the write function
        if($this->ModelID != $this->formatModelID()) {
            $this->ModelID = $this->formatModelID();
        }
    }

    function formatModelID() {
        $name = '';
        $string = $this->Title;
        $pattern = '<\W+>';
        $replacement = '';
        if($string) $name = preg_replace($pattern, $replacement, $string);
        return strtolower($name);
    }

}
