<?php

class CarConfiguratorVariantColour extends DataObject
{

    private static $db = [
        'Title' => 'Varchar(125)',
        'ColourID' => 'Varchar(125)',
        'SortOrder'=>'Int'
    ];
    private static $has_one = [
        'ThumbnailPhoto'         => 'Image',
        'Car'                    => 'CarConfiguratorVariant',
        'SourceFiles'            => 'TarGzFile'
    ];
    private static $has_many = [
    ];

    private static $summary_fields = [

    ];

    private static $singular_name = 'Vehicle Configurator Variant Colour';
    
    public static $default_sort='SortOrder';
    
    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeFieldFromTab('Root.Main', 'ColourID');
        $fields->addFieldsToTab('Root.Main', new HeaderField('Colour', 'Colour'), 'Title');
        $SourceFiles = $fields->dataFieldByName('SourceFiles');
        $SourceFiles->setRightTitle('File must have .tar.gz extension.');
        $SourceFiles->setFolderName('configurator');
        $thumb = $fields->dataFieldByName('ThumbnailPhoto');
        $thumb->setFolderName('configurator');
        $fields->removeFieldFromTab('Root.Main', 'CarID');
        return $fields;
    }

    public function validate() {
        $result = parent::validate();
        if($this->Title == '' || strlen($this->Title) < 3) {
            $result->error('Need at least three characters for the title');
        }
        if($this->ThumbnailPhotoID == 0) {
            $result->error('Need to add a thumbnail picture');
        }
        if($this->SourceFilesID == 0) {
            $result->error('Need to add the source files');
        }
        return $result;
    }

    protected function onAfterWrite() {
        parent::onAfterWrite();
        if(!$this->ColourID) {
           $this->createColourID();
           $this->write();
        }
        $extract_path = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . 'bases' . DIRECTORY_SEPARATOR . $this->Car()->VariantID . '-' . $this->ColourID;
        if($this->SourceFiles()->ID != 0) {
            $this->SourceFiles()->decompressFile($extract_path);
        } else {
           $sourceId =  $_POST['SourceFiles']['Files'][0];
           $sourceFile = TarGzFile::get()->byID($sourceId);
           $sourceFile->decompressFile($extract_path);
        }
    }

    protected function onBeforeDelete() {
        if($this->SourceFiles()->ID != 0) {
            $this->SourceFiles()->deleteConfiguratorDirectory();
        }
        parent::onBeforeDelete();
    }

    public function createColourID() {
        $unique = false;
        if($this->CarID != 0) {
            $parentID = $this->CarID;
            $existingAccID = DataObject::get('CarConfiguratorVariantColour', "CarID" == "$parentID")->filter(array('ColourID' => $this->formatColourID()))->First();
            if($existingAccID) $unique = true;
            $this->ColourID = $this->formatColourID($unique);
        }
    }

    function formatColourID($unique = false) {
        $name = '';
        $string = $this->Title;
        $pattern = '<\W+>';
        $replacement = '-';
        if($string) $name = preg_replace($pattern, $replacement, $string);
        $name = strtolower($name);
        if($unique) {
            $name .= $this->ID;;
        }
        return $name;
    }

}