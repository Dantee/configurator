<?php

class TarGzFile extends File {

    private static $db = [
        'ConfiguratorDirectoryPath' => 'Varchar(255)'
    ];

    public function decompressFile($path_to) {
        if(pathinfo($this->getFullPath(), PATHINFO_EXTENSION) != 'zip') {
            echo "Uploaded file is not a ZIP archive:" . $this->getFullPath();
            return;
        } else {
            echo "File is zip, continuing on: " . $this->getFullPath();
        }
        if($this->getAbsoluteSize() > 0) {
            if($this->ConfiguratorDirectoryPath != $path_to) {
                $this->ConfiguratorDirectoryPath = $path_to;
                $this->write();
            }
            $zip = new ZipArchive();
            if ($zip->open($this->getFullPath()) === TRUE) {
                if (is_dir($path_to)) $this->deleteDir($path_to);
                mkdir($path_to, 0755, true);
                $zip->extractTo($path_to);
                $zip->close();
                echo "Successfully extracted to: " . $path_to;
            } else {
                echo "Could not open ZIP archive here: " . $this->getFullPath();
            }
        }
    }

    public function validate() {
        $result = parent::validate();
        if(pathinfo($this->getFullPath(), PATHINFO_EXTENSION) != 'zip') {
            $result->error('The file extension must be .zip');
        }
        return $result;
    }


    public function deleteConfiguratorDirectory() {
        if($this->ConfiguratorDirectoryPath != '' &&  file_exists($this->ConfiguratorDirectoryPath)) {
            $this->deleteDir($this->ConfiguratorDirectoryPath);
        }
    }

    private function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

}