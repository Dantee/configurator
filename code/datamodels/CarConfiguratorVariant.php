<?php
class CarConfiguratorVariant extends DataObject {
    private static $db = [
        'Title' => 'Varchar(125)',
        'Description' => 'Text',
        'VariantID' => 'Varchar(125)',
        'LinkID' => 'Varchar(125)',
        'FacebookPixelID' => 'Varchar(125)',
        'DefaultColorID' => 'Int'
    ];

    private static $has_many = [
//        'Wheels'    => 'CarConfiguratorVariantWheel',
        'Colours'   => 'CarConfiguratorVariantColour'
    ];

    private static $has_one = [
        'CarModelVariant' => 'CarConfiguratorModel',
        'ThumbnailPhoto' => 'Image',
        'CarModelProductPage' => 'Page',
//        'DirtFiles'           => 'TarGzFile',
//        'WrapLeftImage' => 'Image',
//        'WrapRightImage' => 'Image',
//        'WrapFrontBumperImage' => 'Image',
//        'WrapHoodImage' => 'Image',
//        'WrapBackImage' => 'Image',
    ];
    
    private static $belong_to = [
        'RelatedModel' => 'CarModel'
    ];

    private static $summary_fields = [
        'Title' => 'Car Variant',
        'Car' => 'Vehicle',
        'Description' => 'Description'
    ];

    //for ModelAdmin naming
    private static $singular_name = 'Car Configurator Variant';

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->dataFieldByName('Description')->setRightTitle('New line creates a bullet point. Max four lines.');
        $fields->removeFieldFromTab('Root.Main', 'VariantID');
        $fields->removeFieldFromTab('Root.Main', 'CarModelVariantID');
        $fields->addFieldsToTab('Root.Main', new HeaderField('Variant', 'Vehicle Variant'), 'Title');
        if($this->LinkID != '') {
            $link = new ReadonlyField('link', 'Configurator opener link', '#configure-isuzu/' . $this->LinkID);
            $link->setRightTitle('This link will open the vehicle configurator with this variant selected');
            $fields->addFieldToTab('Root.Main', $link, 'ThumbnailPhoto' );
        }
        $thumb = $fields->dataFieldByName('ThumbnailPhoto');
        $thumb->setRightTitle('Not mandatory. If not added, it will use the model as default');
        $thumb->setFolderName('configurator');
//        $gridFieldConfig = GridFieldConfig_RecordEditor::create();
//        $gridfield = new GridField("Wheels", "Wheels", $this->Wheels(), $gridFieldConfig);
//        $gridFieldConfig = GridFieldConfig_RecordEditor::create();
//        $fields->addFieldToTab('Root.Wheels', $gridfield);
        $conf=GridFieldConfig_RecordEditor::create();
        $conf->addComponent(new GridFieldSortableRows('SortOrder'));
        $fields->addFieldToTab('Root.Colours', new GridField('Colours', 'Colours', $this->Colours(), $conf));

//        $dirtFiles = $fields->dataFieldByName('DirtFiles');
//        $dirtFiles->setRightTitle('File must have .tar.gz extension.');
//        $dirtFiles->setFolderName('configurator');

        $fields->addFieldToTab('Root.Main', TextField::create('FacebookPixelID'));

//        $fields->addFieldsToTab('Root.CustomWraps', [
//            new UploadField('WrapLeftImage'),
//            new UploadField('WrapRightImage'),
//            new UploadField('WrapFrontBumperImage'),
//            new UploadField('WrapHoodImage'),
//            new UploadField('WrapBackImage'),
//        ]);
//
//        $fields->addFieldToTab('Root.CustomWraps', new OptionsetField(
//                $name = "DefaultColorID",
//                $title = "Default Color to Open on up in configurator",
//                $source = $this->Colours()->map(),
//                $value = (($_defaultColor = $this->getDefaultColor()) ? $_defaultColor->ID : null)
//            )
//        );

        return $fields;
    }

    public function getDefaultColor() {
        if($color = $this->Colours()->filter('ID', $this->getField('DefaultColorID'))->first()) {
            return $color;
        } else {
            return $this->Colours()->first();
        }
    }

    public function validate() {
        $result = parent::validate();
        if($this->Title == '' || strlen($this->Title) < 3) {
            $result->error('Need at least three characters for the title');
        }
        if($this->Description != '') {
            preg_match_all('(\\n)', trim($this->Description), $found);
            $number_lines = count($found[0]) + 1;
            if($number_lines > 4) $result->error('Maximum 4 lines are allowed in the description');
        }
        $existingLinkVariant= DataObject::get('CarConfiguratorVariant', "ID != $this->ID")->filter(array('LinkID' => $this->LinkID))->First();
        if($existingLinkVariant) {
            $result->error('A vehicle configurator variant with the same name link already exists');
        }
        return $result;
    }

    protected function onBeforeWrite() {
        //should only call the first time
        if($this->Description) {
            $this->Description = trim($this->Description);
        }
        parent::onBeforeWrite();
    }

    function onAfterWrite() {
        parent::onAfterWrite();
        //makes sure this is call only the first time is created
        if(!$this->VariantID && $this->CarModelVariant()->ID != 0) {
            $this->createVariantID();
        }
        if($this->LinkID == '' && $this->CarModelVariant()->ID != 0)  {
            $this->createLinkID();
        }
//        $extract_path = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . 'layers' . DIRECTORY_SEPARATOR . $this->VariantID . '-dirt';
//        if($this->DirtFiles()->ID != 0) {
//            $this->DirtFiles()->decompressFile($extract_path);
//        } else {
//            if (!empty($_POST['DirtFiles']['Files'])) {
//                $dirtId =  $_POST['DirtFiles']['Files'][0];
//                $dirtFile = TarGzFile::get()->byID($dirtId);
//                $dirtFile->decompressFile($extract_path);
//            }
//        }
        ConfiguratorStaticDataManager::generateCategories();
        ConfiguratorStaticDataManager::generateAllVariants();
    }

    protected function onBeforeDelete() {
        //delete all the children and the car configurator data
        foreach($this->Colours() as $colour) {
            $colour->delete();
        }
//        foreach($this->Wheels() as $wheel) {
//            $wheel->delete();
//        }
//        if($this->DirtFiles()->ID != 0) {
//            $this->DirtFiles()->deleteConfiguratorDirectory();
//        }
        parent::onBeforeDelete();
    }

    function createVariantID() {
        //if the variant has been set dont call the write function
        if($this->VariantID != $this->formatVariantID()) {
            $this->VariantID = $this->formatVariantID();
            $this->write();
        }
    }

    function createLinkID() {
        //if the link has been set dont call the write function
        if($this->LinkID != $this->formatVariantID()) {
            $this->LinkID = $this->formatVariantID();
            $this->write();
        }
    }

    function formatVariantID() {
        $name = '';
        $string = $this->CarModelVariant()->Title;
        $pattern = '<\W+>';
        $replacement = '';
        if($string) $name = preg_replace($pattern, $replacement, $string);
        return strtolower($name) . $this->ID;
    }

    //it returns the parent car name for the summary fields
    public function getCar() {
        return $this->CarModelVariant()->Title;
    }

    public function DropdownSummary() {
        $String = $this->CarModelVariant()->Title . ' | ' . $this->Title;
        return $String;
    }

}
