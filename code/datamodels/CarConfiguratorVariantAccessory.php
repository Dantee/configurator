<?php

class CarConfiguratorVariantAccessory extends DataObject {

    private static $db = [
        'Title'          => 'Varchar(125)',
        'AccessoryID'    => 'Varchar(125)',
        'RotateCar'      => "Enum('0, 1, 4, 34, 28, 22, 19', '0')",
        'ZIndexDefault'  => "Int",
        'ZIndexes'       => "Varchar(255)"
    ];

    private static $has_one = [
        'ThumbnailPhoto'         => 'Image',
        'Car'                    => 'CarConfiguratorModel',
        'SourceFiles'            => 'TarGzFile'
    ];

    private static $has_many = [
    ];

    private static $many_many = [
        'ConflictingAccessories'    => 'CarConfiguratorVariantAccessory',
        'RelatedAccessories'        => 'CarConfiguratorVariantAccessory',
        'AddAfterRemove'            => 'CarConfiguratorVariantAccessory',
    ];

    private static $summary_fields = [
    ];

    private static $defaults = [
        'ZIndexDefault' => 40,
    ];

    private static $singular_name = 'Vehicle Configurator Variant Accessory';

    function getCMSFields() {
        /*
         * Getting model ID
         */
        $request = $_SERVER['REQUEST_URI'];
        //first we search for model ID
        $parameters = explode('/', $request);
        foreach ($parameters as $key => $parameter) {
            if ($parameter == 'CarConfiguratorModel' && $parameters[$key + 1] == 'item')
                $model_id = $parameters[$key + 2];
        }
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Main', new HeaderField('Accessory', 'Accessory'), 'Title');
        $fields->removeFieldFromTab('Root.Main', 'AccessoryID');
        $fields->removeFieldFromTab('Root.Main', 'CarID');
        
        $fields->addFieldToTab('Root.Main', new HiddenField('CarID', 'CarID', $model_id));
        $fields->dataFieldByName('SourceFiles')->setRightTitle('File must have .tar.gz extension.');
        $fields->removeFieldFromTab('Root.Main', 'RotateCar');
        $moveCar = array(
            '0' => 'Don\'t move the vehicle when accessory selected',
            '1' => 'Front',
            '4' => '3/4 Right',
            '34' => '3/4 Left',
            '28' => 'Side',
            '22' => '3/4 Back',
            '19' => 'Back'
        );
        $moveCarDropdown = new DropdownField('RotateCar', 'Rotate Vehicle When Selected', $moveCar);
        $moveCarDropdown->setRightTitle('The vehicle will rotate automatically when an accessory is selected by user');
        $fields->addFieldToTab('Root.Main', $moveCarDropdown);
        $SourceFiles = $fields->dataFieldByName('SourceFiles');
        $SourceFiles->setRightTitle('File must have .tar.gz extension.');
        $SourceFiles->setFolderName('configurator');
        $thumb = $fields->dataFieldByName('ThumbnailPhoto');
        $thumb->setFolderName('configurator');

        $fields->removeByName('ConflictingAccessories');
        $fields->insertAfter(
            ListboxField::create(
                'ConflictingAccessories',
                'Conflicting Accessories',
                CarConfiguratorModel::get()->filter(['ID' => $model_id])->First()->Accessories()->map()->toArray()
            )->setMultiple(true),
            'RotateCar'
        );

        $fields->removeByName('RelatedAccessories');
        $fields->insertAfter(
            ListboxField::create(
                'RelatedAccessories',
                'Related Accessories',
                CarConfiguratorModel::get()->filter(['ID' => $model_id])->First()->Accessories()->map()->toArray()
            )->setMultiple(true),
            'ConflictingAccessories'
        );

        $fields->removeByName('AddAfterRemove');
        $fields->insertAfter(
            ListboxField::create(
                'AddAfterRemove',
                'Add accessories after this removed',
                CarConfiguratorModel::get()->filter(['ID' => $model_id])->First()->Accessories()->map()->toArray()
            )->setMultiple(true),
            'RelatedAccessories'
        );

        if ($this->ID) {
            $fields->removeByName('ZIndex');
            $fields->removeByName('ZIndexes');
            $data = explode(",", $this->ZIndexes);
            for ($i = 1; $i < 37; $i++) {
                $fields->addFieldToTab("Root.ZIndexes", $field = TextField::create("ZIndex[" . $i . "]", "z-index[" . $i . "]"));
                if (isset($data[$i - 1]) && $data[$i - 1] != null) {
                    $field->setValue($data[$i - 1]);
                } else {
                    $field->setValue($this->ZIndexDefault);
                }
            }
        }

        return $fields;
    }

    private function implodeIndexes() {
        $indexes = array();
        for ($i = 1; $i < 37; $i++) {
            array_push($indexes, ($this->getField("ZIndex[" . $i . "]") != null) ? $this->getField("ZIndex[" . $i . "]") : $this->ZIndexDefault);
        }

        return implode(',', $indexes);
    }

    public function getZIndexesArray() {
        return explode(",", $this->ZIndexes);
    }

    public function validate() {
        $result = parent::validate();
        if($this->Title == '' || strlen($this->Title) < 3) {
            $result->error('Need at least three characters for the title');
        }
        if (empty($this->IsDefault)) {
            if ($this->ThumbnailPhotoID == 0) {
                $result->error('Need to add a thumbnail picture');
            }
        }

        if($this->SourceFilesID == 0) {
            $result->error('Need to add the source files');
        }
        return $result;
    }

    protected function onBeforeWrite() {
        parent::onBeforeWrite();

        $this->ZIndexes = $this->implodeIndexes();
    }

    protected function onAfterWrite() {
        parent::onAfterWrite();
        if(!$this->AccessoryID) {
            $this->createAccessoryID();
            $this->write();
        }
        $extract_path = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . 'layers' . DIRECTORY_SEPARATOR . $this->Car()->ModelID . '-'. $this->AccessoryID;
        if($this->SourceFiles()->ID != 0) {
            $this->SourceFiles()->decompressFile($extract_path);
        } else {
            if (isset($_POST['SourceFiles']['Files'][0])) {
                $sourceId =  $_POST['SourceFiles']['Files'][0];
                if ($sourceFile = TarGzFile::get()->byID($sourceId)) {
                    $sourceFile->decompressFile($extract_path);
                }
            }
        }
        ConfiguratorStaticDataManager::generateCategories();
        ConfiguratorStaticDataManager::generateAllVariants();
    }

    protected function onBeforeDelete() {
        if($this->SourceFiles()->ID != 0) {
            $this->SourceFiles()->deleteConfiguratorDirectory();
        }
        parent::onBeforeDelete();
    }

    public function createAccessoryID() {
        $unique = false;
        if($this->CarID != 0) {
            $parentID = $this->CarID;
            $existingAccID = DataObject::get('CarConfiguratorVariantAccessory', "CarID" == "$parentID")->filter(array('AccessoryID' => $this->formatAccessoryID()))->First();
            if($existingAccID) $unique = true;
            $this->AccessoryID = $this->formatAccessoryID($unique);
        }

    }

    function formatAccessoryID($unique = false) {
        $name = '';
        $string = $this->Title;
        $pattern = '<\W+>';
        $replacement = '';
        if($string) $name = preg_replace($pattern, $replacement, $string);
        $name = strtolower($name);
        if($unique) {
            $name .= $this->ID;
        }
        return $name;
    }
}