<?php

class UserConfigurationModel extends DataObject {
    private static $db = array(
        'Uuid' => 'Varchar',
        'Data' => 'Text',
    );

    private static $summary_fields = array('Uuid');
}
