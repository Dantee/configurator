<?php

class BeaconModel extends DataObject {
    private static $db = array(
        'DeviceIdent' => 'varchar(36)',
        'Minor' => 'int',
        'Major' => 'int',
    );

    private static $summary_fields = array(
        'DeviceIdent' => 'DeviceIdent',
        'Minor' => 'Minor',
        'Major' => 'Major',
        'Car' => 'Vehicle',
        'VariantColour' => 'VariantColour',
    );

    private static $has_one = array(
        'Variant' => 'CarConfiguratorVariant',
        'Colour' => 'CarConfiguratorVariantColour'
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $variants = new DropdownField('VariantID', 'Variant', $this->getVariants());
        $fields->addFieldToTab('Root.Main', $variants);
        $colours = new DropdownField('ColourID', 'Colour', $this->getColours());
        $fields->addFieldToTab('Root.Main', $colours);
        return $fields;
    }

    private function getVariants() {
        $variants = CarConfiguratorVariant::get();
        $data = array();
        foreach($variants as $variant) {
            $data[$variant->ID] = $variant->CarModelVariant()->Title . ' | ' . $variant->Title;
        }
        return $data;
    }

    private function getColours() {

        if (empty($this->Variant()->ID)) {
            return array();
        }

        $colours = CarConfiguratorVariantColour::get();
        $data = array();
        foreach ($colours as $colour) {
            if ($colour->Car()->ID == $this->Variant()->ID)
                $data[$colour->ID] = $colour->Title;
        }
        return $data;
    }

    public function getCar() {
        $variant = $this->Variant();
        return $variant->CarModelVariant()->Title . ' | ' . $variant->Title;
    }

    public function getVariantColour() {
        $colour = $this->Colour();
        return !empty($colour) ? $colour->Title : '';
    }

    public function toArray() {
        return [
            'Title' => $this->Title,
            'Minor' => $this->Minor,
            'Major' => $this->Major,
            'Car' => $this->Variant()->CarModelVariant()->ModelID,
            'LinkID' => $this->Variant()->LinkID,
            'Colour' => $this->Variant()->LinkID . '-' . $this->Colour()->ColourID,
        ];
    }

}
