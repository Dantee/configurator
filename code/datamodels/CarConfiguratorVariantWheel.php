<?php

class CarConfiguratorVariantWheel extends DataObject {

    private static $db = [
        'Title' => 'Varchar(125)',
        'WheelID' => 'Varchar(125)'
    ];

    private static $has_one = [
        'ThumbnailPhoto'         => 'Image',
        'CarVariant'             => 'CarConfiguratorVariant',
        'CarModel'               => 'CarConfiguratorModel',
        'SourceFiles'            => 'TarGzFile',
        'DirtFiles'              => 'TarGzFile'
    ];

    private static $has_many = [
    ];

    private static $summary_fields = [
    ];

    private static $singular_name = 'Car Configurator Variant Wheels';

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Main', new HeaderField('Wheels', 'Wheels'), 'Title');
        $fields->removeFieldFromTab('Root.Main','CarVariantID');
        $fields->removeFieldFromTab('Root.Main','WheelID');
        $fields->removeFieldFromTab('Root.Main', 'CarModelID');
        $SourceFiles = $fields->dataFieldByName('SourceFiles');
        $SourceFiles->setRightTitle('File must have .tar.gz extension.');
        $SourceFiles->setFolderName('configurator');
        $DirtFiles = $fields->dataFieldByName('DirtFiles');
        $DirtFiles->setRightTitle('File must have .tar.gz extension.');
        $DirtFiles->setFolderName('configurator');
        $thumb = $fields->dataFieldByName('ThumbnailPhoto');
        $thumb->setFolderName('configurator');
        return $fields;
    }

    public function validate() {
        $result = parent::validate();
        if($this->Title == '' || strlen($this->Title) < 3) {
            $result->error('Need at least three characters for the title');
        }
        if($this->ThumbnailPhotoID == 0) {
            $result->error('Need to add a thumbnail picture');
        }
        if($this->SourceFilesID == 0) {
            $result->error('Need to add the source files');
        }
        return $result;
    }

    function onAfterWrite() {
        parent::onAfterWrite();
        if(!$this->WheelID && ($this->CarVariantID || $this->CarModelID)) {
            $this->createWheelID();
            $this->write();
        }
        //add the parent if is a model or a variant
        $parent = '';
        if($this->CarVariant()->VariantID != '') {
            $parent = $this->CarVariant()->VariantID;
        } else {
            $parent = $this->CarModel()->ModelID;
        }
        $extract_path = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . 'layers' . DIRECTORY_SEPARATOR . $parent . '-'. $this->WheelID;
        if($this->SourceFiles()->ID != 0) {
            $this->SourceFiles()->decompressFile($extract_path);
        } else {
            $sourceId =  $_POST['SourceFiles']['Files'][0];
            $sourceFile = TarGzFile::get()->byID($sourceId);
            $sourceFile->decompressFile($extract_path);
        }
        $extract_path = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . 'layers' . DIRECTORY_SEPARATOR . $parent . '-'. $this->WheelID . '-dirt';
        if($this->DirtFiles()->ID != 0) {
            $this->DirtFiles()->decompressFile($extract_path);
        } else {
            if (isset($_POST['DirtFiles']) && !empty($_POST['DirtFiles'])) {
                $dirtId =  $_POST['DirtFiles']['Files'][0];
                if ($dirtId > 0 && $dirtFile = TarGzFile::get()->byID($dirtId)) {
                    $dirtFile->decompressFile($extract_path);
                }
            }
        }
    }

    public function createWheelID() {
        $unique = false;
        if($this->CarVariantID != 0) {
            $parentID = $this->CarVariantID;
            $existingWheelID = DataObject::get('CarConfiguratorVariantWheel', "CarVariantID" == "$parentID")->filter(array('WheelID' => $this->formatWheelID()))->First();
        } else {
            $parentID = $this->CarModelID;
            $existingWheelID = DataObject::get('CarConfiguratorVariantWheel', "CarModelID" == "$parentID")->filter(array('WheelID' => $this->formatWheelID()))->First();
        }
        if($existingWheelID) $unique = true;
        $this->WheelID = $this->formatWheelID($unique);
        $this->write();
    }

    protected function onBeforeDelete() {
        if($this->SourceFiles()->ID != 0) {
            $this->SourceFiles()->deleteConfiguratorDirectory();
        }
        if($this->DirtFiles()->ID != 0) {
            $this->DirtFiles()->deleteConfiguratorDirectory();
        }
        parent::onBeforeDelete();
    }

    function formatWheelID($unique = false) {
        $name = '';
        $string = $this->Title;
        $pattern = '<\W+>';
        $replacement = '-';
        if($string) $name = preg_replace($pattern, $replacement, $string);
        $name = strtolower($name);
        if($unique) {
            $name .= $this->ID;;
        }
        return $name;
    }

}