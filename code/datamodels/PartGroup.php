<?php

class PartGroup extends DataObject {
    static $db = [
        'Title' => 'Varchar(255)'
    ];

    static $has_many = [
        'Parts' => 'CarConfiguratorVariantPart'
    ];

    static $has_one = [
        'Model' => 'CarConfiguratorModel'
    ];

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $gridFieldConfig = GridFieldConfig_RecordEditor::create();
        $gridFieldConfig->removeComponentsByType('GridFieldDeleteAction');
        $gridFieldConfig->addComponents(
            new GridFieldDeleteAction(false)
        );
        $gridfield = new GridField("Parts", "Parts", $this->Parts(), $gridFieldConfig);
        $fields->addFieldToTab('Root.Parts', $gridfield);
        return $fields;
    }

    public function onBeforeDelete() {
        $parts = $this->Parts();
        foreach ($parts as $part) {
            $part->delete();
        }
        parent::onBeforeDelete();
    }
}