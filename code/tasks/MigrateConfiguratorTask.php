<?php

class MigrateConfiguratorTask extends BuildTask {

    const exportFile = 'export.json';
    const exportDir = 'migrate';

    protected $title = 'Configurator Tasks';
    protected $description = 'Export/Import current Configurator data. Clear thumbnail cache';
    protected $enabled = true;
    private $log;
    private $critical; //if critical error then rollback transaction
    private $fileFields = array(
        'ID',
        'ClassName',
        'Created',
        'LastEdited',
        'Name',
        'Title',
        'Filename',
        'Content',
        'ShowInSearch',
        'ParentID',
        'OwnerID',
        //'SubsiteID'
    );
    private $modelFields = array(
        'CarConfiguratorModel' =>
        array(
            'ID',
            'ClassName',
            'Created',
            'LastEdited',
            'Title',
            'ModelID',
            'CarModelCategoryID',
            'ThumbnailPhotoID',
        ),
        'CarConfiguratorVariant' =>
        array(
            'ID',
            'ClassName',
            'Created',
            'LastEdited',
            'Title',
            'VariantID',
            'Description',
            'LinkID',
            'CarModelVariantID',
            'ThumbnailPhotoID'
        ),
    );
    private $optionFields = array(
        'CarConfiguratorVariantAccessory' =>
        array(
            'ID',
            'ClassName',
            'Created',
            'LastEdited',
            'Title',
            'AccessoryID',
            'RotateCar',
            'ColoursArray',
            'ThumbnailPhotoID',
            'CarID',
            'SourceFilesID'
        ),
        'CarConfiguratorVariantColour' =>
        array(
            'ID',
            'ClassName',
            'Created',
            'LastEdited',
            'Title',
            'ColourID',
            'ThumbnailPhotoID',
            'CarID',
            'SourceFilesID',
            'SortOrder'
        ),
        'CarConfiguratorVariantWheel' =>
        array(
            'ID',
            'ClassName',
            'Created',
            'LastEdited',
            'Title',
            'WheelID',
            'ColoursArray',
            'ThumbnailPhotoID',
            'CarVariantID',
            'CarModelID',
            'SourceFilesID'
        ),
    );

    private function prependArray($string, $array) {
        foreach ($array as $value) {
            $return[] = $string . '.' . $value . ' AS ' . $string . '_' . $value;
        }
        return $return;
    }

    public function run($request) {

        if ($request->getVar('action') == 'restore') {
            if (DB::getConn()->supportsTransactions()) {
                DB::getConn()->transactionStart();
            }
            //empty tables, transaction safe DDL
            $this->trucateTables();

            $parentID = $this->createDbFolder();
            if ($json = $this->getExportJson()) {

                foreach ($this->modelFields as $key_table => $table) {
                    if (isset($json[$key_table]))
                        foreach ($json[$key_table] as $record) {
                            if ($imageID = $this->insertImage($parentID, $record)) {
                                $this->addLog("Image {$imageID} added");
                                $record[$key_table . '_ThumbnailPhotoID'] = $imageID;
                            }


                            $this->insertRecord($key_table, $this->modelFields[$key_table], $record);
                        } else
                        $this->addLog("No data in {$key_table}", TRUE);
                }
                foreach ($this->optionFields as $key_table => $table) {
                    if (isset($json[$key_table]))
                        foreach ($json[$key_table] as $record) {
                            if ($imageID = $this->insertImage($parentID, $record)) {
                                $this->addLog("Image {$imageID} added");
                                $record[$key_table . '_ThumbnailPhotoID'] = $imageID;
                            }

                            if ($sourceID = $this->insertSource($parentID, $record)) {
                                $this->addLog("Source {$sourceID} added");
                                $record[$key_table . '_SourceFilesID'] = $sourceID;
                            }

                            $this->insertRecord($key_table, $this->optionFields[$key_table], $record);
                        } else
                        $this->addLog("No data in {$key_table}", TRUE);
                }
            } else {
                $this->critical[] = TRUE;
                $this->addLog('No ' . self::exportFile . ' found in /' . self::exportDir, TRUE);
            }

            if (count($this->critical) == 0 || $request->getVar('force') == '1') {
                if (DB::getConn()->supportsTransactions()) {
                    DB::getConn()->transactionEnd();
                }
                $this->addLog('<hr><b>All fields were inserted</b>', FALSE);
            } else {
                if (DB::getConn()->supportsTransactions()) {
                    DB::getConn()->transactionRollback();
                }
                $this->addLog('<hr><b>Rolling back changes, unable to insert fields correctry</b>', TRUE);
            }
        }
        if ($request->getVar('action') == 'backup') {
            $this->dumpData();
        }
        if ($request->getVar('action') == 'clear-cache') {
            $this->clearThumbCache();
        }
        echo $this->log;
        echo <<<HTML
        <p> Choices are: <ul><li><a href='?action=backup'>Backup configurator data</a></li><li><a href='?action=restore'>Restore configurator data</a></li><li><a href='?action=restore&force=1'>Force to restore configurator data</a></li><li><a href='?action=clear-cache'>Clear generated thumbnails cache</a></li></ul>
HTML;
    }

    private function insertRecord($tableName, $columns = array(), $rows = array()) {
        foreach ($columns as $key => $column) {
            if (isset($rows[$tableName . '_' . $column]) && $rows[$tableName . '_' . $column] != '')
                $values[] = "'" . addslashes($rows[$tableName . '_' . $column]) . "'";
            else
                $values[] = "NULL";
        }
        $inserted = DB::query("SELECT COUNT(*) FROM {$tableName}")->value();
        $fieldstring = implode(', ', $columns);
        $values = implode(', ', $values);
        $sql = "INSERT INTO $tableName ($fieldstring) VALUES ({$values});";

        $db = DB::query($sql, 0);
        if ($inserted != DB::query("SELECT COUNT(*) FROM {$tableName}")->value()) {
            $id = DB::getGeneratedID($tableName);
            $this->addLog("Inserted $values INTO $tableName");
            return $id;
        } else {
            $this->addLog("Failed to insert values $values INTO $tableName", TRUE);
            $this->critical[] = TRUE;
            return;
        }
    }

    /*
     * Create folder holders in File table and return ID of it
     */

    function createDbFolder() {
        $folder = Folder::find_or_make(CONFIGURATOR_DIR);
        return $folder->ID;
    }

    private function dumpData() {
        /*
         * Creating migration folder for assets
         */
        $path_to_export = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . self::exportDir;
        if (file_exists($path_to_export)) {
            self::removeDirectory($path_to_export);
        }
        mkdir($path_to_export, 02775, true);

        $export_filename = $path_to_export . DIRECTORY_SEPARATOR . self::exportFile;
        /*
         * File fields are same for all models
         */
        $fileFields = implode(',', $this->prependArray('File', $this->fileFields));

        /*
         * For tables with File realtion
         */
        foreach ($this->modelFields as $name => $table) {
            $columns = array();
            $query = DB::query(sprintf("SELECT COLUMN_NAME AS COL FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '%s'", $name));
            foreach ($query as $col) {
                $columns[] = $col['COL'];
            }
            if (count(array_intersect($columns, $table)) >= count($table)) {
                $this->addLog($name . ' :Description matches, proceeding to export');
                $_tempFields = implode(',', $this->prependArray($name, $this->modelFields[$name]));
                $sqlQuery = new SQLQuery();
                $sqlQuery->setFrom($name);
                $sqlQuery->selectField($_tempFields . ', ' . $fileFields);
                $sqlQuery->addLeftJoin('File', "File.ID = {$name}.ThumbnailPhotoID");
                foreach ($sqlQuery->execute() as $result) {
                    if ($result['File_Filename']) {
                        $result['File_Filename'] = $this->copyAssetToMig($result['File_Filename']);
                    }
                    $export[$name][] = $result;
                }
            } else {
                $this->addLog($name . ' :Description did not match the real table, it will not be exported', TRUE);
            }
        }

        /*
         * For tables with File as TarGzFile realtion
         */

        foreach ($this->optionFields as $name => $table) {
            $columns = array();
            $query = DB::query(sprintf("SELECT COLUMN_NAME AS COL FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '%s'", $name));
            foreach ($query as $col) {
                $columns[] = $col['COL'];
            }
            if (count(array_intersect($columns, $table)) >= count($table)) {
                $this->addLog($name . ' :Description matches, proceeding to export');
                $_tempFields = implode(',', $this->prependArray($name, $this->optionFields[$name]));
                $_fileFields = implode(',', $this->prependArray('TarGzFile', $this->fileFields));
                $sqlQuery = new SQLQuery();
                $sqlQuery->setFrom($name);
                $sqlQuery->selectField($_tempFields . ', ' . $_fileFields . ', ' . $fileFields);
                $sqlQuery->addLeftJoin('File', "File.ID = {$name}.ThumbnailPhotoID");
                $sqlQuery->addLeftJoin('File', "TarGzFile.ID = {$name}.SourceFilesID", 'TarGzFile');
                foreach ($sqlQuery->execute() as $result) {
                    if ($result['File_Filename']) {
                        $result['File_Filename'] = $this->copyAssetToMig($result['File_Filename']);
                    }
                    if ($result['TarGzFile_Filename']) {
                        $result['TarGzFile_Filename'] = $this->copyAssetToMig($result['TarGzFile_Filename']);
                    }
                    $export[$name][] = $result;
                }
            } else {
                $this->addLog($name . ' :Description did not match the real table, it will not be exported', TRUE);
            }
        }

        file_put_contents($export_filename, json_encode($export));
        if (file_exists($export_filename)) {
            $this->addLog($export_filename . ' data was successfully dumped', FALSE);
            return TRUE;
        } else {
            $this->addLog('Could not create: ' . $export_filename, TRUE);
            return;
        }
    }

    /*
     * Copies all assets file for export
     */

    private function copyAssetToMig($Filename) {
        $filepath = dirname(ASSETS_PATH) . DIRECTORY_SEPARATOR . $Filename;
        if (file_exists($filepath) && !is_dir($filepath)) {
            copy($filepath, CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . self::exportDir . DIRECTORY_SEPARATOR . basename($filepath));
            $this->addLog($filepath . ' copied', FALSE);
            return basename(ASSETS_PATH) . DIRECTORY_SEPARATOR . CONFIGURATOR_DIR . DIRECTORY_SEPARATOR . basename($filepath);
        } else {
            $this->addLog($filepath . ' doesnt exist', TRUE);
            return basename(ASSETS_PATH) . DIRECTORY_SEPARATOR . CONFIGURATOR_DIR . DIRECTORY_SEPARATOR . basename($filepath);
        }
    }

    /*
     * Copies all assets files from export to public dir
     */

    private function copyAssetToPub($Filename) {
        $filepath = dirname(ASSETS_PATH) . DIRECTORY_SEPARATOR . $Filename;
        $copyFrom = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . self::exportDir . DIRECTORY_SEPARATOR . basename($filepath);
        if (file_exists($copyFrom) && !is_dir($filepath)) {
            copy($copyFrom, $filepath);
            $this->addLog($filepath . ' copied', False);
            return basename(ASSETS_PATH) . DIRECTORY_SEPARATOR . CONFIGURATOR_DIR . DIRECTORY_SEPARATOR . basename($filepath);
        } else {
            $this->addLog($filepath . ' doesnt exist or is a directory', TRUE);
            return basename(ASSETS_PATH) . DIRECTORY_SEPARATOR . CONFIGURATOR_DIR . DIRECTORY_SEPARATOR . basename($filepath);
        }
    }

    function addLog($message, $error = FALSE) {
        if ($error)
            $this->log .= "<code style='color: orange;'>$message</code><br>";
        else
            $this->log .= "<code style='color: green;'>$message</code><br>";
    }

    /*
     * Inserts asset into db, copies it from migrate directory 
     * returns ID of the asset
     */

    function insertImage($parentID, $record) {
        if (isset($record['File_Name'])) {
            $this->copyAssetToPub($record['File_Filename']);
            $found = DB::query("SELECT ID FROM File WHERE Name = '" . $record['File_Name'] . "' AND ParentID = $parentID AND ClassName ='" . $record['File_ClassName'] . "'")->value();
            if (!$found) {
                $fields = $this->fileFields;
                unset($fields['ID']);
                unset($record['File_ID']);
                $record['File_ParentID'] = $parentID;
                foreach ($fields as $key => $column) {
                    if (isset($record['File_' . $column]) && $record['File_' . $column] != '')
                        $values[] = "'" . addslashes($record['File_' . $column]) . "'";
                    else
                        $values[] = "NULL";
                }

                $fieldstring = implode(', ', $fields);
                $values = implode(', ', $values);
                $sql = "INSERT INTO File ($fieldstring) VALUES ({$values});";
                DB::query($sql);
                return DB::getGeneratedID('File');
            } else
                return $found;
        }
    }

    function insertSource($parentID, $record) {
        if (isset($record['TarGzFile_Name'])) {
            $this->copyAssetToPub($record['TarGzFile_Filename']);
            $found = DB::query("SELECT ID FROM File WHERE Name = '" . $record['TarGzFile_Name'] . "' AND ParentID = $parentID AND ClassName ='" . $record['TarGzFile_ClassName'] . "'")->value();
            if (!$found) {
                $fields = $this->fileFields;
                unset($fields['ID']);
                unset($record['TarGzFile_ID']);
                $record['TarGzFile_ParentID'] = $parentID;
                foreach ($fields as $key => $column) {
                    if (isset($record['TarGzFile_' . $column]) && $record['TarGzFile_' . $column] != '')
                        $values[] = "'" . addslashes($record['TarGzFile_' . $column]) . "'";
                    else
                        $values[] = "NULL";
                }

                $fieldstring = implode(', ', $fields);
                $values = implode(', ', $values);
                $sql = "INSERT INTO File ($fieldstring) VALUES ({$values});";
                DB::query($sql);
                return DB::getGeneratedID('File');
            } else
                return $found;
        }
    }

    private function trucateTables() {
        foreach (array_merge($this->optionFields, $this->modelFields) as $key => $data) {
            DB::query("DELETE FROM {$key};");
        }
    }

    /*
     * Clear Thumbnail cache directory
     */

    private function clearThumbCache() {
        $folder = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . CONFIGURATOR_THUMBCAHCE_DIR . DIRECTORY_SEPARATOR;
        if (!file_exists($folder)) {
            $this->addLog('Cache folder does not exist! in <code>' . $folder . '</code>');
        } else {
            self::removeDirectory($folder);
            $this->addLog('Directory <code>' . $folder . '</code> cleared');
        }
        return TRUE;
    }

    public static function removeDirectory($directory) {
        $items = glob($directory . DIRECTORY_SEPARATOR . '{,.}*', GLOB_MARK | GLOB_BRACE);
        foreach ($items as $item) {
            if (basename($item) == '.' || basename($item) == '..')
                continue;
            if (substr($item, -1) == DIRECTORY_SEPARATOR)
                self::removeDirectory($item);
            else
                unlink($item);
        }
        if (is_dir($directory))
            rmdir($directory);
    }

    private function getExportJson() {
        $filename = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . self::exportDir . DIRECTORY_SEPARATOR . self::exportFile;
        if (file_exists($filename))
            return json_decode(file_get_contents($filename), TRUE);
        else
            return NULL;
    }

}
