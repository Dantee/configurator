<?php

class Configurator_Controller extends ContentController
{
    /*
     * Thumbnail generator configuration
     */
    CONST DEPENDECY_DELIMITER = '||';
    CONST THUMBNAIL_FRAME_NUMBER = 5; //Frame number which should be generated
    CONST MAX_THUMB_WIDTH = 600; //because of the cropping, it's not safe to make it bigger. For Facebook share
    CONST MAX_THUMB_WIDTH_SMALL = 160; //small version of thmbnail for internal use
    CONST THUMBNAIL_QUALITY = 100;
    /*
     * Book a test drive page class for configurator
     */
    CONST BOOKING_PAGE_CLASS = 'ConfiguratorTestDriveBookingPage';

    /*
     * General name for manifest file (with or without json extension)
     */
    CONST MANIFEST_FILENAME = 'manifest';
    /*
     * In case we are not supplied with image name in manifest.json, add sprite filename manually
     */
    CONST SPRITENAME = "sprite.png";

    CONST WRAP_PATH = 'http://www.isuzuutes.co.nz:8080/export/';
    //CONST WRAP_PATH = 'http://wraps/export/';

    private static $allowed_actions = array(
        'index', 'assets', 'getIframeURL', 'Categories', 'Cars', 'Variants', 'VariantData', 'Disclaimer', 'Social', 'SocialUrl', 'BookingUrl', 'Thumbnail', 'saveUserData', 'getBeacons',
        'AllVariants', 'getCarConfig'
    );
    private static $url_handlers = array(
        'Cars/$CategoryID!' => 'Cars',
        'Variants/$CarID!' => 'Variants',
        'VariantData/$VariantID!' => 'VariantData',
        'Social/$VariantID!' => 'Social',
        'SocialUrl/$VariantID!' => 'SocialUrl',
    );
    private $api_actions = array(
        'getCarConfig', 'getIframeURL', 'Disclaimer', 'BookingUrl', 'Categories', 'Cars', 'Variants', 'VariantData', 'Social', 'Thumbnail'
    );

    /*
     * Setting header to application/json is browser supports or text/plain for IE
     * Adding UTF-8 content type
     */

    public function handleAction($request, $action)
    {
        if (in_array($action, $this->api_actions)) {
            if (isset($_SERVER['HTTP_ACCEPT']) && strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false) {
                $this->response->addHeader('Content-Type', 'application/json');
            } else {
                $this->response->addHeader('Content-Type', 'text/plain');
            }
            $this->response->addHeader('charset', 'utf-8');
            return parent::handleAction($request, $action);
        } else {
            // default action handling     
            return parent::handleAction($request, $action);
        }
    }

    public function init()
    {
        parent::init();

        $homeLink = Session::get('HomeLink');

        if (!empty($homeLink)) {
            $this->customise(array('HomeLink' => $homeLink));
        }
    }

    public function index()
    {
        return $this->customise(['getTvAssets' => $this->getTvAssets()])->renderWith('ConfiguratorPage');
    }

    public function getIframeURL()
    {
        $LinkId = $_POST['id'];
        $url = '';
        /*
         * Check maybe model exists
         */
        $model = DataObject::get('CarConfiguratorModel')->filter(array('ModelID' => $LinkId))->First();
        if ($model) {
            $modelId = $model->ModelID;
            $modelTitle = $model->Title;
            if ($model->CarVariants()->Count() == 1) {
                $url = "?car-id=$modelId&name=" . urlencode($modelTitle) . "&variant=" . $model->CarVariants()->First()->VariantID;
            } else
                $url = "?car-id=$modelId&name=" . urlencode($modelTitle);
        } else {
            $variant = DataObject::get('CarConfiguratorVariant')->filter(array('LinkID' => $LinkId))->First();
            if ($variant) {
                $modelId = $variant->CarModelVariant()->ModelID;
                $modelTitle = $variant->CarModelVariant()->Title;
                $url = "?car-id=$modelId&name=" . urlencode($modelTitle) . "&variant=$variant->VariantID";
            }
        }
        return json_encode(array('success' => true, 'url' => $url));
    }

    public function getCarConfig()
    {
        $LinkId = $_POST['id'];
        $config = [];
        $model = DataObject::get('CarConfiguratorModel')->filter(array('ModelID' => $LinkId))->First();
        if ($model) {
            if ($model->CarVariants()->Count() > 0) {
                $variant = $model->CarVariants()->First();
            }
        } else {
            $variant = DataObject::get('CarConfiguratorVariant')->filter(array('LinkID' => $LinkId))->First();
        }
        if ($variant) {
            $config['modelID'] = $variant->CarModelVariant()->ModelID;
            $config['variantTitle'] = $variant->CarModelVariant()->Title;
            $config['variantID'] = $variant->VariantID;
            $config['colour'] = $variant->getDefaultColor()->ColourID;

            if ($variant->CarModelVariant()->DefaultWheels()->Count() > 0) {
                $config['wheelID'] = $variant->CarModelVariant()->DefaultWheels()->First()->WheelID;
            } else {
                if ($variant->Wheels()->First()) {
                    $config['wheelID'] = $variant->Wheels()->First()->WheelID;
                }
            }
        }
        return json_encode(['success' => true, 'config' => $config]);
    }

    /*
     * Print the text beneath configurator
     */

    public function Disclaimer()
    {

        $config = SiteConfig::current_site_config();
        if ($disclaimer = $config->CarConfiguratorDisclaimer)
            return json_encode(array('disclaimer' => $disclaimer));
        else
            return json_encode(array('disclaimer' => ''));
    }

    /*
     * Print the categories that we have cars in
     */

    public function Categories()
    {

        $categories = DataObject::get('CarConfiguratorCategory')->sort('ConfiguratorOrder', 'ASC');
        foreach ($categories as $category) {
            foreach ($category->CarConfiguratorModels() as $model) {
                if ($model->ID) {
                    $categories_list[$category->ID] = array('Title' => $category->Title, 'ID' => $category->ID);
                }
            }
        }
        if ($categories_list) {
            $category_nonaasoc = array_values($categories_list);
        } else {
            $category_nonaasoc = array();
        }
        return json_encode(array("Categories" => $category_nonaasoc), JSON_UNESCAPED_SLASHES);
    }

    /*
     * Print the Cars within the given category
     */

    public function Cars()
    {

        $categoryID = $this->request->param('CategoryID');
        $category = array();

        $models = DataObject::get('CarConfiguratorModel')->sort('Title');
        foreach ($models as $model) {
            $thumb = $model->ThumbnailPhoto()->Link();
            //because of many_many we have to do a loop
            foreach ($model->CarModelCategory() as $page) {
                if ($page->ID == $categoryID)
                    $category[$model->ID] = array('ID' => $model->ModelID, 'Title' => $model->Title, 'Thumbnail' => $thumb);
            }
        }

        if ($category) {
            $category_nonaasoc = array_values($category);
        } else {
            $category_nonaasoc = array();
        }
        return json_encode(array("cars" => $category_nonaasoc), JSON_UNESCAPED_SLASHES);
    }

    /*
     * Print the Variants within the given Car range
     */

    public function Variants()
    {

        $carID = $this->request->param('CarID');
        $variant_data = array();
        $models = DataObject::get('CarConfiguratorModel')->sort('Title');

        foreach ($models as $model) {
            if ($model->ModelID == $carID) {
                foreach ($model->CarVariants() as $variant) {
                    /*
                     * If variant doesn't have any thumbnail uplodaded, use default model thumbnail
                     */
                    if ($variant->ThumbnailPhotoID == 0)
                        $thumb = $model->ThumbnailPhoto()->Link();
                    else
                        $thumb = $variant->ThumbnailPhoto()->Link();

                    $variant_data[] = array(
                        'ID' => $variant->VariantID,
                        'Title' => $variant->Title,
                        'Content' => $variant->Description,
                        'Thumbnail' => $thumb
                    );
                }
            }
        }
        return json_encode(array("variants" => $variant_data), JSON_UNESCAPED_SLASHES);
    }

    public function AllVariants()
    {

        $data = array();
        $models = DataObject::get('CarConfiguratorModel')->sort('Title');

        foreach ($models as $model) {

            if (!isset($data['Categories'][$model->CarModelCategory()->ID])) {
                $data['Categories'][$model->CarModelCategory()->ID] = [
                    'ID' => $model->CarModelCategory()->ID,
                    'Title' => $model->CarModelCategory()->Title,
                    'Tag' => str_replace(' ', '-', strtolower($model->CarModelCategory()->Title)),
                    'Variants' => []
                ];
            }


            foreach ($model->CarVariants() as $variant) {
                /*
                 * If variant doesn't have any thumbnail uplodaded, use default model thumbnail
                 */
                if ($variant->ThumbnailPhotoID == 0)
                    $thumb = $model->ThumbnailPhoto()->Link();
                else
                    $thumb = $variant->ThumbnailPhoto()->Link();

                $data['Categories'][$model->CarModelCategory()->ID]['Variants'][] = [
                    'RealID' => $variant->ID,
                    'ID' => $variant->VariantID,
                    'Title' => $variant->Title,
                    'Content' => $variant->Description,
                    'Thumbnail' => $thumb,
                    'ModelID' => $model->ModelID,
                    'FacebookPixelID' => $variant->FacebookPixelID,
                    'CategoryID' => $model->CarModelCategory()->ID
                ];
            }
        }
        return json_encode($data, JSON_UNESCAPED_SLASHES);
    }

    public function VariantData()
    {

        $colours = array();
        $bodies = array();
        $wheels = array();

        //function to prepend directory to image files
        function addDirectory(&$item1, $key, $directory)
        {
            $item1 = "$directory" . DIRECTORY_SEPARATOR . "$item1";
        }

        $variantID = $this->request->param('VariantID');
        $variant = DataObject::get('CarConfiguratorVariant')->filter(array('VariantID' => $variantID))->First();

        if (!$variant) {
            return '[]';
        }

        /*
         * Get Wheels and Accessories that will be reused from variant to variant
         */
        $model = $variant->CarModelVariant();

        foreach ($model->DefaultWheels() as $model_wheel) {
            //if this wheel does not belong to a variant
            if ($model_wheel->VariantID == 0) {
                if ($layer = $this->getLayers($model->ModelID, $model_wheel->WheelID)) {
                    $coloursArr = array_filter(explode(',', $model_wheel->ColoursArray));

                    if ($dirtLayer = $this->getLayers($model->ModelID, $model_wheel->WheelID . '-dirt')) {
                        $dirt = array(
                            'ID' => $model->ModelID . '-' . $model_wheel->WheelID . '-dirt',
                            'zIndex' => /*$dirtLayer['z']*/
                                9999,
                            'Image' => $dirtLayer['dir'] . DIRECTORY_SEPARATOR . $dirtLayer['lp'],
                            'Map' => $dirtLayer['is'],
                            'Frame' => 1,
                        );
                    } else {
                        $dirt = [];
                    }

                    $wheels[] = array(
                        'ID' => $model->ModelID . '-' . $model_wheel->WheelID,
                        'Title' => $model_wheel->Title,
                        'ColourID' => $coloursArr,
                        'Thumbnail' => $model_wheel->ThumbnailPhoto()->Link(),
                        'zIndex' => /*$layer['z']*/
                            30,
                        'Image' => $layer['dir'] . DIRECTORY_SEPARATOR . $layer['lp'],
                        'Map' => $layer['is'],
                        'Frame' => 1,
                        'Dirt' => $dirt
                    );
                }
            }
        }

        /*
         * Get bodies that belong to this variant
         */
        foreach ($variant->Colours() as $colour) {
            if ($base = $this->getBases($variant->VariantID, $colour->ColourID)) {
                //append directory to images
                array_walk($base['is'], 'addDirectory', $base['dir']);

                /*
                 * Get colours that belong to this variant
                 */
                $colours[] = array(
                    'ID' => $variant->VariantID . '-' . $colour->ColourID,
                    'Title' => $colour->Title,
                    'Thumbnail' => $colour->ThumbnailPhoto()->Link(),
                );

                $bodies[] = array(
                    'ID' => $variant->VariantID . '-' . $colour->ColourID,
                    'ColourID' => $variant->VariantID . '-' . $colour->ColourID,
                    'Height' => $base['h'],
                    'Width' => $base['w'],
                    'Images' => $base['is']
                );
            }
        }

        /*
         * Get wheels that belong to a variant
         */
        foreach ($variant->Wheels() as $wheel) {
            if ($layer = $this->getLayers($variant->VariantID, $wheel->WheelID)) {
                $coloursArr = array_filter(explode(',', $wheel->ColoursArray));
                if ($dirtLayer = $this->getLayers($variant->VariantID, $wheel->WheelID . '-dirt')) {
                    $dirt = array(
                        'ID' => $variant->VariantID . '-' . $wheel->WheelID . '-dirt',
                        'zIndex' => /*$dirtLayer['z']*/
                            9999,
                        'Image' => $dirtLayer['dir'] . DIRECTORY_SEPARATOR . $dirtLayer['lp'],
                        'Map' => $dirtLayer['is'],
                        'Frame' => 1,
                    );
                } else {
                    $dirt = [];
                }
                $wheels[] = array(
                    'ID' => $variant->VariantID . '-' . $wheel->WheelID,
                    'Title' => $wheel->Title,
                    'ColourID' => $coloursArr,
                    'Thumbnail' => $wheel->ThumbnailPhoto()->Link(),
                    'zIndex' => /*$layer['z']*/
                        30,
                    'Image' => $layer['dir'] . DIRECTORY_SEPARATOR . $layer['lp'],
                    'Map' => $layer['is'],
                    'Frame' => 1,
                    'Dirt' => $dirt
                );
            }
        }

        if ($dirtLayer = $this->getLayers($variant->VariantID, 'dirt')) {
            $dirt = [
                'ID' => $variant->VariantID . '-dirt',
                'zIndex' => $dirtLayer['z'],
                'Image' => $dirtLayer['dir'] . DIRECTORY_SEPARATOR . $dirtLayer['lp'],
                'Map' => $dirtLayer['is'],
                'Frame' => 1
            ];
        } else {
            $dirt = [];
        }

        $variantData = array('ID' => $variant->VariantID, 'Title' => $variant->Title);

        return json_encode(array(
            'colour' => $colours,
            'bodies' => $bodies,
            'accessories' => $this->getAccessoriesArray($model->Accessories()),
            'wheels' => $wheels,
            'dirt' => $dirt,
            'variant' => $variantData
        ), JSON_UNESCAPED_SLASHES);
    }

    private function getAccessoriesArray($accessories)
    {
        $data = [];
        foreach ($accessories as $model_accessory) {

            $modelID = $model_accessory->Car()->ModelID;
            if ($layer = $this->getLayers($modelID, $model_accessory->AccessoryID)) {

                $conflictingAccessories = [];
                $relatedAccessories = [];
                $AddAfterRemove = [];
                foreach ($model_accessory->ConflictingAccessories() as $accessory) {
                    $conflictingAccessories[] = $modelID . '-' . $accessory->AccessoryID;
                }
                foreach ($model_accessory->RelatedAccessories() as $accessory) {
                    $relatedAccessories[] = $modelID . '-' . $accessory->AccessoryID;
                }
                foreach ($model_accessory->AddAfterRemove() as $accessory) {
                    $AddAfterRemove[] = $modelID . '-' . $accessory->AccessoryID;
                }
                $coloursArr = array_filter(explode(',', $model_accessory->ColoursArray));
                $acc = [
                    'ID' => $modelID . '-' . $model_accessory->AccessoryID,
                    'Title' => $model_accessory->Title,
                    'ColourID' => $coloursArr,
                    'Thumbnail' => $model_accessory->ThumbnailPhoto()->Link(),
                    'zIndex' => 40,
                    'Image' => $layer['dir'] . DIRECTORY_SEPARATOR . $layer['lp'],
                    'Map' => $layer['is'],
                    'Frame' => ($model_accessory->RotateCar == 0 ? NULL : ((int)$model_accessory->RotateCar)),
                    'ConflictingAccessories' => $conflictingAccessories,
                    'RelatedAccessories' => $relatedAccessories,
                    'AddAfterRemove' => $AddAfterRemove,
                    'IsDefault' => !!$model_accessory->IsDefault
                ];
                if ($model_accessory->ClassName == 'CarConfiguratorVariantPart') {
                    $acc['PartGroupID'] = $model_accessory->PartGroupID;
                }
                $data[] = $acc;
            }
        }
        return $data;
    }

    /*public function Social()
    {
        $response = array();
        $variantID = $this->request->param('VariantID');
        if($variant = DataObject::get('CarConfiguratorVariant')->filter(array('VariantID' => $variantID))->First())
        {
            $model = $variant->CarModelVariant();
            $_tmpl = array("{model}", "{variant}");
            $_replace = array($model->Title, $variant->Title);
            $response['configuratorfacebook']['data-title'] = str_replace($_tmpl, $_replace, SiteConfig::current_site_config()->CarConfiguratorFacebookTitle);
            $response['configuratorfacebook']['data-caption'] = str_replace($_tmpl, $_replace, SiteConfig::current_site_config()->CarConfiguratorFacebookCaption);
            $response['configuratorfacebook']['data-description'] = str_replace($_tmpl, $_replace, SiteConfig::current_site_config()->CarConfiguratorFacebookDescription);
            //$response['linkedinshare']['data-linkedintitle'] = $model->getCascadedSocialSharingLinkedinTitle();
            //$response['linkedinshare']['data-linkedinsummary'] = $model->getCascadedSocialSharingLinkedinSummary();
            //$response['twittershare']['data-twittertext'] = $model->getCascadedSocialSharingTwitterText();
            //$response['twittershare']['data-twitterhashtag'] = $model->getCascadedSocialSharingTwitterHashtag();
            //$response['pinterestshare']['data-pinterestdescription'] = $model->getCascadedSocialSharingPinterestDescription();

            return json_encode($response, JSON_UNESCAPED_SLASHES);
        }

        return $this->response->setStatusCode(400);
    }*/

    public function BookingUrl()
    {
        if (class_exists(self::BOOKING_PAGE_CLASS)) {
            $booking_page = DataObject::get(self::BOOKING_PAGE_CLASS)->Sort("LastEdited DESC")->Limit(1)->First()->Link();
            if ($booking_page)
                return json_encode(array('Url' => $booking_page), JSON_UNESCAPED_SLASHES);
            else
                return json_encode(array('Url' => FALSE), JSON_UNESCAPED_SLASHES);
        }
        $this->response->setStatusCode(501);
    }

    public function saveUserData()
    {

        $vars = $this->request->getVars();

        $configuration = new UserConfigurationModel();
        $configuration->Uuid = $vars['UUID'];
        $configuration->Data = serialize($vars);
        $configuration->write();
    }

    public function getBeacons()
    {

        $beacons = BeaconModel::get();
        $data = [];
        foreach ($beacons as $beacon) {
            $data[] = $beacon->toArray();
        }
        return json_encode($data);
    }

    private function getBases($parentID, $colourID)
    {
        $bases_directory = 'bases/' . $parentID . '-' . $colourID;
        if ($bases = $this->getLocalManifest($bases_directory)) {
            $bases['dir'] = $bases_directory;
            return $bases;
        } else
            return;
    }

    private function getBaseById($ID)
    {
        $bases_directory = 'bases/' . $ID;
        if ($bases = $this->getLocalManifest($bases_directory)) {
            $bases['dir'] = $bases_directory;
            return $bases;
        } else
            return;
    }

    private function getLayers($parentID, $itemID)
    {
        $layers_dir = 'layers/' . $parentID . '-' . $itemID;
        if ($layers = $this->getLocalManifest($layers_dir)) {
            $layers['dir'] = $layers_dir;
            //override sprite name by default;
            if (!array_key_exists('lp', $layers)) {
                $layers['lp'] = self::SPRITENAME;
            }
            return $layers;
        } else
            return;
    }

    private function getLayerByID($ID)
    {
        $layers_dir = 'layers/' . $ID;
        if ($layers = $this->getLocalManifest($layers_dir)) {
            $layers['dir'] = $layers_dir;
            //override sprite name by default;
            if (!array_key_exists('lp', $layers)) {
                $layers['lp'] = self::SPRITENAME;
            }
            return $layers['is'];
        } else
            return;
    }

    private function getLocalManifest($directory)
    {
        $filename = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . self::MANIFEST_FILENAME;
        if (file_exists($filename))
            return json_decode(file_get_contents($filename), TRUE);
        else
            return NULL;
    }

    private function getWrapByHash($hash)
    {

        $filename = self::WRAP_PATH . $hash . DIRECTORY_SEPARATOR . self::MANIFEST_FILENAME;
        $manifest = json_decode(file_get_contents($filename), TRUE);
        return $manifest['is'];
    }

    public function Thumbnail()
    {
        /*
         * If any parameters are not set, then probably it is not configurator requesting
         */
        $config = json_decode($this->request->getBody(), true);
        if (!isset($config['colourID']) || !isset($config['wheelID']))
            return $this->response->setStatusCode(400);
        /*
         * Check if parameters are real ids, so we would avoid bogus image creation
         */
        if ($_layer = $this->getBaseById($config['colourID'])) {
            $colour_data = $_layer;
        } else {
            return $this->response->setStatusCode(400);
        }
        if ($_layer = $this->getLayerByID($config['wheelID'])) {
            $wheel_data = $_layer;
        } else {
            return $this->response->setStatusCode(400);
        }
        $accessories = array();
        if (isset($config['accessories']) && is_array($config['accessories'])) {
            foreach ($config['accessories'] as $accessoryID) {
                if ($_layer = $this->getLayerByID($accessoryID)) {
                    $accessories[$accessoryID] = $_layer;
                }
            }
        }

        /*
         * Setting default size of thumbnail to MAX_THUMB_WIDTH_SMALL
         */
        $thumb_width = (isset($config['size']) && $config['size'] == 'large' ? self::MAX_THUMB_WIDTH : self::MAX_THUMB_WIDTH_SMALL);

        /*
         * Get the requied base image
         */
        $destPath = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . CONFIGURATOR_THUMBCAHCE_DIR . DIRECTORY_SEPARATOR;
        if (!file_exists($destPath)) {
            mkdir($destPath, 02775, true);
        }
        /*
         * Generate the filename and if it already exists, just return it
         */
        $filename = md5(json_encode($config)) . image_type_to_extension(IMAGETYPE_JPEG);
        if (!file_exists($destPath . $filename)) {
            /*
             * Open base image
             */
            $basePath = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . 'bases' . DIRECTORY_SEPARATOR . $config['colourID'] . DIRECTORY_SEPARATOR . $colour_data['is'][self::THUMBNAIL_FRAME_NUMBER];
            $baseIMG = $this->imageCreateFromAny($basePath);

            /*
             * Prepare for resizing
             */
            $_width = imagesx($baseIMG);
            $_height = imagesy($baseIMG);
            $width = $thumb_width;
            $height = (($_height * $width) / $_width);

            /*
             * Add wheels, cannot be empty
             */
            if (isset($wheel_data[self::THUMBNAIL_FRAME_NUMBER])) {
                $_layer = $wheel_data[self::THUMBNAIL_FRAME_NUMBER];
                $layer = $this->imageCreateFromAny(CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . 'layers' . DIRECTORY_SEPARATOR . $config['wheelID'] . DIRECTORY_SEPARATOR . self::SPRITENAME . self::THUMBNAIL_FRAME_NUMBER);
                imagecopy($baseIMG, $layer, $_layer['cxo'], $_layer['cyo'], 0, 0, imageSX($layer), imageSY($layer));
                imagedestroy($layer);
            }
            /*
             * Add accessories, can be empty
             */
            foreach ($accessories as $accessoryID => $layer_data) {
                if (isset($layer_data[self::THUMBNAIL_FRAME_NUMBER])) {
                    $_layer = $layer_data[self::THUMBNAIL_FRAME_NUMBER];
                    $layer = $this->imageCreateFromAny(CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . 'layers' . DIRECTORY_SEPARATOR . $accessoryID . DIRECTORY_SEPARATOR . self::SPRITENAME . self::THUMBNAIL_FRAME_NUMBER);
                    imagecopy($baseIMG, $layer, $_layer['cxo'], $_layer['cyo'], 0, 0, imageSX($layer), imageSY($layer));
                    imagedestroy($layer);
                }
            }

            if (!empty($config['wrap'])) {
                if ($wrapData = $this->getWrapByHash($config['wrap'])) {
                    if (isset($wrapData[self::THUMBNAIL_FRAME_NUMBER])) {
                        $_layer = $wrapData[self::THUMBNAIL_FRAME_NUMBER];
                        $layer = $this->imageCreateFromAny(self::WRAP_PATH . $config['wrap'] . DIRECTORY_SEPARATOR . self::SPRITENAME);
                        imagecopy($baseIMG, $layer, $_layer['cxo'], $_layer['cyo'], $_layer['xo'], $_layer['yo'], $_layer['w'], $_layer['h']);
                        imagedestroy($layer);
                    } else {
                        die('wrap frame not found');
                    }
                } else {
                    die('wrap not found');
                }
            }

            /*
             * Resize the image and
             */
            $resized = imagecreatetruecolor($width, $height + ($thumb_width / 20));
            imagecopyresampled($resized, $baseIMG, 0, 0, 80, 80, $width, $height + ($thumb_width / 20), $_width - 260, $_height - 80);
            imagedestroy($baseIMG);

            /*
             * Crop image and then save
             */

            imagejpeg($resized, $destPath . $filename, self::THUMBNAIL_QUALITY);

            imagedestroy($resized);
        }
        $public_url = Director::absoluteBaseURL() . CONFIGURATOR_DIR . DIRECTORY_SEPARATOR . REELD_DIR . DIRECTORY_SEPARATOR . CONFIGURATOR_THUMBCAHCE_DIR . DIRECTORY_SEPARATOR . $filename;

        if (!empty($config['wrap'])) {
            $userWrap = UserWrap::get()->filter(['Hash' => $config['wrap']])->First();
            if ($userWrap) {
                $userWrap->PreviewUrl = $public_url;
                $userWrap->write();
            }
        }

        return json_encode(array('url' => $public_url), JSON_UNESCAPED_SLASHES);

    }

    public function imageCreateFromAny($filepath)
    {
        $type = exif_imagetype($filepath);
        $allowedTypes = array(
            IMAGETYPE_GIF, // [] gif 
            IMAGETYPE_JPEG, // [] jpg 
            IMAGETYPE_PNG, // [] png 
            IMAGETYPE_BMP   // [] bmp 
        );
        if (!in_array($type, $allowedTypes)) {
            return false;
        }
        switch ($type) {
            case IMAGETYPE_GIF :
                $im = @imageCreateFromGif($filepath);
                break;
            case IMAGETYPE_JPEG :
                $im = @imageCreateFromJpeg($filepath);
                break;
            case IMAGETYPE_PNG :
                $im = @imageCreateFromPng($filepath);
                break;
            case IMAGETYPE_BMP :
                $im = @imageCreateFromBmp($filepath);
                break;
        }
        return $im;
    }

    public function Social()
    {
        $variantID = $this->request->param('VariantID');
        if ($variant = DataObject::get('CarConfiguratorVariant')->filter(array('VariantID' => $variantID))->First()) {
            $model = $variant->CarModelVariant();

            $_tmpl = array("{model}", "{variant}");
            $_replace = array($model->Title, $variant->Title);

            $response = array(
                'socialFacebook' => array(
                    'data-title' => str_replace($_tmpl, $_replace, SiteConfig::current_site_config()->CarConfiguratorFacebookTitle),
                    'data-caption' => str_replace($_tmpl, $_replace, SiteConfig::current_site_config()->CarConfiguratorFacebookCaption),
                    'data-description' => str_replace($_tmpl, $_replace, SiteConfig::current_site_config()->CarConfiguratorFacebookDescription),
                    'data-url' => Director::absoluteBaseURL() . $variant->CarModelProductPage()->Link()
                ),
                'socialTwitter' => array(
                    'data-url' => Director::absoluteBaseURL() . $variant->CarModelProductPage()->Link()
                )
            );

            return json_encode($response, JSON_UNESCAPED_SLASHES);
        }

        return $this->response->setStatusCode(400);
    }

    private function getTvAssets()
    {
        $data = new ArrayList();
        if (isset ($_GET['tv'])) {
            foreach (Assets_Controller::$css as $asset) {
                $data->add(new ArrayData(['File' => $asset, 'Type' => 'css']));
            }
            foreach (Assets_Controller::$javascript as $asset) {
                $data->add(new ArrayData(['File' => $asset, 'Type' => 'javascript']));
            }
            $data->add(new ArrayData(['File' => Assets_Controller::$javascripttv[0], 'Type' => 'javascript']));
        } else {
            foreach (Assets_Controller::$cssweb as $asset) {
                $data->add(new ArrayData(['File' => $asset, 'Type' => 'css']));
            }
            foreach (Assets_Controller::$javascript as $asset) {
                $data->add(new ArrayData(['File' => $asset, 'Type' => 'javascript']));
            }
            $data->add(new ArrayData(['File' => Assets_Controller::$javascriptweb[0], 'Type' => 'javascript']));
        }
        return $data;
    }
}
