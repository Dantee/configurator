<?php

class Assets_Controller extends Controller {

    private static $allowed_actions = [
        'getCSS',
        'getJS',
        'getReeldData',
        'getThumbs',
        'getApi',
        'getAssets',
    ];

    public static $cssweb = [
        'src/css/style.css',
    ];

    public static $javascript = [
        'js/jquery.js',
        'js/lodash.min.js',
        'js/signals.min.js',
        'js/crossroads.min.js',
        'js/superagent.js',
        'js/react.min.js',
        'js/jquery.mousewheel.min.js',
        'js/reel.js',
        'reeld/reeld.js'
    ];

    public static $javascriptweb = [
        'src/js/min/main.min.js'
    ];

    public function getCSS() {
        echo $this->collectContent(self::$css, $this->urlParams['ID']);
    }

    public function getReeldData() {
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . 'reeld-data/bases', RecursiveDirectoryIterator::SKIP_DOTS));
        foreach ($iterator as $name => $item) {
            echo Director::protocolAndHost() . DIRECTORY_SEPARATOR . str_replace(BASE_PATH . DIRECTORY_SEPARATOR, '', $name) . "\n";
        }

        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . 'reeld-data/layers', RecursiveDirectoryIterator::SKIP_DOTS));
        foreach ($iterator as $name => $item) {
            echo Director::protocolAndHost() . DIRECTORY_SEPARATOR . str_replace(BASE_PATH . DIRECTORY_SEPARATOR, '', $name) . "\n";
        }
    }

    public function getJS() {
        echo $this->collectContent(self::$javascript, $this->urlParams['ID']);
    }

    public function getThumbs() {
        $basePath = CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . 'api/';
        $categories = json_decode(file_get_contents($basePath . 'Categories'));
        foreach ($categories->Categories as $category) {
            $carsInCategory = json_decode(file_get_contents($basePath . 'Cars/' . $category->ID));
            foreach ($carsInCategory->cars as $car) {
                $this->printThumb($car->Thumbnail);

                $variants = json_decode(file_get_contents($basePath . 'Variants/' . $car->ID));
                foreach ($variants->variants as $variant) {
                    $this->printThumb($variant->Thumbnail);
                    $variantData = json_decode(file_get_contents($basePath . 'VariantData/' . $variant->ID));
                    foreach ($variantData->colour as $colour) {
                        $this->printThumb($colour->Thumbnail);
                    }
                    foreach ($variantData->accessories as $accessory) {
                        $this->printThumb($accessory->Thumbnail);
                    }
                    foreach ($variantData->wheels as $wheel) {
                        $this->printThumb($wheel->Thumbnail);
                    }
                }
            }
        }
    }

    private function printThumb($thumb) {
        if ($thumb != '/assets/') echo Director::protocolAndHost() . $thumb . "\n";
    }

    private function collectContent($files, $lastModify) {
        $content = '';
        foreach ($files as $file) {
            $filetime = filemtime(BASE_PATH . DIRECTORY_SEPARATOR . CONFIGURATOR_DIR . DIRECTORY_SEPARATOR . $file);
            if ($filetime > $lastModify) {
                $content .= Director::protocolAndHost() . DIRECTORY_SEPARATOR . CONFIGURATOR_DIR . DIRECTORY_SEPARATOR . $file . "\n";
            }
        }
        return $content;
    }

    public function getApi() {
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . STATIC_DATA_DIR, RecursiveDirectoryIterator::SKIP_DOTS));
        foreach ($iterator as $name => $item) {
            if ($item->getFileName() == '.gitkeep') continue;
            echo Director::protocolAndHost() . DIRECTORY_SEPARATOR . str_replace(BASE_PATH . DIRECTORY_SEPARATOR, '', $name) . "\n";
        }
    }
    
	public function getAssets() {
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(CONFIGURATOR_PATH . DIRECTORY_SEPARATOR . 'assets', RecursiveDirectoryIterator::SKIP_DOTS));
        foreach ($iterator as $name => $item) {
            echo Director::protocolAndHost() . DIRECTORY_SEPARATOR . str_replace(BASE_PATH . DIRECTORY_SEPARATOR, '', $name) . "\n";
        }
    }
}