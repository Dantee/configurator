<?php

class ConfiguratorToCarModelExtension extends DataExtension {
    
    private static $has_one = [
        'ConfiguratorModel' => 'CarConfiguratorModel'
    ];
    
    function getCarConfiguratorModels() {
        return  $variants = CarConfiguratorModel::get()->map('ID', 'Title');
    }
    public function updateCMSFields(FieldList $fields) {
    /** @var DropdownField $field */
        $field = new DropdownField('ConfiguratorModelID','Relate to Configurator Model', $this->getCarConfiguratorModels());
        $field->setEmptyString('-- Select one --');
        $fields->addFieldToTab('Root.Main', $field);
    }
}