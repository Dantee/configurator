<?php

class SiteConfigConfiguratorExtension extends DataExtension {
    private static $db = [
        'CarConfiguratorFacebookTitle' => "Varchar(255)",
        'CarConfiguratorFacebookCaption' => "Varchar(255)",
        'CarConfiguratorFacebookDescription' => "Text",
        'CarConfiguratorDisclaimer' => "Varchar(255)"
    ];

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldToTab('Root.CarConfiguratorSettings', new HeaderField('FB_Title', 'Facebook extension for configurator'));
        $fields->addFieldToTab('Root.CarConfiguratorSettings', new LabelField('FB_Title', 'Data for Facebook Feed dialog, {model} and {variant} will be replaced with real model and variant names'));
        $fields->addFieldToTab('Root.CarConfiguratorSettings', new TextField('CarConfiguratorFacebookTitle', 'Title'));
        $fields->addFieldToTab('Root.CarConfiguratorSettings', new TextField('CarConfiguratorFacebookCaption', 'Caption'));
        $fields->addFieldToTab('Root.CarConfiguratorSettings', new TextField('CarConfiguratorFacebookDescription', 'Description'));
        
        $fields->addFieldToTab('Root.CarConfiguratorSettings', new HeaderField('Disclaimer_content', 'Disclaimer'));
        $fields->addFieldToTab('Root.CarConfiguratorSettings', new TextField('CarConfiguratorDisclaimer', 'Vehicle Configurator Disclaimer' ));
    }
}