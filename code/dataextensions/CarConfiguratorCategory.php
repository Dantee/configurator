<?php

class CarConfiguratorCategory extends DataObject {
    private static $db = [
        'Title' => 'Varchar(125)',
        'ConfiguratorOrder' => "Enum('0,1,2,3,4,5,6,7,8,9', '0')"
    ];

    private static $has_many = [
        'CarConfiguratorModels' => 'CarConfiguratorModel'
    ];

    public function updateCMSFields(FieldList $fields) {
        $Dropdown = new DropdownField('ConfiguratorOrder', 'Vehicle Configurator Order', singleton('CarConfiguratorCategory')->dbObject('ConfiguratorOrder')->enumValues());
        $Dropdown->setRightTitle('The order in which the categories will be displayed in the vehicle configurator');
        $fields->addFieldToTab('Root.CarConfiguratorOrder', $Dropdown);
    }
}