<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ColourExtension extends DataExtension {
    const checkboxclass = 'groupColours';
    CONST defaultColour = 'c_all';
    
    private static $db = [
        'ColoursArray' => "Text"
    ];
    private static $defaults = [
        'ColoursArray' => [self::defaultColour]
    ];
//
//    private static $summary_fields = [
//        'Title' => 'Title',
//        'Colours' => 'Colours'
//    ];

    function updateSummaryFields(&$fields) {
        $fields['getColours'] = 'Colours';
    }

    public function updateCMSFields(FieldList $fields) {
    $customJS = <<<'EOT'
    (function($) {$(document).on('change','.groupColours input', function() {
    var $inputs = $('.groupColours input'),
        $checked = $inputs.filter(':checked'),
        $all = $inputs.eq(0),
        index = $inputs.index(this);
    (index === 0 ? $checked : $all).removeAttr('checked');
    if(index === 0 || $checked.length === 0) {$all.attr('checked', 'checked');}});
    })(jQuery);
EOT;
        // make an empty choices array so it would display All if colours are yet to add
        $choices = array(self::defaultColour => 'All');
        if($this->owner->ColoursArray == NULL)
            $this->owner->ColoursArray = self::defaultColour;
        
        $request = $_SERVER['REQUEST_URI'];
        //first we search for model ID
        $parameters = explode('/', $request);
        foreach ($parameters as $key => $parameter) {
            if ($parameter == 'CarConfiguratorModel' && $parameters[$key + 1] == 'item')
                $model_id = $parameters[$key + 2];
            if ($parameter == 'CarVariants' && $parameters[$key + 1] == 'item')
                $variant_id = $parameters[$key + 2];
        }

        $model = DataObject::get('CarConfiguratorModel')->byID($model_id);
        foreach ($model->CarVariants() as $variant) {
            if (isset($variant_id)) {
                if ($variant->ID == $variant_id)
                    foreach ($variant->Colours() as $colour) {
                        $choices[$variant->VariantID .'-'. $colour->ColourID] = $variant->Title .' - '. $colour->Title;
                    }
            } else
                foreach ($variant->Colours() as $colour) {
                        $choices[$variant->VariantID .'-'. $colour->ColourID] = $variant->Title .' - '. $colour->Title;
                }
        }
        $Dropdown = new CheckboxSetField('ColoursArray', 'Colour', $choices);
        $Dropdown->setRightTitle('Display this accessory with this color only');
        $Dropdown->addExtraClass(self::checkboxclass);
        $fields->addFieldToTab('Root.Main', $Dropdown);
        $fields->addFieldToTab('Root.Main', new LiteralField('js', "<script>{$customJS}</script>"));
    }
    
    public function onBeforeWrite() {
        if(strpos($this->owner->ColoursArray, self::defaultColour) !== false)
            $this->owner->ColoursArray = NULL;
        parent::onBeforeWrite();
    }

    public function getColours() {
        $colourText = '';
        if($this->owner->ColoursArray != null || $this->owner->ColoursArray != '') {
            $colours = explode(',',$this->owner->ColoursArray);
            foreach($colours as $colourVariantID) {
                $ids = explode('-', $colourVariantID, 2);
                $colourID = $ids[1];
                $variantID = $ids[0];
                $variant = DataObject::get('CarConfiguratorVariant')->filter(array('VariantID' => $variantID))->first();
                $variantTitle =  $variant->Title;
                $variantID = $variant->ID;
                if($colourObj = DataObject::get('CarConfiguratorVariantColour')->filter(array('ColourID' => $colourID, 'CarID' => $variantID))->first()) {
                    if($colourText != '') $colourText .= ', ';
                    $colourText .= $variantTitle . ': ' . $colourObj->Title;
                }
            }
        } else {
            $colourText = 'All';
        }
        return $colourText;
    }


}
